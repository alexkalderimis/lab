module github.com/zaquestion/lab

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/avast/retry-go v0.0.0-20180319101611-5469272a8171
	github.com/cpuguy83/go-md2man v1.0.8 // indirect
	github.com/dustin/go-humanize v1.0.0
	github.com/fatih/color v1.9.0
	github.com/gdamore/tcell v1.1.4
	github.com/gdamore/tcell/v2 v2.5.1 // indirect
	github.com/gopherjs/gopherjs v0.0.0-20181103185306-d547d1d9531e // indirect
	github.com/hashicorp/go-cleanhttp v0.5.1
	github.com/hashicorp/go-retryablehttp v0.6.4
	github.com/hashicorp/hcl v0.0.0-20180404174102-ef8a98b0bbce // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/jinzhu/now v1.1.2
	github.com/jtolds/gls v4.2.1+incompatible // indirect
	github.com/juju/ansiterm v0.0.0-20180109212912-720a0952cc2a // indirect
	github.com/juju/loggo v0.0.0-20190526231331-6e530bcce5d8
	github.com/lunixbochs/vtclean v0.0.0-20180621232353-2d01aacdc34a
	github.com/machinebox/graphql v0.2.2
	github.com/magiconair/properties v1.7.6 // indirect
	github.com/matryer/is v1.2.0 // indirect
	github.com/mitchellh/mapstructure v0.0.0-20180220230111-00c29f56e238 // indirect
	github.com/onsi/gomega v1.4.3 // indirect
	github.com/pelletier/go-toml v1.1.0 // indirect
	github.com/pkg/errors v0.8.0
	github.com/rickar/cal/v2 v2.0.0-beta.2
	github.com/rivo/tview v0.0.0-20220307222120-9994674d60a8
	github.com/russross/blackfriday v1.5.1 // indirect
	github.com/shibukawa/configdir v0.0.0-20170330084843-e180dbdc8da0
	github.com/sirupsen/logrus v1.2.0 // indirect
	github.com/smartystreets/assertions v0.0.0-20180927180507-b2de0cb4f26d // indirect
	github.com/smartystreets/goconvey v0.0.0-20181108003508-044398e4856c // indirect
	github.com/spf13/afero v1.1.0 // indirect
	github.com/spf13/cast v1.2.0 // indirect
	github.com/spf13/cobra v0.0.0-20180412120829-615425954c3b
	github.com/spf13/jwalterweatherman v0.0.0-20180109140146-7c0cea34c8ec // indirect
	github.com/spf13/pflag v1.0.1
	github.com/spf13/viper v0.0.0-20180507071007-15738813a09d
	github.com/stretchr/testify v1.4.0
	github.com/tcnksm/go-gitconfig v0.1.2
	github.com/wadey/gocovmerge v0.0.0-20160331181800-b5bfa59ec0ad // indirect
	github.com/xanzy/go-gitlab v0.29.0
	gitlab.com/alexkalderimis/trapezi v0.0.4
	golang.org/x/arch v0.0.0-20181203225421-5a4828bb7045 // indirect
	golang.org/x/crypto v0.0.0-20191011191535-87dc89f01550
	golang.org/x/sys v0.0.0-20220513210249-45d2b4557a2a // indirect
	golang.org/x/term v0.0.0-20220411215600-e5f449aeb171 // indirect
	gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.0 // indirect
)

replace github.com/spf13/cobra => github.com/rsteube/cobra v0.0.1-zsh-completion-custom

go 1.14
