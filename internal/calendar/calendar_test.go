package calendar

import (
	"path/filepath"
	"testing"
	"time"

	cal "github.com/rickar/cal/v2"
	assert "github.com/stretchr/testify/assert"
	lab "github.com/zaquestion/lab/internal/gitlab"
)

func TestCompactLeave(t *testing.T) {
	configdirs := lab.ConfigDir()
	configdirs.LocalPath, _ = filepath.Abs("./testdata")
	expected := []LeavePeriod{
		LeavePeriod{
			Name:  "vacation-travel",
			Start: Date{Time: time.Date(2020, 2, 20, 0, 0, 0, 0, time.UTC)},
			End:   Date{Time: time.Date(2020, 3, 12, 0, 0, 0, 0, time.UTC)},
		},
		LeavePeriod{
			Name:  "illness",
			Start: Date{Time: time.Date(2020, 4, 5, 0, 0, 0, 0, time.UTC)},
			End:   Date{Time: time.Date(2020, 4, 15, 0, 0, 0, 0, time.UTC)},
		},
	}

	assert.Equal(t, expected, LoadLeave())
}

func TestLeaveIncludesTime(t *testing.T) {
	oneMinute, err := time.ParseDuration("1m")
	assert.Nil(t, err, "we can parse 1min")

	period := LeavePeriod{
		Name:  "test-period",
		Start: Date{Time: time.Date(2020, 2, 25, 0, 0, 0, 0, time.UTC)},
		End:   Date{Time: time.Date(2020, 3, 5, 0, 0, 0, 0, time.UTC)},
	}

	assert.True(t, leaveIncludesTime(period, period.Start.Time), "Includes start time")
	assert.True(t, leaveIncludesTime(period, period.Start.Time.Add(oneMinute)), "Start + 1min")
	assert.True(t, leaveIncludesTime(period, period.End.Time), "Includes end time")
	assert.True(t, leaveIncludesTime(period, period.End.Time.Add(-oneMinute)), "Includes end time - 1min")
	assert.True(t, leaveIncludesTime(period, period.Start.Time.AddDate(0, 0, 1)), "Start + 1day")
	assert.False(t, leaveIncludesTime(period, period.Start.Time.AddDate(0, 0, -1)), "Start - 1day")
	assert.True(t, leaveIncludesTime(period, period.End.Time.AddDate(0, 0, -1)), "End - 1day")
	assert.False(t, leaveIncludesTime(period, period.End.Time.AddDate(0, 0, 1)), "End + 1day")

	singleDay := LeavePeriod{
		Name:  "single-day",
		Start: Date{Time: time.Date(2020, 2, 25, 0, 0, 0, 0, time.UTC)},
		End:   Date{Time: time.Date(2020, 2, 25, 0, 0, 0, 0, time.UTC)},
	}

	assert.True(t, leaveIncludesTime(singleDay, singleDay.Start.Time), "Includes start time")
	assert.True(t, leaveIncludesTime(singleDay, singleDay.Start.Time.Add(oneMinute)), "Includes start time + 1min")
	assert.True(t, leaveIncludesTime(singleDay, cal.DayEnd(singleDay.End.Time)), "Includes end time")
	assert.True(t, leaveIncludesTime(singleDay, cal.DayEnd(singleDay.End.Time).Add(-oneMinute)), "Includes end time -1min")

	assert.True(t, leaveIncludesTime(LoadLeave()[0], time.Date(2020, 2, 20, 0, 0, 0, 0, time.UTC)))
}

func TestFindLeave(t *testing.T) {
	leavePeriods := []LeavePeriod{
		LeavePeriod{
			Name:  "a",
			Start: Date{Time: time.Date(2020, 2, 25, 0, 0, 0, 0, time.UTC)},
			End:   Date{Time: time.Date(2020, 3, 5, 0, 0, 0, 0, time.UTC)},
		},
		LeavePeriod{
			Name:  "b",
			Start: Date{Time: time.Date(2020, 3, 10, 0, 0, 0, 0, time.UTC)},
			End:   Date{Time: time.Date(2020, 3, 15, 0, 0, 0, 0, time.UTC)},
		},
		LeavePeriod{
			Name:  "c",
			Start: Date{Time: time.Date(2020, 3, 25, 0, 0, 0, 0, time.UTC)},
			End:   Date{Time: time.Date(2020, 3, 25, 0, 0, 0, 0, time.UTC)},
		},
	}
	expected := leavePeriods[1]

	for day := 10; day <= 15; day++ {
		found, i := findLeave(leavePeriods, time.Date(2020, 3, day, 0, 0, 0, 0, time.UTC))
		assert.Equal(t, &expected, found)
		assert.Equal(t, 1, i)
	}
	found, _ := findLeave(leavePeriods, time.Date(2020, 3, 16, 0, 0, 0, 0, time.UTC))
	assert.Nil(t, found)
	found, _ = findLeave(leavePeriods, time.Date(2020, 3, 9, 0, 0, 0, 0, time.UTC))
	assert.Nil(t, found)
	found, _ = findLeave(leavePeriods, time.Date(2020, 2, 24, 0, 0, 0, 0, time.UTC))
	assert.Nil(t, found)
	found, _ = findLeave(leavePeriods, time.Date(2020, 3, 26, 0, 0, 0, 0, time.UTC))
	assert.Nil(t, found)

	found, i := findLeave(LoadLeave(), time.Date(2020, 2, 20, 0, 0, 0, 0, time.UTC))
	assert.NotNil(t, found)
	assert.Equal(t, 0, i)
}

func TestBusinessDaysInRange(t *testing.T) {
	jul_05 := time.Date(2020, 7, 05, 0, 0, 0, 0, time.UTC)
	jul_12 := time.Date(2020, 7, 12, 0, 0, 0, 0, time.UTC)
	aug_30 := time.Date(2020, 8, 30, 0, 0, 0, 0, time.UTC)
	sep_06 := time.Date(2020, 9, 06, 0, 0, 0, 0, time.UTC)

	assert.Equal(t, 0, BusinessDaysInRange(nil, nil))
	assert.Equal(t, 5, BusinessDaysInRange(&jul_05, &jul_12)) // regular week
	assert.Equal(t, 4, BusinessDaysInRange(&aug_30, &sep_06)) // Aug. BH
}

func TestIsLeave(t *testing.T) {
	configdirs := lab.ConfigDir()
	configdirs.LocalPath, _ = filepath.Abs("./testdata")

	apr_12 := time.Date(2020, 4, 12, 0, 0, 0, 0, time.UTC)
	apr_13 := time.Date(2020, 4, 13, 0, 0, 0, 0, time.UTC)
	apr_14 := time.Date(2020, 4, 14, 0, 0, 0, 0, time.UTC)

	assert.False(t, IsLeave(apr_12), "Sunday")
	assert.False(t, IsLeave(apr_13), "Easter Monday")
	assert.True(t, IsLeave(apr_14), "Tuesday")
}

func TestLeaveDaysInRange(t *testing.T) {
	configdirs := lab.ConfigDir()
	configdirs.LocalPath, _ = filepath.Abs("./testdata")

	feb_19 := time.Date(2020, 2, 19, 0, 0, 0, 0, time.UTC)
	mar_13 := time.Date(2020, 3, 13, 0, 0, 0, 0, time.UTC)
	apr_04 := time.Date(2020, 4, 04, 0, 0, 0, 0, time.UTC)
	apr_16 := time.Date(2020, 4, 16, 0, 0, 0, 0, time.UTC)
	apr_11 := time.Date(2020, 4, 11, 0, 0, 0, 0, time.UTC)
	apr_30 := time.Date(2020, 4, 30, 0, 0, 0, 0, time.UTC)

	assert.Equal(t, 0, LeaveDaysInRange(nil, nil))
	assert.Equal(t, (10 + 12 - 6), LeaveDaysInRange(&feb_19, &mar_13)) // 3 weekends
	assert.Equal(t, 6, LeaveDaysInRange(&apr_04, &apr_16))             // minus weekends & GF & EM
	assert.Equal(t, (10+12-6)+6, LeaveDaysInRange(&feb_19, &apr_16))
	assert.Equal(t, 2, LeaveDaysInRange(&apr_11, &apr_30)) // weekend + easter
}
