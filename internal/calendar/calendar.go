package calendar

import (
	"encoding/json"
	"errors"
	"fmt"
	"sort"
	"strings"
	"time"

	cal "github.com/rickar/cal/v2"
	"github.com/rickar/cal/v2/gb"
	"github.com/shibukawa/configdir"
	lab "github.com/zaquestion/lab/internal/gitlab"
)

type Date struct {
	time.Time
}

func (t *Date) UnmarshalJSON(b []byte) (err error) {
	s := strings.Trim(string(b), "\"")
	if s == "null" {
		t.Time = time.Time{}
		return
	}
	t.Time, err = time.Parse("2006-01-02", s)
	return err
}

func (t *Date) MarshalJSON() ([]byte, error) {
	var nilTime = (time.Time{}).UnixNano()
	if t.Time.UnixNano() == nilTime {
		return []byte("null"), nil
	}
	return []byte(fmt.Sprintf("\"%s\"", t.Time.Format("2006-01-02"))), nil
}

// LeavePeriod - details of a leave period
type LeavePeriod struct {
	Start Date
	End   Date
	Name  string
}

var (
	c           *cal.BusinessCalendar
	leave       []LeavePeriod
	leaveLoaded bool
)

// BusinessDaysInRange - return the number of business days in the range
func BusinessDaysInRange(start, end *time.Time) int {
	if start == nil || end == nil {
		return 0
	}

	return c.WorkdaysInRange(*start, *end)
}

// LeaveDaysInRange - count the number of leave days in the range
func LeaveDaysInRange(start, end *time.Time) int {
	n := 0
	if start == nil || end == nil {
		return n
	}
	var periods []LeavePeriod
	periods = append(periods, LoadLeave()...)

	for d := cal.DayStart(*start); len(periods) > 0 && (d.Before(*end) || d.Equal(*end)); d = d.AddDate(0, 0, 1) {
		period, i := findLeave(periods, d)
		if period == nil {
			continue
		}

		for leaveIncludesTime(*period, d) && (d.Before(*end) || d.Equal(*end)) {
			if c.IsWorkday(d) { // only count leave on days that would otherwise be a work-day
				n++
			}
			d = d.AddDate(0, 0, 1)
		}
		periods = periods[i+1:]
	}

	return n
}

// IsLeave - does this time occur during a leave period?
func IsLeave(t time.Time) bool {
	if !c.IsWorkday(t) {
		return false
	}
	LoadLeave()
	period, _ := findLeave(leave, t)
	return (period != nil)
}

func findLeave(periods []LeavePeriod, t time.Time) (*LeavePeriod, int) {
	if len(periods) == 0 {
		return nil, 0
	}
	i := sort.Search(len(periods), func(i int) bool {
		return periods[i].End.After(t) || periods[i].End.Equal(t)
	})
	if i >= len(periods) {
		return nil, i
	}
	endingAfterT := periods[i:]

	for j, p := range endingAfterT {
		if leaveIncludesTime(p, t) {
			return &p, i + j
		}
	}
	return nil, i
}

func leaveIncludesTime(period LeavePeriod, t time.Time) bool {
	start := cal.DayStart(period.Start.Time)
	end := cal.DayEnd(period.End.Time)

	return (start.Before(t) || start.Equal(t)) && (end.After(t) || end.Equal(t))
}

func init() {
	c = cal.NewBusinessCalendar()
	c.AddHoliday(gb.Holidays...)
	leaveLoaded = false
}

func AddLeave(period LeavePeriod) error {
	LoadLeave()
	leave = append(leave, period)
	return writeLeave()
}

func GetLeave(i int) (LeavePeriod, error) {
	LoadLeave()
	if len(leave) < i+1 {
		return LeavePeriod{}, errors.New("leave does not exist")
	}
	return leave[i], nil
}

func EditLeave(i int, period LeavePeriod) error {
	var emptyLeave LeavePeriod
	currentLeave, err := GetLeave(i)
	if err != nil {
		return err
	}

	if period.Start != emptyLeave.Start {
		currentLeave.Start = period.Start
	}
	if period.End != emptyLeave.End {
		currentLeave.End = period.End
	}
	if period.Name != emptyLeave.Name {
		currentLeave.Name = period.Name
	}

	leave[i] = currentLeave
	return writeLeave()
}

func writeLeave() error {
	folder := configDir()
	data, err := json.Marshal(leave)
	if folder != nil && err == nil {
		folder.WriteFile("leave.json", data)
	}
	return err
}

func configDir() *configdir.Config {
	folder := lab.ConfigDir().QueryFolderContainsFile("leave.json")
	if folder == nil {
		folder = lab.ConfigDir().QueryFolders(configdir.Global)[0]
	}
	return folder
}

func LoadLeave() []LeavePeriod {
	if leaveLoaded {
		return leave
	}

	folder := lab.ConfigDir().QueryFolderContainsFile("leave.json")
	if folder == nil {
		lab.CmdLogger().Infof("No leave configuration file LocalPath: %s", lab.ConfigDir().LocalPath)
	} else {
		data, err := folder.ReadFile("leave.json")
		if err != nil {
			lab.CmdLogger().Warningf("Could not read leave configuration file")
		} else {
			err := json.Unmarshal(data, &leave)
			if err == nil {
				leaveLoaded = true
				compactLeave()
			} else {
				lab.CmdLogger().Warningf("Leave configuration file is not valid JSON")
			}
		}
	}
	return leave
}

func compactLeave() {
	sort.Slice(leave, func(i, j int) bool {
		a := leave[i]
		b := leave[j]
		return a.Start.Before(b.Start.Time)
	})
	// merge overlapping intervals
	var stack []LeavePeriod
	for _, period := range leave {
		if len(stack) == 0 {
			stack = append(stack, period)
		} else {
			n := len(stack) - 1
			top := &stack[n]
			if overlaps(*top, period) {
				top.Name = fmt.Sprintf("%s-%s", top.Name, period.Name)
				top.End = period.End
			} else {
				stack = append(stack, period)
			}
		}
	}
	leave = stack
}

func overlaps(a, b LeavePeriod) bool {
	if a.Start.Equal(b.Start.Time) || a.Start.Equal(b.End.Time) || a.End.Equal(b.Start.Time) || a.End.Equal(b.End.Time) {
		return true
	}
	return a.Start.Before(b.End.Time) && a.End.After(b.Start.Time)
}
