package interactive

import (
	"context"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"

	humanize "github.com/dustin/go-humanize"
	graphql "github.com/machinebox/graphql"
	gitlab "github.com/xanzy/go-gitlab"
	"github.com/zaquestion/lab/internal/browser"
	"github.com/zaquestion/lab/internal/git"
	lab "github.com/zaquestion/lab/internal/gitlab"
	types "github.com/zaquestion/lab/internal/types"
)

var mrListState struct {
	selectedMR          *types.MRListEntry
	mrList              *[]types.MRListEntry
	getMergeRequestList func() ([]types.MRListEntry, error)
}

type mrListApp struct {
	app           *tview.Application
	layout        *tview.Flex
	pages         *tview.Pages
	list          *tview.List
	options       *tview.Modal
	details       *tview.Flex
	mrSummary     *tview.Flex
	mrWorkFlow    *tview.TextView
	mrMilestone   *tview.TextView
	mrAssignees   *tview.TextView
	mrApprovals   *tview.TextView
	mrReviewers   *tview.TextView
	mrBuildStatus *tview.TextView
	mrAge         *tview.TextView
	description   *tview.TextView
	pipelineJobs  *tview.Flex
	webURLView    *tview.TextView
	mrMainPane    *tview.Pages
	reviewerLine  *tview.Flex
	msgPage       *tview.Modal
}

func newMRListApp() *mrListApp {
	var app mrListApp

	return &app
}

// InteractWithMRList start a TUI app for this MR list
func InteractWithMRList(getMergeRequestList func() ([]types.MRListEntry, error)) {
	mrListState.getMergeRequestList = getMergeRequestList

	app := newMRListApp()
	app.init()

	go app.loadMRs()

	app.start()
}

func (mrListApp *mrListApp) start() {
	if err := mrListApp.app.SetRoot(mrListApp.layout, true).SetFocus(mrListApp.layout).Run(); err != nil {
		panic(err)
	}
}

func (mrListApp *mrListApp) init() {
	mrListApp.app = tview.NewApplication()
	mrListApp.layout = tview.NewFlex().SetDirection(tview.FlexColumn)
	mrListApp.pages = tview.NewPages()

	mrListApp.list = tview.NewList()
	mrListApp.list.SetTitle("Open Merge Requests")
	mrListApp.list.SetBorder(true)
	mrListApp.list.SetInputCapture(func(evt *tcell.EventKey) *tcell.EventKey {
		switch evt.Key() {
		// TODO: tab around the app...
		// case tcell.KeyTab: app.SetFocus(details)
		case tcell.KeyRune:
			switch evt.Rune() {
			case 'j':
				return tcell.NewEventKey(tcell.KeyDown, 'j', tcell.ModNone)
			case 'k':
				return tcell.NewEventKey(tcell.KeyUp, 'k', tcell.ModNone)
			}
		}
		return evt
	})
	mrListApp.options = tview.NewModal().SetText("")
	mrListApp.details = tview.NewFlex().SetDirection(tview.FlexRow)
	mrListApp.details.SetBorder(true)

	mrListApp.mrSummary = tview.NewFlex().SetDirection(tview.FlexColumn)
	mrListApp.mrWorkFlow = tview.NewTextView()
	mrListApp.mrMilestone = tview.NewTextView()
	mrListApp.mrAssignees = tview.NewTextView()
	mrListApp.mrApprovals = tview.NewTextView()
	mrListApp.mrReviewers = tview.NewTextView()
	mrListApp.mrBuildStatus = tview.NewTextView()
	mrListApp.webURLView = tview.NewTextView()
	mrListApp.mrAge = tview.NewTextView()
	mrListApp.mrSummary.AddItem(mrListApp.mrWorkFlow, 0, 4, false)
	mrListApp.mrSummary.AddItem(mrListApp.mrMilestone, 0, 2, false)
	mrListApp.mrSummary.AddItem(mrListApp.mrBuildStatus, 0, 1, false)
	mrListApp.mrSummary.AddItem(mrListApp.mrAge, 0, 1, false)
	mrListApp.description = tview.NewTextView()
	mrListApp.pipelineJobs = tview.NewFlex().SetDirection(tview.FlexRow)
	mrListApp.mrMainPane = tview.NewPages()

	mrListApp.mrMainPane.AddPage("description", mrListApp.description, true, true)
	mrListApp.mrMainPane.AddPage("pipelineJobs", mrListApp.pipelineJobs, true, false)

	mrListApp.details.AddItem(mrListApp.mrAssignees, 1, 0, false)
	mrListApp.reviewerLine = tview.NewFlex().SetDirection(tview.FlexColumn)
	mrListApp.details.AddItem(mrListApp.reviewerLine, 1, 0, false)
	mrListApp.reviewerLine.AddItem(mrListApp.mrApprovals, 0, 6, false)
	mrListApp.reviewerLine.AddItem(mrListApp.mrReviewers, 0, 6, false)
	mrListApp.details.AddItem(mrListApp.mrSummary, 1, 0, false)
	mrListApp.details.AddItem(mrListApp.webURLView, 1, 0, false)
	mrListApp.details.AddItem(mrListApp.mrMainPane, 0, 1, false)

	mrListApp.layout.AddItem(mrListApp.pages, 0, 2, true)
	mrListApp.layout.AddItem(mrListApp.details, 0, 3, false)

	mrListApp.msgPage = tview.
		NewModal().
		AddButtons([]string{"OK"}).
		SetDoneFunc(func(idx int, str string) {
			mrListApp.pages.SwitchToPage("list")
		})

	mrListApp.pages.AddPage("list", mrListApp.list, true, true)
	mrListApp.pages.AddPage("options", mrListApp.options, false, false)
	mrListApp.pages.AddPage("errors", mrListApp.msgPage, false, false)

	mrListApp.options.
		AddButtons([]string{"Checkout", "Browse", "Note", "Failed jobs", "Close", "Back"}).
		SetDoneFunc(mrListApp.mrModelClosed)

	mrListApp.list.SetDoneFunc(mrListApp.app.Stop)
	mrListApp.list.SetChangedFunc(mrListApp.onMRFocussed)
}

func (mrListApp *mrListApp) onMRFocussed(idx int, _main string, _summary string, _sc rune) {
	currentMrs := *mrListState.mrList
	mrListState.selectedMR = &currentMrs[idx]
	mrListApp.mrMainPane.SwitchToPage("description")

	mrListApp.details.SetTitle(mrListState.selectedMR.Title)

	mrListApp.mrWorkFlow.Clear()
	for _, label := range mrListState.selectedMR.Labels.Nodes {
		if strings.HasPrefix(label.Title, "workflow::") {
			mrListApp.mrWorkFlow.SetText(" (" + label.Title + ")")
		}
	}

	mrListApp.mrMilestone.SetText(mrListState.selectedMR.Milestone.Title)

	mrListApp.mrAssignees.Clear()
	var mrAssignBuff strings.Builder
	if len(mrListState.selectedMR.Assignees.Nodes) > 0 {
		mrAssignBuff.WriteString("assigned to:")
		mrListApp.mrAssignees.SetTextColor(tcell.ColorDarkBlue)
	} else {
		mrAssignBuff.WriteString("unassigned")
		mrListApp.mrAssignees.SetTextColor(tcell.ColorGrey)
	}
	for _, assignee := range mrListState.selectedMR.Assignees.Nodes {
		mrAssignBuff.WriteString(" " + assignee.Username)
	}
	mrListApp.mrAssignees.SetText(mrAssignBuff.String())

	mrListApp.mrApprovals.Clear()
	var mrAppBuff strings.Builder
	approvedBy := make(map[string]bool)
	if len(mrListState.selectedMR.ApprovedBy.Nodes) > 0 {
		mrAppBuff.WriteString("[✓]")
		mrListApp.mrApprovals.SetTextColor(tcell.ColorGreen)
	} else {
		mrAppBuff.WriteString("[x] not approved yet")
		mrListApp.mrApprovals.SetTextColor(tcell.ColorGrey)
	}
	for _, approver := range mrListState.selectedMR.ApprovedBy.Nodes {
		approvedBy[approver.Username] = true
		mrAppBuff.WriteString(" " + approver.Username)
	}
	mrListApp.mrApprovals.SetText(mrAppBuff.String())

	var rbuff strings.Builder
	for _, u := range mrListState.selectedMR.Reviewers.Nodes {
		if approvedBy[u.Username] {
			continue
		}
		rbuff.WriteString(" " + u.Username)
	}
	mrListApp.mrReviewers.SetText(rbuff.String())

	pipeline := mrListState.selectedMR.HeadPipeline
	if pipeline != nil {
		status := strings.ToLower(pipeline.Status)
		mrListApp.mrBuildStatus.SetText(status)

		switch gitlab.BuildStateValue(status) {
		case gitlab.Failed:
			mrListApp.mrBuildStatus.SetTextColor(tcell.ColorRed)
		case lab.Success:
			mrListApp.mrBuildStatus.SetTextColor(tcell.ColorGreen)
		case gitlab.Success:
			mrListApp.mrBuildStatus.SetTextColor(tcell.ColorGreen)
		default:
			mrListApp.mrBuildStatus.SetTextColor(tcell.ColorGrey)
		}
	}

	days := mrListState.selectedMR.Age()
	mrListApp.mrAge.SetText(fmt.Sprintf("[%d days]", days))
	if days > 14 {
		mrListApp.mrAge.SetTextColor(tcell.ColorRed)
	} else if days > 10 {
		mrListApp.mrAge.SetTextColor(tcell.ColorOrange)
	} else if days > 7 {
		mrListApp.mrAge.SetTextColor(tcell.ColorBlue)
	} else {
		mrListApp.mrAge.SetTextColor(tcell.ColorGreen)
	}

	mrListApp.description.SetText(mrListState.selectedMR.Description).ScrollToBeginning()
	mrListApp.description.SetTextColor(tcell.ColorLightGray)
	mrListApp.description.SetBackgroundColor(tcell.ColorDarkSlateGrey)

	mrListApp.webURLView.SetText(mrListState.selectedMR.WebUrl)
}

func (mrListApp *mrListApp) showMessageAndReload(str string) {
	go mrListApp.loadMRs()
	mrListApp.msgPage.SetText(str)
	mrListApp.pages.SwitchToPage("list")
	mrListApp.pages.ShowPage("errors")
}

func (mrListApp *mrListApp) mrModelClosed(idx int, str string) {
	switch str {
	case "Browse":
		browser.Open(mrListState.selectedMR.WebUrl)
		mrListApp.app.Stop()
	case "Note":
		err := mrListApp.addNoteToMR()
		if err != nil {
			mrListApp.showError(err)
		} else {
			mrListApp.showMessageAndReload(fmt.Sprintf("Added note to %s", mrListState.selectedMR.Reference))
		}
	case "Checkout":
		mrListApp.checkoutMR()
	case "Close":
		err := closeMR()
		if err != nil {
			mrListApp.showError(err)
		} else {
			mrListApp.showMessageAndReload(fmt.Sprintf("Closed %s", mrListState.selectedMR.Reference))
		}
	case "Failed jobs":
		go mrListApp.loadFailedJobs()
		mrListApp.pages.SwitchToPage("list")
		mrListApp.mrMainPane.SwitchToPage("pipelineJobs")
	default:
		mrListApp.pages.SwitchToPage("list")
	}
}

func (mrListApp *mrListApp) loadFailedJobs() {
	query := `
		query($id: MergeRequestID!) {
			mergeRequest(id: $id) {
				headPipeline {
					status
					finishedAt startedAt createdAt duration
					passed: jobs(statuses: [SUCCESS]) {
					  count
					}
					total: jobs {
					  count
					}
					jobs(statuses: [FAILED]) {
					  nodes { name stage { name } allowFailure }
					}
				}
			}
		}
	`

	mrListApp.pipelineJobs.Clear()
	req := graphql.NewRequest(query)
	mrid := mrListState.selectedMR.ID
	req.Var("id", &mrid)

	ctx := context.Background()
	var resp struct {
		MergeRequest struct {
			HeadPipeline struct {
				Status     string
				FinishedAt *time.Time
				StartedAt  *time.Time
				CreatedAt  *time.Time
				Duration   *int
				Passed     struct {
					Count int
				}
				Total struct {
					Count int
				}
				Jobs struct {
					Nodes []struct {
						Name         string
						AllowFailure bool
						Stage        struct {
							Name string
						}
					}
				}
			}
		}
	}
	client := lab.GQLClient()
	err := client.Run(ctx, req, &resp)

	if err != nil {
		mrListApp.showError(err)
		return
	}

	mrListApp.app.QueueUpdateDraw(func() {
		pipeline := resp.MergeRequest.HeadPipeline
		summary := tview.NewFlex().SetDirection(tview.FlexColumn)
		summary.AddItem(tview.NewTextView().SetText(pipeline.Status), 0, 1, false)
		summary.AddItem(tview.NewTextView().SetText(fmt.Sprintf("Passed: %d/%d", pipeline.Passed.Count, pipeline.Total.Count)), 0, 1, false)
		if pipeline.CreatedAt != nil {
			summary.AddItem(tview.NewTextView().SetText("Created "+layoutTime(*pipeline.CreatedAt)), 0, 1, false)
		}
		if pipeline.FinishedAt != nil {
			summary.AddItem(tview.NewTextView().SetText("Finished "+layoutTime(*pipeline.FinishedAt)), 0, 1, false)
		}
		mrListApp.pipelineJobs.AddItem(summary, 1, 1, false)

		if len(pipeline.Jobs.Nodes) == 0 {
			mrListApp.pipelineJobs.AddItem(tview.NewTextView().SetText("No Failed jobs").SetTextColor(tcell.ColorGreen), 1, 1, false)
		} else {
			mrListApp.pipelineJobs.AddItem(tview.NewTextView().SetText("Failed jobs:").SetTextColor(tcell.ColorRed), 1, 1, false)
			failed := tview.NewTable()
			failed.SetCellSimple(0, 0, "Stage")
			failed.SetCellSimple(0, 1, "Job")
			failed.SetCellSimple(0, 2, "Failure allowed?")
			for i, job := range pipeline.Jobs.Nodes {
				failed.SetCellSimple(i+1, 0, job.Stage.Name)
				failed.SetCellSimple(i+1, 1, job.Name)
				failed.SetCellSimple(i+1, 2, fmt.Sprintf("%v", job.AllowFailure))
			}
			mrListApp.pipelineJobs.AddItem(failed, 0, 1, false)
		}
	})
}

func layoutTime(when time.Time) string {
	var layout string
	if time.Since(when).Hours() < 12 && time.Now().YearDay() == when.YearDay() {
		layout = time.Kitchen
	} else {
		layout = time.Stamp
	}
	zoneName, _ := when.Zone()
	return fmt.Sprintf("%s %s %s", when.Format(layout), zoneName, sinceMessage(when))
}

func sinceMessage(moment time.Time) string {
	ago := time.Since(moment)
	if ago.Seconds() < 1.0 {
		return ""
	}
	return fmt.Sprintf("(%s)", humanize.Time(moment))
}

func (mrListApp *mrListApp) selectMR(mr types.MRListEntry) {
	mrListState.selectedMR = &mr
	mrListApp.options.SetText(mr.Title)
	mrListApp.pages.SendToFront("options")
	mrListApp.pages.ShowPage("options")
}

func (mrListApp *mrListApp) checkoutMR() {
	mrListApp.app.Stop()
	// TODO: also fetch
	if err := git.New("checkout", mrListState.selectedMR.SourceBranch).Run(); err != nil {
		log.Fatal(err)
	}
}

func closeMR() error {
	iid, err := strconv.Atoi(mrListState.selectedMR.IID)
	if err != nil {
		return err
	}
	return lab.MRClose(mrListState.selectedMR.Project.FullPath, iid)
}

func (mrListApp *mrListApp) loadMRs() {
	mrListApp.list.Clear()
	newMRs, err := mrListState.getMergeRequestList()
	if err != nil {
		mrListApp.showError(err)
		return
	}

	mrListState.mrList = &newMRs
	for idx, mr := range newMRs {
		mrListApp.setUpInteractiveListEntry(idx, mr)
	}
}

func (mrListApp *mrListApp) showError(err error) {
	mrListApp.msgPage.SetText(fmt.Sprintf("Error: %s", err))
	mrListApp.msgPage.SetDoneFunc(func(idx int, str string) { mrListApp.app.Stop() })
	mrListApp.pages.SwitchToPage("errors")
}

func (mrListApp *mrListApp) addNoteToMR() error {
	iid, err := strconv.Atoi(mrListState.selectedMR.IID)
	if err != nil {
		return err
	}
	mrListApp.app.Suspend(func() {
		hint, e := git.EditTextWithFiletype("A note for "+mrListState.selectedMR.Reference, "vimwiki")
		if e != nil {
			err = e
			return
		}
		body, e := git.EditFile("NOTE_MSG", hint)
		if e != nil {
			err = e
			return
		}
		_, _, e = lab.MRCreateNote(mrListState.selectedMR.Project.FullPath, "", iid, &gitlab.CreateMergeRequestNoteOptions{
			Body: &body,
		})
		err = e
	})
	return err
}

func (mrListApp *mrListApp) setUpInteractiveListEntry(idx int, mr types.MRListEntry) {
	mrListApp.app.QueueUpdateDraw(func() {
		title := fmt.Sprintf("%s %s: %s", mr.HeadPipeline.StatusMessage(true), mr.IID, mr.Title)
		mrListApp.list.AddItem(title, interactiveSummary(mr), rune(int('a')+idx), func() {
			mrListApp.selectMR(mr)
		})
	})
}

func interactiveSummary(mr types.MRListEntry) string {
	var summary strings.Builder
	summary.WriteString(mr.SourceBranch)
	if mr.TargetBranch != "master" {
		summary.WriteString("=>" + mr.TargetBranch)
	}

	return summary.String()
}
