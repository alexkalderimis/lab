package interactive

import (
	"fmt"
	"strings"

	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
	"github.com/zaquestion/lab/internal/notes"
)

const systemNotesTitle = "system notes"

type mrNotesApp struct {
	app             *tview.Application
	discussions     *notes.Discussions
	layout          *tview.Flex
	pages           *tview.Pages
	menu            *tview.Modal
	errors          *tview.Modal
	discussionsPane *tview.List
	notesPane       *tview.Flex
	systemNotes     []notes.Note
	threads         [][]notes.Note
}

func newMRNotesApp(discussions *notes.Discussions) *mrNotesApp {
	app := mrNotesApp{discussions: discussions}

	return &app
}

// MRNotesApp run an MR Notes interactive app
func MRNotesApp(discussions *notes.Discussions) {
	app := newMRNotesApp(discussions)
	app.init()

	app.load()

	app.start()
}

func (app *mrNotesApp) load() {
	app.systemNotes = []notes.Note{}
	app.threads = [][]notes.Note{}
	app.discussions.Reset()
	app.discussionsPane.Clear()

	for {
		d, err := app.discussions.NextDiscussion()

		if err != nil {
			app.errors.SetText(err.Error())
			app.pages.SwitchToPage("errors")
			break
		}
		if d == nil {
			break
		}

		app.addDiscussion(*d)
	}
	app.discussionsPane.AddItem(systemNotesTitle, fmt.Sprintf("%d events", len(app.systemNotes)), '-', func() {
		app.app.SetFocus(app.notesPane)
	})
}

func (app *mrNotesApp) addDiscussion(d notes.Discussion) {
	if isSystem(d) {
		for _, note := range d.Notes.Nodes {
			app.systemNotes = append(app.systemNotes, note)
		}
		return
	}
	app.threads = append(app.threads, d.Notes.Nodes)

	//view := discussionView(d)
	details := fmt.Sprintf("%d notes", len(d.Notes.Nodes))
	var summary strings.Builder
	authors := make(map[string]bool)

	for _, note := range d.Notes.Nodes {
		authors[note.Author.Username] = true
	}

	for name := range authors {
		if summary.Len() > 0 {
			summary.WriteString(", ")
		}
		summary.WriteString(name)
	}

	r := shortcut(app.discussionsPane.GetItemCount())

	app.discussionsPane.AddItem(summary.String(), details, r, func() {
		app.app.SetFocus(app.notesPane)
	})
}

func isSystem(d notes.Discussion) bool {
	for _, note := range d.Notes.Nodes {
		if !note.System {
			return false
		}
	}

	return true
}

func shortcut(idx int) rune {
	if idx <= 25 {
		return rune(int('a') + idx)
	}
	return rune(int('A') + idx - 26)
}

func discussionView(d notes.Discussion) tview.Primitive {
	view := tview.NewGrid()
	view.AddItem(tview.NewTextView().SetText(fmt.Sprintf("%d notes", len(d.Notes.Nodes))),
		0, 0, 1, 1, 0, 0, false)
	resolver := tview.NewButton("Resolve")
	view.AddItem(resolver, 0, 1, 1, 1, 0, 0, false)

	if d.Resolved {
		resolver.SetLabel("Resolved")
	}
	resolver.SetSelectedFunc(func() {
		// TODO: resolve: d.ID
	})

	for i, note := range d.Notes.Nodes {
		view.AddItem(noteView(note), 1+i, 0, 1, 1, 0, 0, true)
	}

	return view
}

func noteView(n notes.Note) tview.Primitive {
	v := tview.NewFlex().SetDirection(tview.FlexRow)

	meta := tview.NewFlex().SetDirection(tview.FlexColumn)
	meta.AddItem(tview.NewTextView().SetText("@"+n.Author.Username), 0, 1, false)
	if n.Resolved {
		resolved := tview.NewTextView().SetText("RESOLVED")
		resolved.SetTextColor(tcell.ColorGreen)

		meta.AddItem(resolved, 0, 1, false)
	}

	body := tview.NewTextView()
	body.SetText(n.Body)
	body.SetBorder(true)

	v.AddItem(meta, 1, 0, false)
	v.AddItem(body, 0, 1, false)

	return v
}

func (app *mrNotesApp) init() {
	app.app = tview.NewApplication()
	app.layout = tview.NewFlex().SetDirection(tview.FlexColumn)
	app.pages = tview.NewPages()
	app.discussionsPane = tview.NewList()
	app.notesPane = tview.NewFlex().SetDirection(tview.FlexRow)

	app.discussionsPane.SetTitle("discussions")
	app.discussionsPane.SetBorder(true)
	app.discussionsPane.SetInputCapture(func(evt *tcell.EventKey) *tcell.EventKey {
		switch evt.Key() {
		// TODO: tab around the app...
		// case tcell.KeyTab: app.SetFocus(details)
		case tcell.KeyRune:
			switch evt.Rune() {
			case 'j':
				return tcell.NewEventKey(tcell.KeyDown, 'j', tcell.ModNone)
			case 'k':
				return tcell.NewEventKey(tcell.KeyUp, 'k', tcell.ModNone)
			}
		}
		return evt
	})
	app.discussionsPane.SetChangedFunc(func(i int, title string, _d string, _r rune) {
		if title == systemNotesTitle {
			app.showNotes(app.systemNotes)
		} else if i < len(app.threads) {
			app.showNotes(app.threads[i])
		}
	})

	app.notesPane.SetTitle("notes")
	app.notesPane.SetBorder(true)

	app.errors = tview.NewModal().
		AddButtons([]string{"OK"}).
		SetDoneFunc(func(i int, s string) {
			app.pages.SwitchToPage("notes")
		})

	app.menu = tview.NewModal().SetText("Menu")
	app.menu.
		AddButtons([]string{"Resolve", "Reply", "Thumb", "Edit", "Back"}).
		SetDoneFunc(app.doMenuSelection)

	app.pages.AddPage("notes", app.layout, true, true)
	app.pages.AddPage("menu", app.menu, false, false)
	app.pages.AddPage("errors", app.errors, false, false)

	app.layout.AddItem(app.discussionsPane, 0, 2, true)
	app.layout.AddItem(app.notesPane, 0, 3, true)
}

func (app *mrNotesApp) showNotes(notes []notes.Note) {
	app.notesPane.Clear()

	for _, note := range notes {
		func() {
			view := noteView(note)

			app.notesPane.AddItem(view, 0, 1, true)
		}()
	}
}

func (app *mrNotesApp) doMenuSelection(idx int, action string) {
	switch action {
	case "Resolve":
		// TODO
	case "Reply":
		// TODO
	case "Thumb":
		// TODO
	case "Edit":
		// TODO
	case "Back":
		app.pages.SwitchToPage("notes")
	}
}

func (app *mrNotesApp) start() {
	app.pages.SwitchToPage("notes")
	if err := app.app.SetRoot(app.pages, true).SetFocus(app.layout).Run(); err != nil {
		panic(err)
	}
}

func (app *mrNotesApp) Stop() {
	app.app.Stop()
}
