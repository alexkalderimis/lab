package types

import (
	"math"
	"strings"
	"time"

	gitlab "github.com/xanzy/go-gitlab"
	gid "github.com/zaquestion/lab/internal/gid"
	lab "github.com/zaquestion/lab/internal/gitlab"
)

// MergeStatus How mergeable something is
type MergeStatus (string)

// CanBeMerged It can be merged
const CanBeMerged = MergeStatus("CAN_BE_MERGED")

// Unchecked We have not checked yet
const Unchecked = MergeStatus("UNCHECKED")

// Checking We are checking now
const Checking = MergeStatus("CHECKING")

// CannotBeMerged We have checked, it cannot be merged
const CannotBeMerged = MergeStatus("CANNOT_BE_MERGED")

// Recheck It cannot be merged, recheck is needed
const Recheck = MergeStatus("CANNOT_BE_MERGED_RECHECK")

// PageInfo Connection pagination information
type PageInfo struct {
	HasNextPage bool
	EndCursor   *string
}

// NamedUser Representation of a user
type NamedUser struct {
	Username string
}

// Titled Representation of a thing with a title (e.g. milestone)
type Titled struct {
	Title string
}

// Project A basic representation of a project
type Project struct {
	Name     string
	FullPath string
	ID       gid.GID
}

// MRListEntry A representation of a MR
type MRListEntry struct {
	ID             gid.GID
	IID            string
	Reference      string
	ShortRef       string
	Title          string
	SourceBranch   string
	TargetBranch   string
	State          string
	MergeStatus    MergeStatus
	WebUrl         string
	Description    string
	WorkInProgress bool
	CreatedAt      *time.Time
	UpdatedAt      *time.Time
	HeadPipeline   *PipelineNode
	Milestone      Titled
	Author         NamedUser
	Assignees      struct {
		Nodes []NamedUser
	}
	Reviewers struct {
		Nodes []NamedUser
	}
	ApprovedBy struct {
		Nodes []NamedUser
	}
	Labels struct {
		Nodes []Titled
	}
	Project struct {
		ID       gid.GID
		Name     string
		FullPath string
	}
	DiffStatsSummary struct {
		Additions int
		Deletions int
		FileCount int
	}
}

func (mr MRListEntry) Age() int {
	if mr.CreatedAt == nil {
		return 0
	}
	return int(math.Floor(time.Now().Sub(*mr.CreatedAt).Hours() / 24))
}

// JobNode Representation of a job
type JobNode struct {
	DetailedStatus struct {
		Status string
	}
}

// PipelineNode Representation of a pipeline
type PipelineNode struct {
	Status     string
	FinishedAt *time.Time
	CreatedAt  *time.Time
	Duration   int
	Total      struct {
		Count int
	}
	Success struct {
		Count int
	}
	Failed struct {
		Count int
	}
	Jobs struct {
		Nodes []JobNode
	}
}

func (p *PipelineNode) BuildState() gitlab.BuildStateValue {
	if p == nil {
		return gitlab.BuildStateValue("unknown")
	}

	return gitlab.BuildStateValue(strings.ToLower(p.Status))
}

func (p *PipelineNode) StatusMessage(symbolic bool) string {
	if !symbolic {
		return strings.ToUpper(p.Status)
	}

	switch p.BuildState() {
	case gitlab.Failed:
		return "🔴"
	case lab.Success:
		return "✅"
	case gitlab.Success:
		return "✅"
	case gitlab.Running:
		return "🕞"
	case gitlab.Created:
		return "🌱"
	case gitlab.Pending:
		return "🌱"
	case gitlab.Skipped:
		return "⏭️"
	default:
		return "❔"
	}
}
