package gid

import (
	"encoding/json"
	"testing"

	assert "github.com/stretchr/testify/assert"
)

func TestParseGID(t *testing.T) {
	input := `{"gid": "gid://gitlab/Project/123"}`
	bytes := []byte(input)
	expected := GID{
		App:   "gitlab",
		Model: "Project",
		ID:    "123",
	}

	var res struct {
		GID GID
	}

	err := json.Unmarshal(bytes, &res)

	assert.Nil(t, err)
	assert.Equal(t, expected, res.GID)
}

func TestParseNullGID(t *testing.T) {
	input := `{"gid": null}`
	bytes := []byte(input)
	expected := GID{}

	var res struct {
		GID GID
	}

	err := json.Unmarshal(bytes, &res)

	assert.Nil(t, err)
	assert.Equal(t, expected, res.GID)
}
