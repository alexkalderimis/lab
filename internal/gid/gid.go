package gid

import (
	"fmt"
	"io"
	"strconv"
	"strings"
)

type PathSegment string

func (s *PathSegment) Scan(state fmt.ScanState, verb rune) error {
	var runes []rune
	for {
		r, _, err := state.ReadRune()
		if err == io.EOF || r == '/' {
			state.UnreadRune()
			*s = PathSegment(string(runes))
			return nil
		}
		if err != nil {
			return err
		}
		runes = append(runes, r)
	}
}

/* GID A Global Identifier */
type GID struct {
	App   string
	Model string
	ID    string
}

type GIDFactory GID

func (f *GIDFactory) Create(id int64) GID {
	return GID{App: f.App, Model: f.Model, ID: fmt.Sprintf("%d", id)}
}

func Factory(app, model string) GIDFactory {
	return GIDFactory(GID{App: app, Model: model})
}

func (g *GID) UnmarshalJSON(b []byte) (err error) {
	s := strings.Trim(string(b), "\"")
	if s == "null" {
		g.App = ""
		g.Model = ""
		g.ID = ""
		return
	}
	var app, model, id PathSegment
	n, err := fmt.Sscanf(s, "gid://%s/%s/%s", &app, &model, &id)
	if n == 3 {
		g.App = string(app)
		g.Model = string(model)
		g.ID = string(id)
	}

	return err
}

func (g GID) MarshalJSON() ([]byte, error) {
	str := fmt.Sprintf("\"gid://%s/%s/%s\"", g.App, g.Model, g.ID)
	return []byte(str), nil
}

func (g *GID) IntegerID() (int64, error) {
	i, err := strconv.ParseInt(g.ID, 10, 64)
	return i, err
}
