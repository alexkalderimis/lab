package notes

import (
	"time"

	gid "github.com/zaquestion/lab/internal/gid"
	"github.com/zaquestion/lab/internal/types"
)

// Discussion A Discussion on a noteable
type Discussion struct {
	ID       gid.GID
	ReplyID  gid.GID
	Resolved bool
	Notes    struct {
		Nodes []Note
	}
}

// Note a Note on a noteable
type Note struct {
	Author    types.NamedUser
	Body      string
	ID        gid.GID
	CreatedAt time.Time
	System    bool
	Resolved  bool
	WebURL    string
	Position  *struct {
		DiffRefs struct {
			HeadSHA string
		}
		NewPath string
		NewLine *int
	}
}
