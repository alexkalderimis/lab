package notes

import (
	"context"
	"fmt"

	"github.com/machinebox/graphql"

	"github.com/zaquestion/lab/internal/gid"
	lab "github.com/zaquestion/lab/internal/gitlab"
	"github.com/zaquestion/lab/internal/types"
)

// TODO: use generics here?

// Discussions an iterator over discussions
type Discussions struct {
	idx      int
	done     bool
	mrID     gid.GID
	items    []Discussion
	pageInfo *types.PageInfo
}

// Notes an iterator over notes
type Notes struct {
	idx      int
	done     bool
	mrID     gid.GID
	items    []Note
	pageInfo *types.PageInfo
}

// MRDiscussions construct a new iterator
func MRDiscussions(mrID gid.GID) Discussions {
	return Discussions{mrID: mrID}
}

// MRNotes construct a new iterator
func MRNotes(mrID gid.GID) Notes {
	return Notes{mrID: mrID}
}

// Reset iterator, ensuring that fresh data will be fetched.
func (ds *Discussions) Reset() {
	*ds = MRDiscussions(ds.mrID)
}

// NextDiscussion get the next discussion
func (ds *Discussions) NextDiscussion() (*Discussion, error) {
	if ds.pageInfo == nil || (ds.pageInfo.HasNextPage && ds.idx >= len(ds.items)) {
		err := ds.fetchDiscussions()
		if err != nil {
			return nil, err
		}
	}

	if !ds.done && ds.idx < len(ds.items) {
		d := ds.items[ds.idx]
		ds.idx++
		return &d, nil
	}

	return nil, nil
}

// NextNote get the next discussion
func (it *Notes) NextNote() (*Note, error) {
	if it.pageInfo == nil || (it.pageInfo.HasNextPage && it.idx >= len(it.items)) {
		err := it.fetchNotes()
		if err != nil {
			return nil, err
		}
	}

	for !it.done && it.idx < len(it.items) {
		item := it.items[it.idx]
		it.idx++
		return &item, nil
	}

	return nil, nil
}

func (it *Notes) fetchNotes() error {
	client := lab.GQLClient()
	ctx, cancel := context.WithCancel(context.Background())

	defer cancel()

	req := graphql.NewRequest(noteQuery)
	req.Var("id", it.mrID)
	if it.pageInfo != nil && it.pageInfo.EndCursor != nil {
		req.Var("cursor", it.pageInfo.EndCursor)
	}

	var resp struct {
		MergeRequest struct {
			Notes struct {
				PageInfo types.PageInfo
				Nodes    []Note
			}
		}
	}

	err := client.Run(ctx, req, &resp)
	if err != nil {
		it.done = true
		return err
	}

	it.idx = 0
	it.items = resp.MergeRequest.Notes.Nodes
	it.pageInfo = &resp.MergeRequest.Notes.PageInfo

	return nil
}

func (ds *Discussions) fetchDiscussions() error {
	client := lab.GQLClient()
	ctx, cancel := context.WithCancel(context.Background())

	defer cancel()

	req := graphql.NewRequest(discussionQuery)
	req.Var("id", ds.mrID)
	if ds.pageInfo != nil && ds.pageInfo.EndCursor != nil {
		req.Var("cursor", ds.pageInfo.EndCursor)
	}

	var resp struct {
		MergeRequest struct {
			Discussions struct {
				PageInfo types.PageInfo
				Nodes    []Discussion
			}
		}
	}

	err := client.Run(ctx, req, &resp)
	if err != nil {
		ds.done = true
		return err
	}

	ds.idx = 0
	ds.items = resp.MergeRequest.Discussions.Nodes
	ds.pageInfo = &resp.MergeRequest.Discussions.PageInfo

	return nil
}

var noteQuery = fmt.Sprintf(`
query($id: MergeRequestID!, $cursor: String) {
  mergeRequest(id: $id) {
	  webUrl
		notes(first: 100, after: $cursor) {
		  pageInfo { ... pagingForwards }
			nodes { ...noteFields }
		}
	}
}

%s

%s
`, pagingFrag, noteFrag)

var discussionQuery = fmt.Sprintf(`
query($id: MergeRequestID!, $cursor: String) {
  mergeRequest(id: $id) {
	  webUrl
		discussions(first: 100, after: $cursor) {
		  pageInfo { ... pagingForwards }
		  nodes {
			  id
			  replyId
				resolved
				notes {
				  nodes { ...noteFields }
				}
			}
		}
	}
}

%s

%s
`, pagingFrag, noteFrag)

const pagingFrag = `
fragment pagingForwards on PageInfo {
  endCursor hasNextPage
}
`

const noteFrag = `
fragment noteFields on Note {
	author { ... namedUser }
	system
	resolved
	body
	createdAt
	id
	resolved
	position { ... pos }
	WebURL: url
}

fragment pos on DiffPosition {
	diffRefs { headSha }
	newPath
	newLine
}

fragment namedUser on User {
  username
}
`
