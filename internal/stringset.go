package stringset

type StringSet struct {
	trie map[rune]*StringSet
}

func Singleton(s string) StringSet {
	var set StringSet
	set.Add(s)
	return set
}

func (set *StringSet) Contains(item string) bool {
	runes := []rune(item)
	if len(runes) == 0 {
		return true
	}
	s := set
	ok := true
	for i, char := range runes {
		s, ok = s.trie[char]
		if !ok {
			return false
		}
		if i+1 == len(runes) {
			return true
		}
	}
	return false
}

func (set *StringSet) Add(item string) {
	runes := []rune(item)
	if len(runes) == 0 {
		return
	}
}
