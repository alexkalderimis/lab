package gitlab

import (
	"os"
	time "time"
)

func accessTime(file os.FileInfo) time.Time {
	return file.ModTime()
}
