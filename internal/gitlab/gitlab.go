// Package gitlab is an internal wrapper for the go-gitlab package
//
// Most functions serve to expose debug logging if set and accept a project
// name string over an ID.
package gitlab

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	time "time"

	color "github.com/fatih/color"
	"github.com/hashicorp/go-cleanhttp"
	retryablehttp "github.com/hashicorp/go-retryablehttp"
	"github.com/juju/loggo"
	"github.com/juju/loggo/loggocolor"
	graphql "github.com/machinebox/graphql"
	"github.com/pkg/errors"
	configdir "github.com/shibukawa/configdir"
	"github.com/spf13/viper"
	gitlab "github.com/xanzy/go-gitlab"
	"github.com/zaquestion/lab/internal/git"
)

var (
	// ErrProjectNotFound is returned when a GitLab project cannot be found.
	ErrProjectNotFound = errors.New("gitlab project not found")
)

var (
	lab              *gitlab.Client
	host             string
	user             string
	token            string
	UseColor         bool // should colour be used?
	cmdLogger        = loggo.GetLogger("cmd")
	failed           = color.New(color.FgRed)
	passed           = color.New(color.FgGreen)
	skipped          = color.New(color.FgYellow)
	running          = color.New(color.FgBlue)
	created          = color.New(color.FgMagenta)
	defaultPrinter   = color.New(color.FgBlack)
	graphqlClient    *graphql.Client
	cacheDirectories = map[string]minutes{
		"":        24 * 7.0 * 60,
		"ttl_10m": 10.0,
		"ttl_5m":  5.0,
		"ttl_1m":  1.0,
	}
	lastCachePrune     *time.Time
	cachingDisabled    = false
	loggingInitialized = false
)

const Success gitlab.BuildStateValue = "passed"
const None gitlab.BuildStateValue = "__none__"

type BuildStateValue gitlab.BuildStateValue

func (v *BuildStateValue) UnmarshalJSON(data []byte) error {
	var s string
	if err := json.Unmarshal(data, &s); err != nil {
		return err
	}
	*v = BuildStateValue(gitlab.BuildStateValue(strings.ToLower(s)))

	return nil
}

func StatusColor(status string) *color.Color {
	return BuildStateColor(gitlab.BuildStateValue(status))
}

func (v BuildStateValue) ColorString() string {
	s := string(v)
	color := StatusColor(s)

	return color.Sprintf(s)
}

func BuildStateColor(status gitlab.BuildStateValue) *color.Color {
	if !UseColor {
		return defaultPrinter
	}

	switch status {
	case gitlab.Failed:
		return failed
	case Success:
		return passed
	case gitlab.Success:
		return passed
	case gitlab.Running:
		return running
	case gitlab.Created:
		return created
	case gitlab.Pending:
		return created
	case gitlab.Skipped:
		return skipped
	default:
		return defaultPrinter
	}
}

// Host exposes the GitLab scheme://hostname used to interact with the API
func Host() string {
	return host
}

// User exposes the configured GitLab user
func User() string {
	return user
}

// get the current user ID
func UserID() (int, error) {
	filename := "current_user.json"
	getUser := func() (*gitlab.User, error) {
		u, _, err := lab.Users.CurrentUser()
		return u, err
	}
	return userIdWithCache(filename, getUser)
}

func UserIDByUserName(username string) (int, error) {
	filename := fmt.Sprintf("user_%s.json", username)
	getUser := func() (*gitlab.User, error) {
		opts := gitlab.ListUsersOptions{
			ListOptions: gitlab.ListOptions{
				PerPage: 1,
			},
			Username: &username,
		}
		users, _, err := lab.Users.ListUsers(&opts)
		if err != nil {
			return nil, err
		} else {
			for _, user := range users {
				return user, nil
			}
		}
		return nil, errors.New("No user found with username " + username)
	}
	return userIdWithCache(filename, getUser)
}

func userIdWithCache(filename string, fn func() (*gitlab.User, error)) (int, error) {
	inCache, bytes, err := ReadCacheTouch(filename, false)
	if inCache && err == nil {
		user := gitlab.User{}
		err := json.Unmarshal(bytes, &user)
		if err == nil {
			return user.ID, nil
		}
	}
	u, err := fn()
	if err != nil {
		return 0, err
	} else {
		bytes, err := json.Marshal(u)
		if err == nil {
			WriteCache(filename, bytes)
		}

		return u.ID, nil
	}
}

func Client() *gitlab.Client {
	return lab
}

func InitCache() {
	if cacheDir != nil {
		return
	}

	configDirs = configdir.New("zaquestion", "lab-cli")
	cacheDir = configDirs.QueryCacheFolder()
	cacheDir.MkdirAll()
	noCache := os.Getenv("LAB_NO_CACHE")
	if noCache != "" && noCache != "false" {
		cachingDisabled = true
	}
}

// Init initializes a gitlab client for use throughout lab.
func Init(_host, _user, _token string) {
	initLogger()
	InitCache()
	cmdLogger.Debugf("initializing")
	defaultPrinter.DisableColor()
	if len(_host) > 0 && _host[len(_host)-1:][0] == '/' {
		_host = _host[0 : len(_host)-1]
	}
	host = _host
	user = _user
	token = _token
	lab = gitlab.NewClient(gitlabHttpClient(), token)
	if lab.BaseURL().String() != (_host + "/api/v4/") {
		cmdLogger.Debugf("Changing BaseURL from %s to %s", lab.BaseURL().String(), _host)
		lab.SetBaseURL(host + "/api/v4")
	}
	graphqlClient = graphql.NewClient(host+"/api/graphql", withToken())
	graphqlClient.Log = func(s string) {
		cmdLogger.Debugf(s)
	}
	viper.SetDefault("edit.filetype", "markdown")
}

func ConfigDir() *configdir.ConfigDir {
	return &configDirs
}

func gitlabHttpClient() *http.Client {
	specification := os.Getenv("LAB_LOGGO_SPEC")
	if specification == "" {
		return nil
	}
	wrapped := cleanhttp.DefaultPooledClient()
	wrapped.Transport = loggingTransport(wrapped.Transport)

	return wrapped
}

func GQLClient() *graphql.Client {
	return graphqlClient
}

func withToken() graphql.ClientOption {
	client := http.DefaultClient

	rt := WithHeader(client.Transport)
	rt.Set("Authorization", "Bearer "+token)
	client.Transport = rt

	return graphql.WithHTTPClient(client)
}

type withHeader struct {
	http.Header
	rt http.RoundTripper
}

type LoggingTransport struct {
	logger loggo.Logger
	rt     http.RoundTripper
}

func loggingTransport(inner http.RoundTripper) LoggingTransport {
	return LoggingTransport{
		logger: loggo.GetLogger("http"),
		rt:     inner,
	}
}

func (wl LoggingTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	q := req.URL.Query()
	if len(q) > 0 {
		wl.logger.Debugf("HTTP Request %s path=%s, query=%s", req.Method, req.URL.Path, q)
	} else {
		wl.logger.Debugf("HTTP Request %s path=%s", req.Method, req.URL.Path)
	}

	return wl.rt.RoundTrip(req)
}

func WithHeader(rt http.RoundTripper) withHeader {
	if rt == nil {
		rt = http.DefaultTransport
	}

	return withHeader{Header: make(http.Header), rt: rt}
}

func (h withHeader) RoundTrip(req *http.Request) (*http.Response, error) {
	for k, v := range h.Header {
		req.Header[k] = v
	}

	return h.rt.RoundTrip(req)
}

func initLogger() {
	if loggingInitialized {
		return
	}

	loggo.ReplaceDefaultWriter(loggocolor.NewWriter(os.Stderr))
	specification := os.Getenv("LAB_LOGGO_SPEC")
	if specification != "" {
		err := loggo.ConfigureLoggers(specification)
		if err != nil {
			log.Fatal(err)
		}
	}
	loggingInitialized = true
}

func CmdLogger() loggo.Logger {
	initLogger()
	return cmdLogger
}

func ReadCache(fileName string) (bool, []byte, error) {
	return ReadCacheTouch(fileName, false)
}

func ReadCacheTouch(fileName string, touch bool) (bool, []byte, error) {
	if cachingDisabled {
		cmdLogger.Debugf("Caching disabled")
		return false, nil, nil
	}
	cmdLogger.Debugf("Reading %s from cache", fileName)
	if !cacheDir.Exists(fileName) {
		cmdLogger.Debugf("%s not in %s", fileName, cacheDir.Path)
		return false, nil, nil
	}

	if touch {
		cmdLogger.Debugf("Touching %s", fileName)
		touchCacheFile(fileName)
	}

	b, err := cacheDir.ReadFile(fileName)
	return true, b, err
}

// allow touching to error.
func touchCacheFile(fileName string) {
	fullPath := filepath.Join(cacheDir.Path, fileName)
	file, err := os.Stat(fullPath)
	if err != nil {
		return
	}
	currenttime := time.Now().Local()
	modifiedtime := file.ModTime()
	os.Chtimes(fullPath, currenttime, modifiedtime)
}

func WriteCache(fileName string, buffer []byte) error {
	RemoveOldCacheEntries()
	cmdLogger.Infof("Writing %s to cache", fileName)
	return cacheDir.WriteFile(fileName, buffer)
}

func Uncache(cacheKey string) error {
	for dir, _ := range cacheDirectories {
		path := filepath.Join(dir, cacheKey)
		if cacheDir.Exists(path) {
			cmdLogger.Infof("Removing %s", path)
			fileName := filepath.Join(cacheDir.Path, path)
			err := os.Remove(fileName)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

// todo- have different expiry for different files
func RemoveOldCacheEntries() {
	var err error
	if lastCachePrune != nil && time.Since(*lastCachePrune).Minutes() < 1.0 {
		cmdLogger.Debugf("Skipping prune: only %0.2f seconds have elapsed", time.Since(*lastCachePrune).Seconds())
		return
	}
	now := time.Now()
	lastCachePrune = &now
	cmdLogger.Debugf("Removing old cache entries")
	for dir, ttl := range cacheDirectories {
		err = removeExpired(dir, ttl)
		if err != nil {
			cmdLogger.Warningf("RemoveOldCacheEntries: %v", err)
		}
	}
}

type minutes float64

func removeExpired(dir string, maxAge minutes) error {
	var path string
	if dir == "" {
		path = cacheDir.Path
	} else if !cacheDir.Exists(dir) {
		return nil
	} else {
		path = filepath.Join(cacheDir.Path, dir)
	}
	cmdLogger.Debugf("Removing old entries from %s older than %0.2f minutes", path, maxAge)

	files, err := ioutil.ReadDir(path)
	if err != nil {
		return err
	}
	cmdLogger.Debugf("Inspecting %d cached files", len(files))
	currenttime := time.Now()

	for _, f := range files {
		if f.IsDir() {
			continue
		}
		atime := accessTime(f)
		fileToRemove := filepath.Join(path, f.Name())
		currentAge := minutes(currenttime.Sub(atime).Minutes())
		if currentAge > maxAge {
			cmdLogger.Infof("Removing %s - it is %0.2f minutes old", fileToRemove, currentAge)
			err = os.Remove(fileToRemove)
			if err != nil {
				return err
			}
		} else {
			cmdLogger.Debugf("Leaving %s - it is %0.2f minutes old, and has %0.2f minutes left", fileToRemove, currentAge, maxAge-currentAge)
		}
	}
	return nil
}

// Defines filepath for default GitLab templates
const (
	TmplMR    = "merge_request_templates/default.md"
	TmplIssue = "issue_templates/default.md"
)

// Cache directory
var (
	configDirs configdir.ConfigDir
	cacheDir   *configdir.Config
)

// LoadGitLabTmpl loads gitlab templates for use in creating Issues and MRs
//
// https://gitlab.com/help/user/project/description_templates.md#setting-a-default-template-for-issues-and-merge-requests
func LoadGitLabTmpl(tmplName string) string {
	wd, err := git.WorkingDir()
	if err != nil {
		log.Fatal(err)
	}

	tmplFile := filepath.Join(wd, ".gitlab", tmplName)
	f, err := os.Open(tmplFile)
	if os.IsNotExist(err) {
		return ""
	} else if err != nil {
		log.Fatal(err)
	}

	tmpl, err := ioutil.ReadAll(f)
	if err != nil {
		log.Fatal(err)
	}

	return strings.TrimSpace(string(tmpl))
}

var (
	localProjects map[string]*gitlab.Project = make(map[string]*gitlab.Project)
)

// GetProject looks up a Gitlab project by ID.
func GetProject(projectID interface{}) (*gitlab.Project, error) {
	var opts gitlab.GetProjectOptions
	target, resp, err := lab.Projects.GetProject(projectID, &opts)
	if resp != nil && resp.StatusCode == http.StatusNotFound {
		return nil, ErrProjectNotFound
	}
	if err != nil {
		return nil, err
	}
	return target, nil
}

// FindProject looks up the Gitlab project. If the namespace is not provided in
// the project string it will search for projects in the users namespace
func FindProject(project string) (*gitlab.Project, error) {
	if target, ok := localProjects[project]; ok {
		return target, nil
	}

	search := project
	// Assuming that a "/" in the project means its owned by an org
	if !strings.Contains(project, "/") {
		search = user + "/" + project
	}

	var opts gitlab.GetProjectOptions
	target, resp, err := lab.Projects.GetProject(search, &opts)
	if resp != nil && resp.StatusCode == http.StatusNotFound {
		return nil, ErrProjectNotFound
	}
	if err != nil {
		return nil, err
	}
	// fwiw, I feel bad about this
	localProjects[project] = target

	return target, nil
}

// Fork creates a user fork of a GitLab project
func Fork(project string) (string, error) {
	if !strings.Contains(project, "/") {
		return "", errors.New("remote must include namespace")
	}
	parts := strings.Split(project, "/")

	// See if a fork already exists
	target, err := FindProject(parts[1])
	if err == nil {
		return target.SSHURLToRepo, nil
	} else if err != nil && err != ErrProjectNotFound {
		return "", err
	}

	target, err = FindProject(project)
	if err != nil {
		return "", err
	}

	var opts gitlab.ForkProjectOptions
	fork, _, err := lab.Projects.ForkProject(target.ID, &opts)
	if err != nil {
		return "", err
	}

	return fork.SSHURLToRepo, nil
}

// MRCreate opens a merge request on GitLab
func MRCreate(project string, opts *gitlab.CreateMergeRequestOptions) (string, error) {
	p, err := FindProject(project)
	if err != nil {
		return "", err
	}

	mr, _, err := lab.MergeRequests.CreateMergeRequest(p.ID, opts)
	if err != nil {
		return "", err
	}
	return mr.WebURL, nil
}

// MRCreateNote adds a note to a merge request on GitLab
func MRCreateNote(project string, discussionId string, mrNum int, opts *gitlab.CreateMergeRequestNoteOptions) (string, *gitlab.Note, error) {
	p, err := FindProject(project)
	if err != nil {
		return "", nil, err
	}

	var note *gitlab.Note
	if discussionId == "" {
		note, _, err = lab.Notes.CreateMergeRequestNote(p.ID, mrNum, opts)
	} else {
		dopts := gitlab.AddMergeRequestDiscussionNoteOptions{
			Body: opts.Body,
		}
		note, _, err = lab.Discussions.AddMergeRequestDiscussionNote(p.ID, mrNum, discussionId, &dopts)
	}
	if err != nil {
		return "", note, err
	}
	// Command only note, probably.
	if note.ID == 0 {
		return "Added note", note, nil
	}

	// Unlike MR, Note has no WebURL property, so we have to create it
	// ourselves from the project, noteable id and note id
	return fmt.Sprintf("%s/merge_requests/%d#note_%d", p.WebURL, note.NoteableIID, note.ID), note, nil
}

// MRGet retrieves the merge request from GitLab project
func MRGet(project string, iid int) (*gitlab.MergeRequest, error) {
	p, err := FindProject(project)
	if err != nil {
		return nil, err
	}

	mr, r, err := lab.MergeRequests.GetMergeRequest(p.ID, iid, nil)
	if r.StatusCode == 404 {
		return nil, errors.New(fmt.Sprintf("Could not find !%d in %s", iid, project))
	}
	if err != nil {
		return nil, err
	}

	return mr, nil
}

// MRList lists the MRs on a GitLab project
func MRList(project string, opts gitlab.ListProjectMergeRequestsOptions, n int) ([]*gitlab.MergeRequest, error) {
	if n == -1 {
		opts.PerPage = 100
	}
	p, err := FindProject(project)
	if err != nil {
		return nil, err
	}

	list, resp, err := lab.MergeRequests.ListProjectMergeRequests(p.ID, &opts)
	if err != nil {
		return nil, err
	}
	if resp.CurrentPage == resp.TotalPages {
		return list, nil
	}
	opts.Page = resp.NextPage
	for len(list) < n || n == -1 {
		if n != -1 {
			opts.PerPage = n - len(list)
		}
		mrs, resp, err := lab.MergeRequests.ListProjectMergeRequests(p.ID, &opts)
		if err != nil {
			return nil, err
		}
		opts.Page = resp.NextPage
		list = append(list, mrs...)
		if resp.CurrentPage == resp.TotalPages {
			break
		}
	}
	return list, nil
}

// MRClose closes an mr on a GitLab project
func MRClose(pid interface{}, id int) error {
	mr, _, err := lab.MergeRequests.GetMergeRequest(pid, id, nil)
	if err != nil {
		return err
	}
	if mr.State == "closed" {
		return fmt.Errorf("mr already closed")
	}
	_, _, err = lab.MergeRequests.UpdateMergeRequest(pid, int(id), &gitlab.UpdateMergeRequestOptions{
		StateEvent: gitlab.String("close"),
	})
	if err != nil {
		return err
	}
	return nil
}

// MRAssign assigns the given MR to the given user, and updates the value of MR
func MRAssign(mr *gitlab.MergeRequest, userID int) error {
	for _, user := range mr.Assignees {
		if user.ID == userID {
			return nil
		}
	}
	_mr, _, err := lab.MergeRequests.UpdateMergeRequest(mr.ProjectID, mr.ID, &gitlab.UpdateMergeRequestOptions{
		AssigneeIDs: []int{userID},
	})
	if err != nil {
		return err
	}
	mr = _mr
	return nil
}

// MRRebase merges an mr on a GitLab project
func MRRebase(pid interface{}, id int) error {
	_, err := lab.MergeRequests.RebaseMergeRequest(pid, int(id))
	if err != nil {
		return err
	}
	return nil
}

// MRMerge merges an mr on a GitLab project
func MRMerge(pid interface{}, id int) error {
	_, _, err := lab.MergeRequests.AcceptMergeRequest(pid, int(id), &gitlab.AcceptMergeRequestOptions{
		MergeWhenPipelineSucceeds: gitlab.Bool(true),
	})
	if err != nil {
		return err
	}
	return nil
}

// MRApprove approves an mr on a GitLab project
func MRApprove(pid interface{}, id int) error {
	_, _, err := lab.MergeRequestApprovals.ApproveMergeRequest(pid, id, &gitlab.ApproveMergeRequestOptions{})
	if err != nil {
		return err
	}
	return nil
}

// MRThumbUp places a thumb up/down on a merge request
func MRThumbUp(pid interface{}, id int) error {
	_, _, err := lab.AwardEmoji.CreateMergeRequestAwardEmoji(pid, id, &gitlab.CreateAwardEmojiOptions{
		Name: "thumbsup",
	})
	if err != nil {
		return err
	}
	return nil
}

// MRThumbDown places a thumb up/down on a merge request
func MRThumbDown(pid interface{}, id int) error {
	_, _, err := lab.AwardEmoji.CreateMergeRequestAwardEmoji(pid, id, &gitlab.CreateAwardEmojiOptions{
		Name: "thumbsdown",
	})
	if err != nil {
		return err
	}
	return nil
}

// IssueCreate opens a new issue on a GitLab project
func IssueCreate(project string, opts *gitlab.CreateIssueOptions) (string, error) {
	p, err := FindProject(project)
	if err != nil {
		return "", err
	}

	mr, _, err := lab.Issues.CreateIssue(p.ID, opts)
	if err != nil {
		return "", err
	}
	return mr.WebURL, nil
}

// IssueUpdate edits an issue on a GitLab project
func IssueUpdate(project string, issueNum int, opts *gitlab.UpdateIssueOptions) (string, error) {
	p, err := FindProject(project)
	if err != nil {
		return "", err
	}

	issue, _, err := lab.Issues.UpdateIssue(p.ID, issueNum, opts)
	if err != nil {
		return "", err
	}
	return issue.WebURL, nil
}

// IssueCreateNote creates a new note on an issue and returns the note URL
func IssueCreateNote(project string, issueNum int, opts *gitlab.CreateIssueNoteOptions) (string, error) {
	p, err := FindProject(project)
	if err != nil {
		return "", err
	}

	note, _, err := lab.Notes.CreateIssueNote(p.ID, issueNum, opts)
	if err != nil {
		return "", err
	}

	// Unlike Issue, Note has no WebURL property, so we have to create it
	// ourselves from the project, noteable id and note id
	return fmt.Sprintf("%s/issues/%d#note_%d", p.WebURL, note.NoteableIID, note.ID), nil
}

// IssueGet retrieves the issue information from a GitLab project
func IssueGet(project string, issueNum int) (*gitlab.Issue, error) {
	p, err := FindProject(project)
	if err != nil {
		return nil, err
	}

	issue, _, err := lab.Issues.GetIssue(p.ID, issueNum)
	if err != nil {
		return nil, err
	}

	return issue, nil
}

// IssueList gets a list of issues on a GitLab Project
func IssueList(project string, opts gitlab.ListProjectIssuesOptions, n int) ([]*gitlab.Issue, error) {
	if n == -1 {
		opts.PerPage = 100
	}
	p, err := FindProject(project)
	if err != nil {
		return nil, err
	}

	list, resp, err := lab.Issues.ListProjectIssues(p.ID, &opts)
	if err != nil {
		return nil, err
	}
	if resp.CurrentPage == resp.TotalPages {
		return list, nil
	}

	opts.Page = resp.NextPage
	for len(list) < n || n == -1 {
		if n != -1 {
			opts.PerPage = n - len(list)
		}
		issues, resp, err := lab.Issues.ListProjectIssues(p.ID, &opts)
		if err != nil {
			return nil, err
		}
		opts.Page = resp.NextPage
		list = append(list, issues...)
		if resp.CurrentPage == resp.TotalPages {
			break
		}
	}
	return list, nil
}

// IssueClose closes an issue on a GitLab project
func IssueClose(pid interface{}, id int) error {
	_, _, err := lab.Issues.UpdateIssue(pid, id, &gitlab.UpdateIssueOptions{
		StateEvent: gitlab.String("close"),
	})
	if err != nil {
		return err
	}
	return nil
}

// IssueListDiscussions retrieves the discussions (aka notes & comments) for an issue
func IssueListDiscussions(project string, issueNum int) ([]*gitlab.Discussion, error) {
	p, err := FindProject(project)
	if err != nil {
		return nil, err
	}

	discussions := []*gitlab.Discussion{}
	opt := &gitlab.ListIssueDiscussionsOptions{
		// 100 is the maximum allowed by the API
		PerPage: 100,
		Page:    1,
	}

	for {
		// get a page of discussions from the API ...
		d, resp, err := lab.Discussions.ListIssueDiscussions(p.ID, issueNum, opt)
		if err != nil {
			return nil, err
		}

		// ... and add them to our collection of discussions
		discussions = append(discussions, d...)

		// if we've seen all the pages, then we can break here
		if opt.Page >= resp.TotalPages {
			break
		}

		// otherwise, update the page number to get the next page.
		opt.Page = resp.NextPage
	}

	return discussions, nil
}

// BranchPushed checks if a branch exists on a GitLab project
func BranchPushed(pid interface{}, branch string) bool {
	b, _, err := lab.Branches.GetBranch(pid, branch)
	if err != nil {
		return false
	}
	return b != nil
}

// LabelList gets a list of labels on a GitLab Project
func LabelList(project string) ([]*gitlab.Label, error) {
	p, err := FindProject(project)
	if err != nil {
		return nil, err
	}

	list, _, err := lab.Labels.ListLabels(p.ID, &gitlab.ListLabelsOptions{})
	if err != nil {
		return nil, err
	}

	return list, nil
}

// ProjectSnippetCreate creates a snippet in a project
func ProjectSnippetCreate(pid interface{}, opts *gitlab.CreateProjectSnippetOptions) (*gitlab.Snippet, error) {
	snip, _, err := lab.ProjectSnippets.CreateSnippet(pid, opts)
	if err != nil {
		return nil, err
	}

	return snip, nil
}

// ProjectSnippetDelete deletes a project snippet
func ProjectSnippetDelete(pid interface{}, id int) error {
	_, err := lab.ProjectSnippets.DeleteSnippet(pid, id)
	return err
}

// ProjectSnippetList lists snippets on a project
func ProjectSnippetList(pid interface{}, opts gitlab.ListProjectSnippetsOptions, n int) ([]*gitlab.Snippet, error) {
	if n == -1 {
		opts.PerPage = 100
	}
	list, resp, err := lab.ProjectSnippets.ListSnippets(pid, &opts)
	if err != nil {
		return nil, err
	}
	if resp.CurrentPage == resp.TotalPages {
		return list, nil
	}
	opts.Page = resp.NextPage
	for len(list) < n || n == -1 {
		if n != -1 {
			opts.PerPage = n - len(list)
		}
		snips, resp, err := lab.ProjectSnippets.ListSnippets(pid, &opts)
		if err != nil {
			return nil, err
		}
		opts.Page = resp.NextPage
		list = append(list, snips...)
		if resp.CurrentPage == resp.TotalPages {
			break
		}
	}
	return list, nil
}

// SnippetCreate creates a personal snippet
func SnippetCreate(opts *gitlab.CreateSnippetOptions) (*gitlab.Snippet, error) {
	snip, _, err := lab.Snippets.CreateSnippet(opts)
	if err != nil {
		return nil, err
	}

	return snip, nil
}

// SnippetDelete deletes a personal snippet
func SnippetDelete(id int) error {
	_, err := lab.Snippets.DeleteSnippet(id)
	return err
}

// SnippetList lists snippets on a project
func SnippetList(opts gitlab.ListSnippetsOptions, n int) ([]*gitlab.Snippet, error) {
	if n == -1 {
		opts.PerPage = 100
	}
	list, resp, err := lab.Snippets.ListSnippets(&opts)
	if err != nil {
		return nil, err
	}
	if resp.CurrentPage == resp.TotalPages {
		return list, nil
	}
	opts.Page = resp.NextPage
	for len(list) < n || n == -1 {
		if n != -1 {
			opts.PerPage = n - len(list)
		}
		snips, resp, err := lab.Snippets.ListSnippets(&opts)
		if err != nil {
			return nil, err
		}
		opts.Page = resp.NextPage
		list = append(list, snips...)
		if resp.CurrentPage == resp.TotalPages {
			break
		}
	}
	return list, nil
}

// Lint validates .gitlab-ci.yml contents
func Lint(content string) (bool, error) {
	lint, _, err := lab.Validate.Lint(content)
	if err != nil {
		return false, err
	}
	if len(lint.Errors) > 0 {
		return false, errors.New(strings.Join(lint.Errors, " - "))
	}
	return lint.Status == "valid", nil
}

// ProjectCreate creates a new project on GitLab
func ProjectCreate(opts *gitlab.CreateProjectOptions) (*gitlab.Project, error) {
	p, _, err := lab.Projects.CreateProject(opts)
	if err != nil {
		return nil, err
	}
	return p, nil
}

// ProjectDelete creates a new project on GitLab
func ProjectDelete(pid interface{}) error {
	_, err := lab.Projects.DeleteProject(pid)
	if err != nil {
		return err
	}
	return nil
}

// ProjectList gets a list of projects on GitLab
func ProjectList(opts gitlab.ListProjectsOptions, n int) ([]*gitlab.Project, error) {
	list, resp, err := lab.Projects.ListProjects(&opts)
	if err != nil {
		return nil, err
	}
	if resp.CurrentPage == resp.TotalPages {
		return list, nil
	}
	opts.Page = resp.NextPage
	for len(list) < n || n == -1 {
		if n != -1 {
			opts.PerPage = n - len(list)
		}
		projects, resp, err := lab.Projects.ListProjects(&opts)
		if err != nil {
			return nil, err
		}
		opts.Page = resp.NextPage
		list = append(list, projects...)
		if resp.CurrentPage == resp.TotalPages {
			break
		}
	}
	return list, nil
}

// MRPipelines get the pipelines for a given MR
func MRPipelines(pid interface{}, mr *gitlab.MergeRequest) ([]*gitlab.PipelineInfo, error) {
	pipelines, _, err := lab.MergeRequests.ListMergeRequestPipelines(pid, mr.IID)
	return pipelines, err
}

// MRPipelineCreate Create a new pipeline for an MR
func MRPipelineCreate(project int64, iid string) (*gitlab.PipelineInfo, error) {
	var options []gitlab.OptionFunc
	u := fmt.Sprintf("projects/%d/merge_requests/%s/pipelines", project, iid)
	req, err := lab.NewRequest("POST", u, nil, options)
	if err != nil {
		return nil, err
	}

	p := new(gitlab.PipelineInfo)
	_, err = lab.Do(req, p)
	if err != nil {
		return nil, err
	}
	return p, err
}

// CIJobs returns a list of jobs in a pipeline for a given sha. The jobs are
// returned sorted by their CreatedAt time
func CIJobs(pid interface{}, branch string) ([]*gitlab.Job, error) {
	pipelines, _, err := lab.Pipelines.ListProjectPipelines(pid, &gitlab.ListProjectPipelinesOptions{
		Ref: gitlab.String(branch),
	})
	if len(pipelines) == 0 || err != nil {
		return nil, err
	}
	return PipelineJobs(pid, pipelines[0].ID)
}

func PipelineJobs(pid interface{}, target int) ([]*gitlab.Job, error) {
	opts := &gitlab.ListJobsOptions{
		ListOptions: gitlab.ListOptions{
			PerPage: 500,
		},
	}
	list, resp, err := lab.Jobs.ListPipelineJobs(pid, target, opts)
	if err != nil {
		return nil, err
	}
	if resp.CurrentPage == resp.TotalPages {
		return list, nil
	}
	opts.Page = resp.NextPage
	for {
		jobs, resp, err := lab.Jobs.ListPipelineJobs(pid, target, opts)
		if err != nil {
			return nil, err
		}
		opts.Page = resp.NextPage
		list = append(list, jobs...)
		if resp.CurrentPage == resp.TotalPages {
			break
		}
	}
	return list, nil
}

// CITrace searches by name for a job and returns its trace file. The trace is
// static so may only be a portion of the logs if the job is till running. If
// no name is provided job is picked using the first available:
// 1. Last Running Job
// 2. First Pending Job
// 3. Last Job in Pipeline
func CITrace(pid interface{}, branch, name string) (io.Reader, *gitlab.Job, error) {
	jobs, err := CIJobs(pid, branch)
	if len(jobs) == 0 || err != nil {
		return nil, nil, err
	}
	var (
		job          *gitlab.Job
		lastRunning  *gitlab.Job
		firstPending *gitlab.Job
	)

	for _, j := range jobs {
		if j.Status == "running" {
			lastRunning = j
		}
		if j.Status == "pending" && firstPending == nil {
			firstPending = j
		}
		if j.Name == name {
			job = j
			// don't break because there may be a newer version of the job
		}
	}
	if job == nil {
		job = lastRunning
	}
	if job == nil {
		job = firstPending
	}
	if job == nil {
		job = jobs[len(jobs)-1]
	}
	r, _, err := lab.Jobs.GetTraceFile(pid, job.ID)
	if err != nil {
		return nil, job, err
	}

	return r, job, err
}

func uncachePipeline(pipelineId int) {
	cmdLogger.Debugf("Removing cache entries for Pipeline %d", pipelineId)
	for dir, _ := range cacheDirectories {
		pipelineFile := filepath.Join(dir, fmt.Sprintf("pipeline_%d.json", pipelineId))
		if cacheDir.Exists(pipelineFile) {
			os.Remove(filepath.Join(cacheDir.Path, pipelineFile))
		}
		jobFile := filepath.Join(dir, fmt.Sprintf("pipeline_jobs_%d.json", pipelineId))
		if cacheDir.Exists(jobFile) {
			os.Remove(filepath.Join(cacheDir.Path, jobFile))
		}
	}
}

// CIPlayOrRetry runs a job either by playing it for the first time or by
// retrying it based on the currently known job state
func CIPlayOrRetry(pid interface{}, jobID int, status string) (*gitlab.Job, error) {
	switch status {
	case "pending", "running":
		return nil, nil
	case "manual":
		j, _, err := lab.Jobs.PlayJob(pid, jobID)
		if err != nil {
			return nil, err
		}
		uncachePipeline(j.Pipeline.ID)
		return j, nil
	default:

		j, _, err := lab.Jobs.RetryJob(pid, jobID)
		if err != nil {
			return nil, err
		}
		uncachePipeline(j.Pipeline.ID)

		return j, nil
	}
}

// CICancel cancels a job for a given project by its ID.
func CICancel(pid interface{}, jobID int) (*gitlab.Job, error) {
	j, _, err := lab.Jobs.CancelJob(pid, jobID)
	if err != nil {
		return nil, err
	}
	return j, nil
}

// CICreate creates a pipeline for given ref
func CICreate(pid interface{}, opts *gitlab.CreatePipelineOptions) (*gitlab.Pipeline, error) {
	p, _, err := lab.Pipelines.CreatePipeline(pid, opts)
	if err != nil {
		return nil, err
	}
	return p, nil
}

// CITrigger triggers a pipeline for given ref
func CITrigger(pid interface{}, opts gitlab.RunPipelineTriggerOptions) (*gitlab.Pipeline, error) {
	p, _, err := lab.PipelineTriggers.RunPipelineTrigger(pid, &opts)
	if err != nil {
		return nil, err
	}
	return p, nil
}

// UserIDFromUsername returns the associated Users ID in GitLab. This is useful
// for API calls that allow you to reference a user, but only by ID.
func UserIDFromUsername(username string) (int, error) {
	us, _, err := lab.Users.ListUsers(&gitlab.ListUsersOptions{
		Username: gitlab.String(username),
	})
	if err != nil || len(us) == 0 {
		return -1, err
	}
	return us[0].ID, nil
}

// OffsetPagination modifies the request to apply the given offset-pagination
func OffsetPagination(page int, perPage int) gitlab.OptionFunc {
	return func(req *retryablehttp.Request) error {
		q := req.URL.Query()
		q.Add("page", strconv.Itoa(page))
		q.Add("per_page", strconv.Itoa(perPage))
		req.URL.RawQuery = q.Encode()
		return nil
	}
}

func parseID(id interface{}) (string, error) {
	switch v := id.(type) {
	case int:
		return strconv.Itoa(v), nil
	case string:
		return v, nil
	default:
		return "", fmt.Errorf("invalid ID type %#v, the ID must be an int or a string", id)
	}
}

// Helper function to escape a project identifier.
func pathEscape(s string) string {
	return strings.Replace(url.PathEscape(s), ".", "%2E", -1)
}
