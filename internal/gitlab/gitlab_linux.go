package gitlab

import (
	"os"
	"syscall"
	time "time"
)

func accessTime(file os.FileInfo) time.Time {
	stat := file.Sys().(*syscall.Stat_t)
	return time.Unix(int64(stat.Atim.Sec), int64(stat.Atim.Nsec))
}
