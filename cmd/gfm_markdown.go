package cmd

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/spf13/cobra"
	gitlab "github.com/xanzy/go-gitlab"
	"github.com/zaquestion/lab/internal/git"
	lab "github.com/zaquestion/lab/internal/gitlab"
	"log"
	"text/template"
)

var markdownConfig struct {
	text       string
	plain      bool
	reqOptions []gitlab.OptionFunc
}

type markdownParams struct {
	Text    string `json:"text"`
	Gfm     bool   `json:"gfm"`
	Project string `json:"project"`
}

type markdownResponse struct {
	Html string
}

var gfmMarkdownCmd = &cobra.Command{
	Use:   "markdown [remote]",
	Short: "Render some markdown",
	Run: func(cmd *cobra.Command, args []string) {
		projectPath, _, err := parseArgsRemoteString(args)
		if markdownConfig.text == "" {
			text, err := markdownText()
			if err != nil {
				log.Fatal(err)
			}
			markdownConfig.text, err = git.EditFile("MARKDOWN", text)
			if err != nil {
				log.Fatal(err)
			}
		}
		client := lab.Client()
		params := markdownParams{
			Text:    markdownConfig.text,
			Gfm:     !markdownConfig.plain,
			Project: projectPath,
		}
		jsonParams, err := json.Marshal(params)
		lab.CmdLogger().Infof("Params: %s", jsonParams)

		req, err := client.NewRequest("POST", "markdown", params, markdownConfig.reqOptions)
		if err != nil {
			log.Fatal(err)
		}
		var mdResp markdownResponse
		_, err = client.Do(req, &mdResp)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(mdResp.Html)
	},
}

func markdownText() (string, error) {
	const tmpl = `{{.InitMsg}}
{{.CommentChar}} Write some markdown to convert. Commented lines are discarded.`

	initMsg := "\n"
	commentChar := git.CommentChar()

	t, err := template.New("tmpl").Parse(tmpl)
	if err != nil {
		return "", err
	}

	msg := &struct {
		InitMsg     string
		CommentChar string
	}{
		InitMsg:     initMsg,
		CommentChar: commentChar,
	}

	var b bytes.Buffer
	err = t.Execute(&b, msg)
	if err != nil {
		return "", err
	}

	return b.String(), nil
}

func init() {
	projectCmd.AddCommand(gfmMarkdownCmd)
	gfmMarkdownCmd.Flags().StringVarP(&markdownConfig.text, "text", "t", "", "The text to render")
	gfmMarkdownCmd.Flags().BoolVarP(&markdownConfig.plain, "plain", "p", false, "Use plain markdown")
}
