package cmd

import (
	"bytes"
	"context"
	"crypto/sha1"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"syscall"
	"text/template"
	"time"

	humanize "github.com/dustin/go-humanize"
	graphql "github.com/machinebox/graphql"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	gitconfig "github.com/tcnksm/go-gitconfig"
	gitlab "github.com/xanzy/go-gitlab"
	"github.com/zaquestion/lab/internal/gid"
	"github.com/zaquestion/lab/internal/git"
	lab "github.com/zaquestion/lab/internal/gitlab"
	types "github.com/zaquestion/lab/internal/types"
	"gitlab.com/alexkalderimis/trapezi"
)

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:                   "lab",
	Short:                 "A Git Wrapper for GitLab",
	Long:                  ``,
	ZshCompletionFunction: zshCompletionFunction,
	Run: func(cmd *cobra.Command, args []string) {
		if ok, err := cmd.Flags().GetBool("version"); err == nil && ok {
			versionCmd.Run(cmd, args)
			return
		}
		helpCmd.Run(cmd, args)
	},
}

func mustNot(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func rpad(s string, padding int) string {
	template := fmt.Sprintf("%%-%ds", padding)
	return fmt.Sprintf(template, s)
}

var templateFuncs = template.FuncMap{
	"rpad": rpad,
}

const labUsageTmpl = `{{range .Commands}}{{if (and (or .IsAvailableCommand (ne .Name "help")) (and (ne .Name "clone") (ne .Name "version") (ne .Name "merge-request")))}}
  {{rpad .Name .NamePadding }} {{.Short}}{{end}}{{end}}`

const mrTTL = "5m"

const (
	jobFormat        = "%*s: %*s - %10s id: %s\n"
	jobFormatNoStage = "  %*s - %10s id: %s\n"
)

func mrCacheKey(mrID int64) string {
	return fmt.Sprintf("ttl_%s/merge_request_%d.json", mrTTL, mrID)
}

func labUsageFormat(c *cobra.Command) string {
	t := template.New("top")
	t.Funcs(templateFuncs)
	template.Must(t.Parse(labUsageTmpl))

	var buf bytes.Buffer
	err := t.Execute(&buf, c)
	if err != nil {
		c.Println(err)
	}
	return buf.String()
}

func helpFunc(cmd *cobra.Command, args []string) {
	// When help func is called from the help command args will be
	// populated. When help is called with cmd.Help(), the args are not
	// passed through, so we pick them up ourselves here
	if len(args) == 0 {
		args = os.Args[1:]
	}
	rootCmd := cmd.Root()
	// Show help for sub/commands -- any commands that isn't "lab" or "help"
	if cmd, _, err := rootCmd.Find(args); err == nil &&
		cmd != rootCmd && strings.Split(cmd.Use, " ")[0] != "help" {
		// Cobra will check parent commands for a helpFunc and we only
		// want the root command to actually use this custom help func.
		// Here we trick cobra into thinking that there is no help func
		// so it will use the default help for the subcommands
		cmd.Root().SetHelpFunc(nil)
		err2 := cmd.Help()
		if err2 != nil {
			log.Fatal(err)
		}
		return
	}
	formatChar := "\n"
	if git.IsHub {
		formatChar = ""
	}

	git := git.New()
	git.Stdout = nil
	git.Stderr = nil
	usage, _ := git.CombinedOutput()
	fmt.Printf("%s%sThese GitLab commands are provided by lab:\n%s\n\n", string(usage), formatChar, labUsageFormat(cmd.Root()))
}

var helpCmd = &cobra.Command{
	Use:   "help [command [subcommand...]]",
	Short: "Show the help for lab",
	Long:  ``,
	Run:   helpFunc,
}

func init() {
	// NOTE: Calling SetHelpCommand like this causes helpFunc to be called
	// with correct arguments. If the default cobra help func is used no
	// arguments are passed through and subcommand help breaks.
	RootCmd.SetHelpCommand(helpCmd)
	RootCmd.SetHelpFunc(helpFunc)
	RootCmd.Flags().Bool("version", false, "Show the lab version")
}

// TODO: this parseArgs thing has gotten way THE FUCK out of hand. Please fix.

// parseArgsStr returns a string and a number if parsed. Many commands accept a
// string to operate on (remote or search) and number such as a page id
func parseArgsStr(args []string) (string, int64, error) {
	return parseArgsStringInt(args)
}

func resolveMrId(mrNum int64) (int64, error) {
	if int(mrNum) > 0 {
		return mrNum, nil
	}
	return 0, errors.New("Not a valid IID")
}

func resolveMrIdWithFallback(rn string, mrNum int64, mrState MRState) (int64, error) {
	_, err := resolveMrId(mrNum)
	if err == nil {
		return mrNum, err
	}
	currentBranch, err := git.CurrentBranch()
	if err != nil {
		return 0, err
	}

	bytes := fmt.Sprintf("%v-%v-%v-%v", rn, mrNum, mrState, currentBranch)
	hash := sha1.Sum([]byte(bytes))
	filename := fmt.Sprintf("merge_request_%x.json", hash)
	fn := func() (*MergeRequestIdentifiers, error) {
		return FindMRForBranchAndState(rn, currentBranch, mrState)
	}
	mr, err := resolveMRWithCache(filename, fn)
	if err != nil {
		return -1, err
	}
	return mr.NumericIID()
}

// MergeRequestIdentifiers - The immutable properties of a merge-request
type MergeRequestIdentifiers struct {
	ID        int64
	IID       string
	ProjectID int64
}

// MergeRequestID Get a MergeRequestID
func (idents *MergeRequestIdentifiers) MergeRequestID() gid.GID {
	ids := gid.Factory("gitlab", "MergeRequest")

	return ids.Create(idents.ID)
}

func (idents *MergeRequestIdentifiers) NumericIID() (int64, error) {
	return strconv.ParseInt(idents.IID, 10, 64)
}

func (idents *MergeRequestIdentifiers) AsParams() (string, int, error) {
	iid, err := idents.NumericIID()
	if err != nil {
		return "", 0, err
	}
	if int(iid) < 0 {
		return "", 0, errors.New("IID is too large: " + idents.IID)
	}
	return strconv.FormatInt(idents.ProjectID, 10), int(iid), nil
}

func (idents *MergeRequestIdentifiers) fetchMR() (*gitlab.MergeRequest, error) {
	pid, iid, err := idents.AsParams()
	if err != nil {
		return nil, err
	}
	lab.CmdLogger().Debugf("Fetching MR: %s/%d", idents.ProjectID, iid)
	mr, _, err := lab.Client().MergeRequests.GetMergeRequest(pid, iid, nil)
	return mr, err
}

// To avoid caching issue, MRs are stripped of labels
func mrListWithCache(rn string, num int, opts gitlab.ListProjectMergeRequestsOptions) ([]*gitlab.MergeRequest, error) {
	bytes, err := json.Marshal(opts)
	if err != nil {
		lab.CmdLogger().Debugf("Cannot cache: %s", err)
		return lab.MRList(rn, opts, num)
	}
	rnBytes := []byte(rn)
	bytes = append(bytes, rnBytes...)

	hash := sha1.Sum(bytes)
	filename := fmt.Sprintf("ttl_5m/mr_list_%x.json", hash)
	lab.RemoveOldCacheEntries()
	inCache, bytes, err := lab.ReadCache(filename)

	if inCache && err == nil {
		var mrs []*gitlab.MergeRequest
		err := json.Unmarshal(bytes, &mrs)
		if err == nil {
			return mrs, nil
		} else {
			lab.CmdLogger().Debugf("Value found in cache, but cannot Unmarshal: %s", err)
		}
	}

	mrs, err := lab.MRList(rn, opts, num)
	if err != nil {
		return nil, err
	}

	for _, mr := range mrs {
		mr.Labels = nil
	}

	bytes, err = json.Marshal(mrs)
	if err == nil {
		lab.WriteCache(filename, bytes)
	} else {
		lab.CmdLogger().Debugf("Cannot write to cache: %s", err)
	}

	return mrs, err
}

// Cached lookup of a merge request. The only properties it is valid to read on the
// returned value are ID, IID and ProjectID - these are hence moved to separate struct
func resolveMRWithCache(filename string, fn func() (*MergeRequestIdentifiers, error)) (MergeRequestIdentifiers, error) {
	lab.RemoveOldCacheEntries()
	inCache, bytes, err := lab.ReadCache(filename)
	if inCache && err == nil {
		lab.CmdLogger().Debugf("Found MR in cache at %s", filename)
		mr := MergeRequestIdentifiers{}
		err := json.Unmarshal(bytes, &mr)
		if err == nil {
			return mr, nil
		} else {
			lab.CmdLogger().Warningf("Could not unmarshal from cached value: %s", err)
		}
	}
	lab.CmdLogger().Debugf("Cache miss - fetching %s", filename)
	mr, err := fn()
	if err != nil {
		return MergeRequestIdentifiers{}, err
	}
	bytes, err = json.Marshal(mr)
	if err == nil {
		lab.WriteCache(filename, bytes)
	}

	if mr == nil {
		return MergeRequestIdentifiers{}, errors.New("No MR found")
	}

	return *mr, nil
}

func mergeRequestToIdents(mr gitlab.MergeRequest) MergeRequestIdentifiers {
	idents := MergeRequestIdentifiers{
		ID:        int64(mr.ID),
		IID:       fmt.Sprintf("%d", mr.IID),
		ProjectID: int64(mr.ProjectID),
	}
	return idents
}

// parseArgsStringInt returns a string and a number if parsed.
func parseArgsStringInt(args []string) (string, int64, error) {
	if len(args) == 2 {
		n, err := strconv.ParseInt(args[1], 0, 64)
		if err != nil {
			return args[0], 0, err
		}
		return args[0], n, nil
	}
	if len(args) == 1 {
		n, err := strconv.ParseInt(args[0], 0, 64)
		if err != nil {
			return args[0], 0, nil
		}
		return "", n, nil
	}
	return "", 0, nil
}

// parseArgs returns a remote name and a number if parsed
func parseArgs(args []string) (string, int64, error) {
	return parseArgsRemoteInt(args)
}

// MRState What state is the MR in?
type MRState string

// StateOpen the OPEN MR state
const StateOpen MRState = "opened"

// StateClosed the CLOSED MR state
const StateClosed MRState = "closed"

// StateMerged and, you guessed it, the MERGED MR state
const StateMerged MRState = "merged"

func parseProjectMR(args []string, mrState MRState) (string, *MergeRequestIdentifiers, error) {
	fullPath, iid64, err := parseArgs(args)
	if err != nil {
		return "", nil, err
	}
	iid := int(iid64)
	var filename, currentBranch string
	if iid <= 0 {
		currentBranch, err = git.CurrentBranch()
		if err != nil {
			return "", nil, err
		}
		bytes := fmt.Sprintf("%v-%v-%v", fullPath, mrState, currentBranch)
		hash := sha1.Sum([]byte(bytes))
		lab.CmdLogger().Debugf("Hash for request is %x", hash)
		filename = fmt.Sprintf("merge_request_%x.json", hash)
	} else {
		filename = fmt.Sprintf("merge_request_%s_%d.json", fullPath, iid)
	}
	fn := func() (*MergeRequestIdentifiers, error) {
		if iid <= 0 {
			return FindMRForBranchAndState(fullPath, currentBranch, mrState)
		}
		return FindMRForIID(fullPath, strconv.Itoa(iid))
	}
	mr, err := resolveMRWithCache(filename, fn)

	if err != nil {
		return "", nil, err
	}
	return fullPath, &mr, nil
}

func uncacheMRIdents(args []string, mrState MRState) error {
	fullPath, iid64, err := parseArgs(args)
	if err != nil {
		return err
	}
	iid := int(iid64)
	var filename string

	if iid <= 0 {
		currentBranch, err := git.CurrentBranch()
		if err != nil {
			return err
		}
		bytes := fmt.Sprintf("%v-%v-%v", fullPath, mrState, currentBranch)
		hash := sha1.Sum([]byte(bytes))
		lab.CmdLogger().Debugf("Hash for request is %x", hash)
		filename = fmt.Sprintf("merge_request_%x.json", hash)
	} else {
		filename = fmt.Sprintf("merge_request_%s_%d.json", fullPath, iid)
	}

	return lab.Uncache(filename)
}

func FindMRForIID(pid, iid string) (*MergeRequestIdentifiers, error) {
	req := graphql.NewRequest(`
      query($projectPath: ID!, $iid: String!) {
			  project(fullPath: $projectPath) {
				  id
					mergeRequests(first: 1, iids: [$iid]) {
					  nodes { iid id }
					}
				}
			}
      `)
	req.Var("projectPath", pid)
	req.Var("iid", iid)
	return queryForMRIdents(req)
}

func FindMRForBranchAndState(pid string, currentBranch string, mrState MRState) (*MergeRequestIdentifiers, error) {
	query := `
		query($projectPath: ID!, $branch: String!, $state: MergeRequestState!) {
			project(fullPath: $projectPath) {
				id
				mergeRequests(first: 1, sourceBranches: [$branch], state: $state) {
					nodes { iid id }
				}
			}
		}
	`
	req := graphql.NewRequest(query)
	req.Var("projectPath", pid)
	req.Var("branch", currentBranch)
	req.Var("state", mrState)
	lab.CmdLogger().Debugf("Finding %s MRs for %s on %s", mrState, pid, currentBranch)
	lab.CmdLogger().Debugf("query: %s", query)
	lab.CmdLogger().Debugf("projectPath: %s, branch: %s, state: %s", pid, currentBranch, mrState)
	return queryForMRIdents(req)
}

func queryForMRIdents(req *graphql.Request) (*MergeRequestIdentifiers, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(2_000_000_000))
	defer cancel()
	client := lab.GQLClient()
	var resp struct {
		Project struct {
			ID            gid.GID
			MergeRequests struct {
				Nodes []struct {
					ID  gid.GID
					IID string
				}
			}
		}
	}

	lab.CmdLogger().Debugf("Running request")
	err := client.Run(ctx, req, &resp)
	if err != nil {
		return nil, err
	}
	if err != nil {
		return nil, err
	}
	if len(resp.Project.MergeRequests.Nodes) == 1 {
		mr := resp.Project.MergeRequests.Nodes[0]
		id, err := mr.ID.IntegerID()
		if err != nil {
			return nil, err
		}
		projectID, err := resp.Project.ID.IntegerID()
		if err != nil {
			return nil, err
		}
		lab.CmdLogger().Debugf("found: %s", mr.IID)
		return &MergeRequestIdentifiers{ID: id, ProjectID: projectID, IID: mr.IID}, nil
	}
	return nil, errors.New("Could not find MergeRequest")
}

func GetProjectWithCache(pid gid.GID) (*types.Project, error) {
	id, err := pid.IntegerID()

	if err != nil {
		return nil, err
	}

	lab.RemoveOldCacheEntries()

	filename := fmt.Sprintf("project_%d.json", id)
	inCache, bytes, err := lab.ReadCache(filename)
	if inCache && err == nil {
		lab.CmdLogger().Debugf("Project %d found in cache", id)
		project := types.Project{}
		err := json.Unmarshal(bytes, &project)
		if err != nil {
			lab.Uncache(filename)
		} else {
			return &project, err
		}
	}

	query := `
  query ($id: $ID!) {
		projects(ids: [$id], first: 1) {
		  nodes { fullPath name id }
		}
	}
	`

	client := lab.GQLClient()
	req := graphql.NewRequest(query)
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(2_000_000_000))
	defer cancel()

	var resp struct {
		projects struct {
			nodes []*types.Project
		}
	}

	req.Var("id", id)

	err = client.Run(ctx, req, &resp)
	if err != nil {
		return nil, err
	}

	if len(resp.projects.nodes) == 0 {
		return nil, errors.New(fmt.Sprintf("Project not found: %d", id))
	}

	bytes, err = json.Marshal(resp.projects.nodes[0])
	if err == nil {
		lab.WriteCache(filename, bytes)
	}
	return resp.projects.nodes[0], err
}

func GetPipelineWithCache(pid string, pipelineId int) (*gitlab.Pipeline, error) {
	lab.RemoveOldCacheEntries()
	for _, dir := range []string{"1m", "5m", "10m"} {
		filename := fmt.Sprintf("ttl_%s/pipeline_%d.json", dir, pipelineId)
		inCache, bytes, err := lab.ReadCache(filename)
		if inCache && err == nil {
			lab.CmdLogger().Debugf("Pipeline %d found in cache %s", pipelineId, dir)
			pipeline := gitlab.Pipeline{}
			err := json.Unmarshal(bytes, &pipeline)
			return &pipeline, err
		}
	}
	lab.CmdLogger().Debugf("Cache miss - fetching pipeline %d", pipelineId)
	pipeline, _, err := lab.Client().Pipelines.GetPipeline(pid, pipelineId)
	if err != nil {
		return pipeline, err
	}
	bytes, err := json.Marshal(pipeline)
	if err == nil {
		var filename string
		if pipeline.Status == "created" || pipeline.Status == "pending" || pipeline.Status == "running" {
			filename = fmt.Sprintf("ttl_1m/pipeline_%d.json", pipelineId)
		} else if pipeline.Status == "failed" {
			filename = fmt.Sprintf("ttl_5m/pipeline_%d.json", pipelineId)
		} else {
			filename = fmt.Sprintf("ttl_10m/pipeline_%d.json", pipelineId)
		}
		lab.WriteCache(filename, bytes)
	}
	return pipeline, nil
}

type JobStats struct {
	Passed  int
	Failed  int
	Running int
	Pending int
	Total   int
	Failing bool
}

func GetJobStats(pipeline types.PipelineNode) JobStats {
	var stats JobStats
	jobs := pipeline.Jobs.Nodes
	stats.Failing = pipeline.Status == "FAILED"

	for _, job := range jobs {
		stats.Total += 1
		switch job.DetailedStatus.Status {
		case "failed":
			stats.Failed += 1
		case "success":
			stats.Passed += 1
		case "passed":
			stats.Passed += 1
		case "running":
			stats.Running += 1
		case "created":
			stats.Pending += 1
		case "pending":
			stats.Pending += 1
		}
	}

	return stats
}

func GetPipelineJobsWithCache(pid string, pipelineId int) ([]*gitlab.Job, error) {
	lab.RemoveOldCacheEntries()
	for _, dir := range []string{"1m", "5m", "10m", ""} {
		var filename string
		if dir == "" {
			filename = fmt.Sprintf("pipeline_jobs_%d.json", pipelineId)
		} else {
			filename = fmt.Sprintf("ttl_%s/pipeline_jobs_%d.json", dir, pipelineId)
		}
		inCache, bytes, err := lab.ReadCache(filename)
		if inCache && err == nil {
			lab.CmdLogger().Debugf("Jobs for Pipeline %d found in cache %s", pipelineId, dir)
			jobs := make([]*gitlab.Job, 0)
			err := json.Unmarshal(bytes, &jobs)
			return jobs, err
		}
	}
	lab.CmdLogger().Debugf("Cache miss - fetching pipeline jobs %d", pipelineId)
	jobs, err := lab.PipelineJobs(pid, pipelineId)
	if err != nil {
		return jobs, err
	}
	bytes, err := json.Marshal(jobs)
	if err == nil {
		var status string = "unknown"
		var filename string
		if len(jobs) > 0 {
			job := jobs[0]
			status = job.Pipeline.Status
		}
		if status == "created" || status == "pending" || status == "running" {
			filename = fmt.Sprintf("ttl_5m/pipeline_jobs_%d.json", pipelineId)
		} else if status == "success" {
			filename = fmt.Sprintf("pipeline_jobs_%d.json", pipelineId)
		} else {
			filename = fmt.Sprintf("ttl_10m/pipeline_jobs_%d.json", pipelineId)
		}
		lab.WriteCache(filename, bytes)
	}
	return jobs, nil
}

func getMRByIdentsWithCache(mrIdent MergeRequestIdentifiers) (*gitlab.MergeRequest, error) {
	lab.RemoveOldCacheEntries()
	filename := mrCacheKey(mrIdent.ID)
	inCache, bytes, err := lab.ReadCacheTouch(filename, false)
	if inCache && err == nil {
		lab.CmdLogger().Debugf("MR %d found in cache", mrIdent.ID)
		mr := gitlab.MergeRequest{}
		err := unmarshallMR(bytes, &mr)
		return &mr, err
	}
	lab.CmdLogger().Debugf("Cache miss - fetching merge request %d", mrIdent.ID)
	mr, err := mrIdent.fetchMR()
	if err != nil {
		return mr, err
	}
	bytes, err = json.Marshal(mr)
	if err == nil {
		lab.WriteCache(filename, bytes)
	}
	return mr, nil
}

// This is needed becaue the type in gitlab-go does not round-trip
func unmarshallMR(bytes []byte, mr *gitlab.MergeRequest) error {
	var preparse map[string]interface{}
	err := json.Unmarshal(bytes, &preparse)
	if err != nil {
		return err
	}
	delete(preparse, "labels")
	processed_bytes, err := json.Marshal(preparse)
	if err != nil {
		return err
	}
	return json.Unmarshal(processed_bytes, &mr)
}

// parseArgsRemoteInt is similar to parseArgsStringInt except that it uses the
// string argument as a remote and returns the project name for that remote
func parseArgsRemoteInt(args []string) (string, int64, error) {
	if !git.InsideGitRepo() {
		return "", 0, nil
	}
	remote, num, err := parseArgsStr(args)
	if err != nil {
		return "", 0, err
	}
	// check if fullPath
	if strings.Contains(remote, "/") {
		project, err := lab.FindProject(remote)
		if err == nil {
			return project.PathWithNamespace, int64(project.ID), nil
		}
	}
	ok, err := git.IsRemote(remote)
	if err != nil {
		return "", 0, err
	} else if !ok && remote != "" {
		switch len(args) {
		case 1:
			return "", 0, errors.Errorf("%s is not a valid remote or number", args[0])
		default:
			return "", 0, errors.Errorf("%s is not a valid remote", args[0])
		}
	}
	if err != nil || remote == "" {
		remote = forkedFromRemote
	}
	rn, err := git.PathWithNameSpace(remote)
	if err != nil {
		return "", 0, err
	}
	return rn, num, nil
}

// parseArgsRemoteString returns a remote name and a string if parsed.
// If there is an error, it returns two empty strings.
// If no remote is given, it returns the project name of the default remote
// (ie 'origin').
// If no second argument is given, it returns "" as second return value.
func parseArgsRemoteString(args []string) (string, string, error) {
	if !git.InsideGitRepo() {
		if len(args) >= 2 {
			return args[0], args[1], nil
		} else if len(args) >= 1 {
			return args[0], "", nil
		} else {
			return "", "", errors.New("Not enough arguments. Expected at least 1")
		}
	}

	remote, str := forkedFromRemote, ""

	if len(args) == 1 {
		ok, err := git.IsRemote(args[0])
		if err != nil {
			return "", "", err
		}
		if ok {
			remote = args[0]
		} else {
			str = args[0]
		}
	} else if len(args) > 1 {
		remote, str = args[0], args[1]
	}

	ok, err := git.IsRemote(remote)
	if err != nil {
		return "", "", err
	}
	if !ok {
		return "", "", errors.Errorf("%s is not a valid remote", remote)
	}

	remote, err = git.PathWithNameSpace(remote)
	if err != nil {
		return "", "", err
	}
	return remote, str, nil
}

var (
	// Will be updated to upstream in Execute() if "upstream" remote exists
	forkedFromRemote = "origin"
	// Will be updated to lab.User() in Execute() if forkedFrom is "origin"
	forkRemote = "origin"
)

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	_, err := gitconfig.Local("remote.upstream.url")
	if err == nil {
		forkedFromRemote = "upstream"
	}
	ur, err := git.UpstreamRemote()
	if err == nil {
		forkedFromRemote = ur
	}
	if forkedFromRemote == "" {
		// use the remote tracked by the branch if set
		masterRemote, err := gitconfig.Local("branch.master.remote")
		if err == nil {
			forkedFromRemote = masterRemote
		}
	}

	if forkedFromRemote == "origin" {
		// Check if the user fork exists
		_, err = gitconfig.Local("remote." + lab.User() + ".url")
		if err == nil {
			forkRemote = lab.User()
		}
	}
	// Check if the user is calling a lab command or if we should passthrough
	// NOTE: The help command won't be found by Find, which we are counting on
	cmd, _, err := RootCmd.Find(os.Args[1:])
	if err != nil || cmd.Use == "clone" {
		// Determine if any undefined flags were passed to "clone"
		// TODO: Evaluate and support some of these flags
		// NOTE: `hub help -a` wraps the `git help -a` output
		if (cmd.Use == "clone" && len(os.Args) > 2) || os.Args[1] == "help" {
			// ParseFlags will err in these cases
			err = cmd.ParseFlags(os.Args[1:])
			if err == nil {
				if err := RootCmd.Execute(); err != nil {
					// Execute has already logged the error
					os.Exit(1)
				}
				return
			}
		}

		// Lab passthrough for these commands can cause confusion. See #163
		if os.Args[1] == "create" {
			log.Fatalf("Please call `hub create` directly for github, the lab equivalent is `lab project create`")
		}
		if os.Args[1] == "browse" {
			log.Fatalf("Please call `hub browse` directly for github, the lab equivalent is `lab <object> browse`")
		}
		if os.Args[1] == "alias" {
			log.Fatalf("Please call `hub alias` directly for github, there is no lab equivalent`")
		}

		// Passthrough to git for any unrecognized commands
		err = git.New(os.Args[1:]...).Run()
		if exiterr, ok := err.(*exec.ExitError); ok {
			if status, ok := exiterr.Sys().(syscall.WaitStatus); ok {
				os.Exit(status.ExitStatus())
			}
		}
		if err != nil {
			log.Fatal(err)
		}
		return
	}

	// allow flags to the root cmd to be passed through. Technically we'll drop any exit code info which isn't ideal.
	// TODO: remove for 1.0 when we stop wrapping git
	if cmd.Use == RootCmd.Use && len(os.Args) > 1 {
		var hFlaged bool
		for _, v := range os.Args {
			if v == "--help" {
				hFlaged = true
			}
		}
		if !hFlaged {
			git.New(os.Args[1:]...).Run()
			return
		}
	}
	if err := RootCmd.Execute(); err != nil {
		// Execute has already logged the error
		os.Exit(1)
	}
}

func NoteText() (string, error) {
	return git.EditTextWithFiletype("a message for this note", "vimwiki")
}

func GitEditText(what string) (string, error) {
	return git.EditTextWithFiletype(what, "")
}

func NoteMsg(fileName string, msgs []string) (string, error) {
	if len(msgs) > 0 {
		return strings.Join(msgs[0:], "\n\n"), nil
	}

	text, err := NoteText()
	if err != nil {
		return "", err
	}
	return git.EditFile(fileName, text)
}

func printKV(tbl []trapezi.CompoundCell, key string, value trapezi.Cell) []trapezi.CompoundCell {
	k := trapezi.Text(key)
	k.Align = trapezi.ALIGN_RIGHT

	return append(tbl, trapezi.Join(": ", k, value))
}

func printTable(table []trapezi.CompoundCell) {
	lab.CmdLogger().Infof("Printing table: %d rows", len(table))
	for _, row := range table {
		row.Render(os.Stdout)
		fmt.Println("")
	}
}

func statusCell(status gitlab.BuildStateValue, msg string) *trapezi.SimpleCell {
	cell := trapezi.Text(msg)
	cell.Printer = lab.BuildStateColor(status)

	return cell
}

func trackMRBuildStatusByBranchName(branchName string, status gitlab.BuildStateValue) {
	fileName := ".lab_branch_buildstatus.json"
	updateMRDataByBranchName(fileName, branchName, string(status))
}

func trackMRByBranchName(branchName string, reference string) {
	fileName := ".lab_branch_mr_mapping.json"
	updateMRDataByBranchName(fileName, branchName, reference)
}

func updateMRDataByBranchName(fileName string, branchName string, datum string) {
	branchData := make(map[string]string)
	file, err := os.Open(fileName)

	if err == nil {
		bytes, err := ioutil.ReadAll(file)
		if err != nil {
			lab.CmdLogger().Warningf("Could not read %s: %s", fileName, err)
			return
		}
		err = json.Unmarshal(bytes, &branchData)
		if err != nil {
			lab.CmdLogger().Warningf("Could not parse %s: %s", fileName, err)
			return
		}
	}
	if branchData[branchName] == datum {
		return // nothing to do!
	}

	branchData[branchName] = datum
	bytes, err := json.Marshal(branchData)
	if err != nil {
		lab.CmdLogger().Warningf("Could not serialize data: %v, %s", branchData, err)
		return
	}
	err = ioutil.WriteFile(fileName, bytes, 0644)
	if err != nil {
		lab.CmdLogger().Warningf("Could not write %s: %s", fileName, err)
		return
	}
}

func pluralize(noun string, quantity int64) string {
	if quantity == 0 {
		return ""
	} else if quantity == 1 {
		return "1 " + noun
	} else {
		return fmt.Sprintf("%d %ss", quantity, noun)
	}
}

func layoutTime(when time.Time) string {
	var layout string
	if time.Since(when).Hours() < 12 && time.Now().YearDay() == when.YearDay() {
		layout = time.Kitchen
	} else {
		layout = time.Stamp
	}
	zoneName, _ := when.Zone()
	return fmt.Sprintf("%s %s %s", when.Format(layout), zoneName, sinceMessage(when))
}

func sinceMessage(moment time.Time) string {
	ago := time.Since(moment)
	if ago.Seconds() < 1.0 {
		return ""
	}
	return fmt.Sprintf("(%s)", humanize.Time(moment))
}
