package cmd

import (
	"io/ioutil"
	"log"
	"os"

	"github.com/spf13/cobra"
	gitlab "github.com/xanzy/go-gitlab"
	"github.com/zaquestion/lab/internal/git"
	lab "github.com/zaquestion/lab/internal/gitlab"
	"golang.org/x/crypto/ssh/terminal"
)

var writeOpts struct {
	format string
}

func wikiWrite(cmd *cobra.Command, args []string) {
	projectPath, pageName, err := parseArgsRemoteString(args)
	if err != nil {
		log.Fatal(err)
	}
	if pageName == "" {
		log.Fatal("pageName cannot be empty")
	}
	content, title, found := getContent(projectPath, pageName)

	if found {
		updateWikiPage(projectPath, pageName, title, content)
	} else {
		createWikiPage(projectPath, pageName, content)
	}
}

func updateWikiPage(projectPath, pageName, title, content string) {
	client := lab.Client()
	opts := gitlab.EditWikiPageOptions{
		Content: &content,
		Title:   &title,
		Format:  &writeOpts.format,
	}
	page, _, err := client.Wikis.EditWikiPage(projectPath, pageName, &opts)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Updated %s\n", page.Title)
}

func getContent(projectPath, pageName string) (string, string, bool) {
	oldContent, title, found := getOldContent(projectPath, pageName)

	bytes, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		log.Fatal(err)
	}
	content := string(bytes)

	if content == "" {
		content, err = git.EditFile("wiki/"+pageName, oldContent)
		if err != nil {
			log.Fatal(err)
		}
	}
	if content == "" {
		log.Fatal("Content must not be empty")
	}

	return content, title, found
}

func getOldContent(projectPath, pageName string) (oldContent string, title string, found bool) {
	client := lab.Client()

	page, r, err := client.Wikis.GetWikiPage(projectPath, pageName)
	if r.StatusCode == 404 && terminal.IsTerminal(int(os.Stdout.Fd())) {
		oldContent, err = GitEditText("the content for this wiki page")
		if err != nil {
			log.Fatal(err)
		}
		found = false
		title = pageName
	} else if err != nil {
		log.Fatal(err)
	} else {
		oldContent = page.Content
		title = page.Title
		found = true
	}
	return
}

func createWikiPage(projectPath, pageName, content string) {
	client := lab.Client()
	opts := gitlab.CreateWikiPageOptions{
		Content: &content,
		Title:   &pageName,
		Format:  &writeOpts.format,
	}
	page, _, err := client.Wikis.CreateWikiPage(projectPath, &opts)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Created %s\n", page.Title)
}

func init() {
	wikiWriteCmd := &cobra.Command{
		Use:   "write [projectPath] [page]",
		Short: "Write a page",
		Long: `
		Write a wiki page. This command takes content from stdin or
		an interactive editor, and writes it to the given wiki page.
		`,
		Run: wikiWrite,
	}
	WikiCmd.AddCommand(wikiWriteCmd)
	wikiWriteCmd.Flags().StringVarP(&writeOpts.format, "format", "f", "markdown", "Set the format")
}
