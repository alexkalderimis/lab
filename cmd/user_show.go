package cmd

import (
	"context"
	"fmt"
	"log"

	graphql "github.com/machinebox/graphql"
	"github.com/spf13/cobra"
	lab "github.com/zaquestion/lab/internal/gitlab"
)

var userShowCfg struct {
	username string
}

var userShowCmd = &cobra.Command{
	Use:        "show [remote] [id]",
	Aliases:    []string{"get"},
	ArgAliases: []string{"s"},
	Short:      "Describe a user",
	Long:       ``,
	Run: func(cmd *cobra.Command, args []string) {
		fullPath, _, err := parseArgsRemoteInt(args)
		if err != nil {
			log.Fatal(err)
		}
		user, err := findUser(fullPath)
		if err != nil {
			log.Fatal(err)
		}
		if user.Username == "" {
			log.Fatal("Not found")
		}
		printUser(user)
	},
}

func printUser(user User) {
	fmt.Printf(
		`
@%s
State: %s
Name: %s
Status: %s
`,
		user.Username, user.State, user.Name, user.Status.Message)
}

// User represents a gitlab user
type User struct {
	Name     string
	State    string
	Username string
	Status   struct {
		Message string
	}
}

func findUser(fullPath string) (User, error) {
	client := lab.GQLClient()
	ctx := context.Background()
	var req *graphql.Request
	if userShowCfg.username == "" {
		req = graphql.NewRequest("query { user: currentUser { name state username status { message } } }")
	} else {
		req = graphql.NewRequest("query($username: String!) { user(username: $username) { name state username status { message } } }")
		req.Var("username", userShowCfg.username)
	}
	var resp struct {
		User User
	}
	err := client.Run(ctx, req, &resp)
	return resp.User, err
}

func init() {
	userShowCmd.MarkZshCompPositionalArgumentCustom(1, "__lab_completion_remote")
	userCmd.AddCommand(userShowCmd)
	userShowCmd.Flags().StringVarP(&userShowCfg.username, "username", "u", "", "Show the user with this username")
}
