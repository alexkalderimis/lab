package cmd

import (
	"github.com/spf13/cobra"
)

// userCmd represents the section of commands that operates on users
var userCmd = &cobra.Command{
	Use:   "user",
	Short: `Commands on users`,
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

func init() {
	RootCmd.AddCommand(userCmd)
}
