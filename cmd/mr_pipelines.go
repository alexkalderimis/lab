package cmd

import (
	"context"
	"fmt"
	"log"
	"strings"
	"time"

	humanize "github.com/dustin/go-humanize"
	graphql "github.com/machinebox/graphql"
	"github.com/spf13/cobra"
	gitlab "github.com/xanzy/go-gitlab"
	lab "github.com/zaquestion/lab/internal/gitlab"
	"github.com/zaquestion/lab/internal/types"
	"gitlab.com/alexkalderimis/trapezi"
)

var mrPipelinesConfig struct {
	currentUserName *string
	titlePrinter    trapezi.CellPrinter
	currentMrIID    string
	header          bool
	rows            int
}

// listCmd represents the list command
var pipelinesCmd = &cobra.Command{
	Use:     "pipelines [remote] [iid]",
	Aliases: []string{"ps"},
	Short:   "List pipelines for a merge request",
	Long:    ``,
	Args:    cobra.MaximumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		var err error
		pid, idents, err := parseProjectMR(args, StateOpen)
		if err != nil {
			log.Fatal(err)
		}
		mrPipelinesConfig.currentMrIID = idents.IID

		pipelines, err := fetchPipelines(pid, idents)
		if err != nil {
			log.Fatal(err)
		}

		printPLTable(pipelines)
	},
}

func printPLTable(pipelines []types.PipelineNode) {
	var table []trapezi.CompoundCell

	if mrPipelinesConfig.header && len(pipelines) != 0 {
		table = append(table, trapezi.Join(" | ",
			trapezi.Text("Status"),
			trapezi.Text("Created At"),
			trapezi.Text("Duration"),
			trapezi.Text("Finished At"),
			trapezi.Text("Passed"),
			trapezi.Text("Failed"),
			trapezi.Text("Total")))
		table = append(table, trapezi.Join("-|-",
			trapezi.PaddedWith('-', trapezi.Empty()),
			trapezi.PaddedWith('-', trapezi.Empty()),
			trapezi.PaddedWith('-', trapezi.Empty()),
			trapezi.PaddedWith('-', trapezi.Empty()),
			trapezi.PaddedWith('-', trapezi.Empty()),
			trapezi.PaddedWith('-', trapezi.Empty()),
			trapezi.PaddedWith('-', trapezi.Empty())))
	}

	for _, entry := range pipelines {
		table = append(table, renderPipeline(entry))
	}
	trapezi.AlignTable(table)
	printTable(table)
}

func fetchPipelines(pid string, idents *MergeRequestIdentifiers) ([]types.PipelineNode, error) {
	client := lab.GQLClient()
	query := `
		query ($projectPath: ID!, $iid: String!, $n: Int) {
		  project(fullPath: $projectPath) {
			  mergeRequest(iid: $iid) {
					pipelines(first: $n) {
					  nodes {
						  status
							createdAt
							finishedAt
							duration
							success: jobs(statuses: [SKIPPED, SUCCESS]) { count }
							failed: jobs(statuses: [FAILED]) { count }
							total: jobs { count }
						}
					}
				}
			}
		}
		`
	req := graphql.NewRequest(query)
	if mrPipelinesConfig.rows > 0 {
		req.Var("n", mrPipelinesConfig.rows)
	}
	ctx := context.Background()
	var resp struct {
		Project struct {
			MergeRequest struct {
				Pipelines struct {
					Nodes []types.PipelineNode
				}
			}
		}
	}
	req.Var("projectPath", pid)
	req.Var("iid", idents.IID)
	err := client.Run(ctx, req, &resp)
	return resp.Project.MergeRequest.Pipelines.Nodes, err
}

func renderPipeline(entry types.PipelineNode) trapezi.CompoundCell {
	now := time.Now()
	var row [7]trapezi.Cell
	row[0] = statusCell(gitlab.BuildStateValue(strings.ToLower(entry.Status)), entry.Status)
	row[1] = trapezi.Text(humanize.RelTime(*entry.CreatedAt, now, "ago", ""))
	if entry.Duration > 0 {
		row[2] = trapezi.Text(fmt.Sprintf("%02.2f minutes", float64(entry.Duration)/60.0))
	} else {
		row[2] = trapezi.Text("...")
	}
	if entry.FinishedAt == nil {
		row[3] = trapezi.Text("")
	} else {
		row[3] = trapezi.Text(humanize.RelTime(*entry.FinishedAt, now, "ago", ""))
	}
	row[4] = trapezi.AlignRight(statusCell(gitlab.Success, fmt.Sprintf("%d", entry.Success.Count)))
	row[5] = trapezi.AlignRight(statusCell(gitlab.Failed, fmt.Sprintf("%d", entry.Failed.Count)))
	row[6] = trapezi.Text(fmt.Sprintf("%d", entry.Total.Count))

	return trapezi.CompoundCell{Cells: row[:], Divider: " | "}
}

func init() {
	pipelinesCmd.MarkZshCompPositionalArgumentCustom(1, "__lab_completion_remote")
	pipelinesCmd.Flags().BoolVarP(&mrPipelinesConfig.header, "header", "", false, "Show column headers")
	pipelinesCmd.Flags().IntVarP(&mrPipelinesConfig.rows, "rows", "n", 0, "Limit number of rows")
	mrCmd.AddCommand(pipelinesCmd)
}
