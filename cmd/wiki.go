package cmd

import (
	"github.com/spf13/cobra"
)

var WikiCmd = &cobra.Command{
	Use:   "wiki",
	Short: "Commands for managing wiki pages",
}

func init() {
	RootCmd.AddCommand(WikiCmd)
}
