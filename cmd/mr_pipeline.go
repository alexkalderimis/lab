package cmd

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"
	lab "github.com/zaquestion/lab/internal/gitlab"
)

var mrPipelineCmd = &cobra.Command{
	Use:     "pipeline [remote] [id]",
	Aliases: []string{"run"},
	Short:   "Run a pipeline for a merge request",
	Run: func(cmd *cobra.Command, args []string) {
		_, mr, err := parseProjectMR(args, StateOpen)
		if err != nil {
			log.Fatal(err)
		}

		p, err := lab.MRPipelineCreate(mr.ProjectID, mr.IID)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Printf("Started new pipeline: %d\nSee: %s\n", p.ID, p.WebURL)
	},
}

func init() {
	mrPipelineCmd.MarkZshCompPositionalArgumentCustom(1, "__lab_completion_remote")
	mrPipelineCmd.MarkZshCompPositionalArgumentCustom(2, "__lab_completion_merge_request $words[2]")
	mrCmd.AddCommand(mrPipelineCmd)
}
