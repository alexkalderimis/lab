package cmd

import (
	"log"
	"os"
	"os/exec"

	"github.com/spf13/cobra"
	lab "github.com/zaquestion/lab/internal/gitlab"
)

var projectCloneCmdConfig struct {
	HTTP bool
}

var projectCloneCmd = &cobra.Command{
	Use:     "clone name [localName]",
	Aliases: []string{"c"},
	Short:   "Clone a project",
	Long:    ``,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			log.Fatal("project name is required'")
		}
		if len(args) > 2 {
			log.Fatal("at most two arguments are allowed")
		}
		name := args[0]
		localName := name
		if len(args) == 2 {
			localName = args[1]
		}

		p, err := lab.FindProject(name)

		if err != nil {
			log.Fatal(err)
		}

		var url string
		if projectCloneCmdConfig.HTTP {
			url = p.HTTPURLToRepo
		} else {
			url = p.SSHURLToRepo
		}
		git := exec.Command("git", "clone", url, localName)
		git.Stdout = os.Stdout
		git.Stderr = os.Stderr
		err = git.Run()
		if err != nil {
			log.Fatal(err)
		}
	},
}

func init() {
	projectCmd.AddCommand(projectCloneCmd)
	projectCloneCmd.Flags().BoolVarP(&projectCloneCmdConfig.HTTP, "http", "", false, "Clone using HTTP URL")
	projectCloneCmd.MarkZshCompPositionalArgumentCustom(1, "()")
}
