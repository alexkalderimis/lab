package cmd

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"

	graphql "github.com/machinebox/graphql"
	"github.com/spf13/cobra"
	gitlab "github.com/xanzy/go-gitlab"
	"github.com/zaquestion/lab/internal/git"
	lab "github.com/zaquestion/lab/internal/gitlab"
	types "github.com/zaquestion/lab/internal/types"
	"gitlab.com/alexkalderimis/trapezi"
)

var mrShowCfg struct {
	showDescription bool
	showJSON        bool
	labelPrefix     string
}

var mrShowCmd = &cobra.Command{
	Use:        "show [remote] [id]",
	Aliases:    []string{"get"},
	ArgAliases: []string{"s"},
	Short:      "Describe a merge request",
	Long:       ``,
	Run: func(cmd *cobra.Command, args []string) {
		rn, iid, err := parseArgs(args)
		if err != nil {
			log.Fatal(err)
		}
		showdata, err := useGraphql(rn, iid)
		if err != nil {
			log.Fatal(err)
		}
		if mrShowCfg.showJSON {
			bytes, err := json.Marshal(showdata)
			if err != nil {
				log.Fatal(err)
			}
			fmt.Println(string(bytes))
		} else {
			printMR(showdata)
		}
	},
}

func printMR(mr MergeRequest) {
	assignees := FormatUserlist(mr.Assignees.Nodes)
	milestone := "None"
	state := map[string]string{
		"opened": "Open",
		"closed": "Closed",
		"merged": "Merged",
	}[mr.State]

	if mr.Milestone.Title != "" {
		milestone = mr.Milestone.Title
	}
	maxLineWidth := 80
	var labelLines []string

	if len(mr.Labels.Edges) > 0 {
		var buff strings.Builder
		for _, edge := range mr.Labels.Edges {
			if buff.Len() > maxLineWidth {
				labelLines = append(labelLines, buff.String())
				buff.Reset()
			}
			if strings.HasPrefix(edge.Node.Title, mrShowCfg.labelPrefix) {
				if buff.Len() > 0 {
					buff.WriteString(", ")
				}
				buff.WriteString(edge.Node.Title)
			}
		}
		labelLines = append(labelLines, buff.String())
	}

	fmt.Printf("!%s %s\n===================================", mr.IID, mr.Title)
	if mrShowCfg.showDescription {
		fmt.Printf("\n%s\n-----------------------------------", mr.Description)
	}

	var kvs []trapezi.CompoundCell
	kvs = printKV(kvs, "Project", trapezi.Text(mr.Project.Name))
	kvs = printKV(kvs, "Branches", trapezi.Text(fmt.Sprintf("%s >=> %s", mr.SourceBranch, mr.TargetBranch)))
	kvs = printKV(kvs, "Status", trapezi.Join(" ",
		trapezi.Text(state),
		statusCell(gitlab.BuildStateValue(strings.ToLower(mr.HeadPipeline.Status)), fmt.Sprintf("(%s)", mr.HeadPipeline.Status)),
		trapezi.Text(mr.MergeStatus)))
	kvs = printKV(kvs, "Assignees", trapezi.Text(assignees))
	kvs = printKV(kvs, "Current Reviewers", trapezi.Text(FormatUserlist(mr.Reviewers.Nodes)))
	kvs = printKV(kvs, "Approved By", trapezi.Text(fmt.Sprintf("(%d) %s", len(mr.ApprovedBy.Nodes), FormatUserlist(mr.ApprovedBy.Nodes))))
	kvs = printKV(kvs, "Author", trapezi.Text(mr.Author.Username))
	kvs = printKV(kvs, "Milestone", trapezi.Text(milestone))
	if len(labelLines) > 0 {
		for i, line := range labelLines {
			key := ""
			if i == 0 {
				key = "Labels"
			}
			kvs = printKV(kvs, key, trapezi.Text(line))
		}
	}
	kvs = printKV(kvs, "WebURL", trapezi.Text(mr.WebURL))
	trapezi.AlignTable(kvs)
	fmt.Println()
	printTable(kvs)
}

func FormatUserlist(users []types.NamedUser) string {
	var sb strings.Builder
	if len(users) == 0 {
		return "None"
	}
	for _, user := range users {
		if user.Username == "" {
			continue
		}

		if sb.Len() > 0 {
			sb.WriteString(", ")
		}
		sb.WriteString(user.Username)
	}
	return sb.String()
}

type MergeRequest struct {
	IID          string
	Title        string
	Description  string
	SourceBranch string
	TargetBranch string
	State        string
	MergeStatus  string
	WebURL       string
	HeadPipeline struct {
		Status string
	}
	Author    types.NamedUser
	Milestone types.Titled
	Assignees struct {
		Nodes []types.NamedUser
	}
	ApprovedBy struct {
		Nodes []types.NamedUser
	}
	Labels struct {
		Edges []struct {
			Node types.Titled
		}
	}
	Project struct {
		Name string
	}
	Participants struct {
		Nodes []types.NamedUser
	}
	Reviewers struct {
		Nodes []types.NamedUser
	}
}

const mrFragment = `
fragment mrFields on MergeRequest {
	iid
	title
	description
	sourceBranch
	targetBranch
	state
	mergeStatus
	webUrl
	headPipeline { status }
	author { username }
	participants { nodes { username } }
	assignees { nodes { username } }
	reviewers { nodes { username } }
	approvedBy { nodes { username } }
	milestone {
		title
	}
	labels {
		edges {
			node { title }
		}
	}
	project { name }
}
`

const mrReqByBranch = `
query ($projectPath: ID!, $sourceBranch: String!) {
	project(fullPath: $projectPath) {
		mrs: mergeRequests(first: 1, sourceBranches: [$sourceBranch], state:opened) {
			nodes {
				...mrFields
			}
		}
		closed: mergeRequests(first: 1, sourceBranches: [$sourceBranch]) {
			nodes {
				...mrFields
			}
		}
	}
}
`

const mrReqByIID = `
query ($projectPath: ID!, $iid: String!) {
	project(fullPath: $projectPath) {
		mrs: mergeRequests(first: 1, iids: [$iid]) {
			nodes {
				...mrFields
			}
		}
	}
}
`

func useGraphql(pid string, iid int64) (MergeRequest, error) {
	client := lab.GQLClient()
	// make a request
	var queryText string
	if iid > 0 {
		queryText = mrReqByIID + "\n\n" + mrFragment
	} else {
		queryText = mrReqByBranch + "\n\n" + mrFragment
	}
	lab.CmdLogger().Debugf("queryText: %s", queryText)
	req := graphql.NewRequest(queryText)
	ctx := context.Background()
	var resp struct {
		Project struct {
			Mrs struct {
				Nodes []MergeRequest
			}
			Closed struct {
				Nodes []MergeRequest
			}
		}
	}

	req.Var("projectPath", pid)
	if iid <= 0 {
		branch, err := git.CurrentBranch()
		if err != nil {
			return MergeRequest{}, err
		}
		req.Var("sourceBranch", branch)
		lab.CmdLogger().Debugf("pid: %s, branch: %s", pid, branch)
	} else {
		req.Var("iid", strconv.FormatInt(iid, 10))
		lab.CmdLogger().Debugf("pid: %s, iid: %d", pid, iid)
	}

	err := client.Run(ctx, req, &resp)
	if err != nil {
		return MergeRequest{}, err
	}
	if len(resp.Project.Mrs.Nodes) == 0 && len(resp.Project.Closed.Nodes) == 0 {
		return MergeRequest{}, err
	}
	if len(resp.Project.Mrs.Nodes) == 0 && len(resp.Project.Closed.Nodes) == 1 {
		return resp.Project.Closed.Nodes[0], err
	}
	return resp.Project.Mrs.Nodes[0], err
}

func init() {
	if fileInfo, _ := os.Stdout.Stat(); (fileInfo.Mode() & os.ModeCharDevice) != 0 {
		// we are on a tty
		lab.UseColor = true
	}
	mrShowCmd.MarkZshCompPositionalArgumentCustom(1, "__lab_completion_remote")
	mrShowCmd.MarkZshCompPositionalArgumentCustom(2, "__lab_completion_merge_request $words[2]")
	mrCmd.AddCommand(mrShowCmd)
	mrShowCmd.Flags().BoolVarP(&mrShowCfg.showDescription, "description", "d", false, "Show the full description")
	mrShowCmd.Flags().StringVarP(&mrShowCfg.labelPrefix, "labels", "l", "", "Show labels with this prefix")
	mrShowCmd.Flags().BoolVarP(&mrShowCfg.showJSON, "json", "j", false, "Print the result as JSON")
}
