package cmd

import (
	"fmt"
	"log"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	gitlab "github.com/xanzy/go-gitlab"
	"github.com/zaquestion/lab/internal/git"
	lab "github.com/zaquestion/lab/internal/gitlab"
)

var mrEditCmd = &cobra.Command{
	Use:        "edit [remote] [id]",
	ArgAliases: []string{"e"},
	Short:      "Edit a merge request",
	Long:       ``,
	Run: func(cmd *cobra.Command, args []string) {
		rn, mrIID, err := parseArgs(args)
		if err != nil {
			log.Fatal(err)
		}
		showdata, err := useGraphql(rn, mrIID)
		iid, err := strconv.Atoi(showdata.IID)
		if err != nil {
			log.Fatal(err)
		}
		preferredFiletype := viper.GetString("edit.filetype")

		hdr, err := git.EditTextWithFiletype("MR Description", preferredFiletype)
		if err != nil {
			log.Fatal(err)
		}

		text := showdata.Description + "\n" + hdr
		desc, err := git.EditFile("MR_DESC.md", text)
		if err != nil {
			log.Fatal(err)
		}
		if strings.Trim(desc, " \n") == "" {
			fmt.Println("Aborting due to empty description")
			return
		}

		client := lab.Client()
		opts := gitlab.UpdateMergeRequestOptions{
			Description: &desc,
		}
		mr, _, err := client.MergeRequests.UpdateMergeRequest(rn, iid, &opts)

		if err != nil {
			log.Fatal(err)
		}

		fmt.Printf("Updated %s: %s\n", mr.Reference, mr.Title)
	},
}

func init() {
	mrEditCmd.MarkZshCompPositionalArgumentCustom(1, "__lab_completion_remote")
	mrEditCmd.MarkZshCompPositionalArgumentCustom(2, "__lab_completion_merge_request $words[2]")
	mrCmd.AddCommand(mrEditCmd)
}
