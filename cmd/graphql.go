package cmd

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"strings"

	graphql "github.com/machinebox/graphql"
	"github.com/spf13/cobra"
	lab "github.com/zaquestion/lab/internal/gitlab"
)

var gqlBasicVars []string

var graphqlCmd = &cobra.Command{
	Use:     "graphql [remote] GQL [JSON_VARIABLES]",
	Aliases: []string{"gql"},
	Short:   "Send a GraphQL query",
	Run: func(cmd *cobra.Command, args []string) {
		var vars string
		if len(args) > 1 {
			vars = args[1]
		}

		resp, err := sendGraphqlRequest(args[0], vars)
		if err != nil {
			log.Fatal(err)
		}
		b, err := json.Marshal(resp)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Print(string(b))
	},
}

type JSONValue = interface{}

type GraphQLResponse = map[string]JSONValue

func sendGraphqlRequest(gql string, vars string) (GraphQLResponse, error) {
	var resp GraphQLResponse
	client := lab.GQLClient()
	req := graphql.NewRequest(gql)
	var variables = make(map[string]JSONValue)

	if vars != "" {
		err := json.Unmarshal([]byte(vars), &variables)
		if err != nil {
			log.Fatalf("Error parsing variables: %v", err)
		}
	}

	lab.CmdLogger().Debugf("basic vars:", gqlBasicVars)
	for _, basic := range gqlBasicVars {
		parts := strings.SplitN(basic, ":", 2)
		lab.CmdLogger().Debugf("Parsed %v to %v", basic, parts)
		if len(parts) != 2 {
			log.Fatalf("Bad variable. Cannot parse %s", basic)
		}
		lab.CmdLogger().Debugf("Setting %v to %v", parts[0], parts[1])
		variables[parts[0]] = parts[1]
	}

	for k, v := range variables {
		req.Var(k, v)
	}
	ctx := context.Background()
	err := client.Run(ctx, req, &resp)

	return resp, err
}

func init() {
	RootCmd.AddCommand(graphqlCmd)
	graphqlCmd.Flags().StringSliceVarP(&gqlBasicVars, "var", "v", gqlBasicVars, "set a string variable: key:value")
}
