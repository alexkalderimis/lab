package cmd

import (
	"log"

	"github.com/spf13/cobra"
	lab "github.com/zaquestion/lab/internal/gitlab"
)

var mrUncacheCmd = &cobra.Command{
	Use:        "uncache [remote] [id]",
	Aliases:    []string{},
	ArgAliases: []string{},
	Short:      "Clear the cache of any MR related things",
	Long:       ``,
	Run: func(cmd *cobra.Command, args []string) {
		_, mr, err := parseProjectMR(args, StateOpen)
		if err != nil {
			if err.Error() != "Could not find MergeRequest" {
				log.Fatal(err)
			}
			return
		}
		err = lab.Uncache(mrCacheKey(mr.ID))
		if err != nil {
			log.Fatal(err)
		}
	},
}

func init() {
	cmd := mrUncacheCmd
	cmd.MarkZshCompPositionalArgumentCustom(1, "__lab_completion_remote")
	cmd.MarkZshCompPositionalArgumentCustom(2, "__lab_completion_merge_request $words[2]")
	mrCmd.AddCommand(cmd)
}
