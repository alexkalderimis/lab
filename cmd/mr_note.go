package cmd

import (
	"fmt"
	"log"
	"runtime"
	"strconv"

	"github.com/spf13/cobra"
	gitlab "github.com/xanzy/go-gitlab"
	lab "github.com/zaquestion/lab/internal/gitlab"
)

var (
	discussionID string
)

var mrCreateNoteCmd = &cobra.Command{
	Use:     "note [remote] <id>",
	Aliases: []string{"comment"},
	Short:   "Add a note or comment to an MR on GitLab",
	Long:    ``,
	Run: func(cmd *cobra.Command, args []string) {
		rn, mrNum, err := parseArgs(args)
		if err != nil {
			log.Fatal(err)
		}
		mrID, err := resolveMrIdWithFallback(rn, mrNum, StateOpen)
		if err != nil {
			log.Fatal(err)
		}

		msgs, err := cmd.Flags().GetStringArray("message")
		if err != nil {
			log.Fatal(err)
		}

		body, err := NoteMsg("MR_NOTE", msgs)
		if err != nil {
			_, f, l, _ := runtime.Caller(0)
			log.Fatal(f+":"+strconv.Itoa(l)+" ", err)
		}
		if body == "" {
			log.Fatal("aborting note due to empty note msg")
		}
		lab.CmdLogger().Debugf("adding note: %s", body)
		noteURL, note, err := lab.MRCreateNote(rn, discussionID, int(mrID), &gitlab.CreateMergeRequestNoteOptions{
			Body: &body,
		})
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(noteURL)

		if note.ID != 0 && note.Body != "" {
			fmt.Println(note.Body)
		}

	},
}

func init() {
	mrCreateNoteCmd.Flags().StringArrayP("message", "m", []string{}, "Use the given <msg>; multiple -m are concatenated as separate paragraphs")
	mrCreateNoteCmd.Flags().StringVarP(&discussionID, "reply-to", "r", "", "Add this comment to the specified discussion thread")

	mrCreateNoteCmd.MarkZshCompPositionalArgumentCustom(1, "__lab_completion_remote")
	mrCreateNoteCmd.MarkZshCompPositionalArgumentCustom(2, "__lab_completion_merge_request $words[2]")
	mrCmd.AddCommand(mrCreateNoteCmd)
}
