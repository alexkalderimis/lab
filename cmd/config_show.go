package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var configShowCmd = &cobra.Command{
	Use:        "show [key]",
	ArgAliases: []string{"get"},
	Short:      "View config values",
	Long:       ``,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 1 {
			key := args[0]
			value := viper.Get(key)
			m := make(map[string]interface{})
			m[key] = value
			printMap("", "", m)
		} else {
			conf := viper.AllSettings()
			printMap("", "", conf)
		}
	},
}

func printMap(indent, k string, m map[string]interface{}) {
	for key, value := range m {
		path := k
		if path == "" {
			path = key
		} else {
			path += "." + key
		}

		switch v := value.(type) {
		case string:
			fmt.Printf("%s%s = %s\n", indent, path, v)
		case map[string]interface{}:
			printMap(indent, path, v)
		case []map[string]interface{}:
			fmt.Printf("%s%s:\n", indent, path)
			printArray(indent, v)
		default:
			fmt.Printf("%s = %v\n", path, v)
		}
	}
}

func printArray(indent string, array []map[string]interface{}) {
	for _, conf := range array {
		printMap(indent+" ", "", conf)
	}
}

func init() {
	configCmd.AddCommand(configShowCmd)
}
