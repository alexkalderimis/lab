package cmd

import (
	"github.com/spf13/cobra"
)

var configCmd = &cobra.Command{
	Use:   "config",
	Short: `Configuration commands`,
}

func init() {
	RootCmd.AddCommand(configCmd)
}
