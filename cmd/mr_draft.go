package cmd

import (
	"context"
	"errors"
	"fmt"
	"log"
	"strings"

	graphql "github.com/machinebox/graphql"
	"github.com/spf13/cobra"
	lab "github.com/zaquestion/lab/internal/gitlab"
)

var newDraftStatus bool

func setDraft(fullPath string, iid string) (string, bool, error) {
	var resp struct {
		R struct {
			Errors       []string
			MergeRequest struct {
				Reference string
				Wip       bool
			}
		}
	}
	query := `
	mutation SetDraft($path: ID!, $iid: String!, $draft: Boolean!) {
		R: mergeRequestSetDraft(input: { projectPath: $path, iid: $iid, draft: $draft }) {
		  errors
			mergeRequest {
			  reference
			  wip: draft
			}
	  }	
	}
  `

	client := lab.GQLClient()
	req := graphql.NewRequest(query)
	ctx := context.Background()
	req.Var("path", fullPath)
	req.Var("iid", iid)
	req.Var("draft", newDraftStatus)

	err := client.Run(ctx, req, &resp)
	if err != nil {
		return "", false, err
	}

	if len(resp.R.Errors) > 0 {
		msg := strings.Join(resp.R.Errors, "; ")
		return "", false, errors.New("Could not set draft: " + msg)
	}

	return resp.R.MergeRequest.Reference, resp.R.MergeRequest.Wip, nil
}

var mrDraftCmd = &cobra.Command{
	Use:     "draft [remote] <iid>",
	Aliases: []string{"wip"},
	Short:   "Toggle draft-status",
	Long:    ``,
	Run: func(cmd *cobra.Command, args []string) {
		fullPath, mrIdents, err := parseProjectMR(args, StateOpen)
		if err != nil {
			log.Fatal(err)
		}
		ref, isDraft, err := setDraft(fullPath, mrIdents.IID)

		if err != nil {
			log.Fatal(err)
		}

		fmt.Printf("Merge Request %s draft status is now %v\n", ref, isDraft)
	},
}

func init() {
	mrDraftCmd.MarkZshCompPositionalArgumentCustom(1, "__lab_completion_remote")
	mrDraftCmd.MarkZshCompPositionalArgumentCustom(2, "__lab_completion_merge_request $words[2]")
	mrDraftCmd.Flags().BoolVarP(&newDraftStatus, "wip", "w", true, "Mark as WIP")
	mrCmd.AddCommand(mrDraftCmd)
}
