package cmd

import (
	"fmt"
	"log"
	"runtime"
	"strconv"

	"github.com/spf13/cobra"
	gitlab "github.com/xanzy/go-gitlab"
	lab "github.com/zaquestion/lab/internal/gitlab"
)

var issueCreateNoteCmd = &cobra.Command{
	Use:     "note [remote] <id>",
	Aliases: []string{"comment"},
	Short:   "Add a note or comment to an issue on GitLab",
	Long:    ``,
	Args:    cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		rn, issueNum, err := parseArgs(args)
		if err != nil {
			log.Fatal(err)
		}

		msgs, err := cmd.Flags().GetStringSlice("message")
		if err != nil {
			log.Fatal(err)
		}

		body, err := NoteMsg("ISSUE_NOTE", msgs)
		if err != nil {
			_, f, l, _ := runtime.Caller(0)
			log.Fatal(f+":"+strconv.Itoa(l)+" ", err)
		}
		if body == "" {
			log.Fatal("aborting note due to empty note msg")
		}

		lab.CmdLogger().Debugf("adding note: %s", body)
		noteURL, err := lab.IssueCreateNote(rn, int(issueNum), &gitlab.CreateIssueNoteOptions{
			Body: &body,
		})
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(noteURL)
	},
}

//
func init() {
	issueCreateNoteCmd.Flags().StringSliceP("message", "m", []string{}, "Use the given <msg>; multiple -m are concatenated as separate paragraphs")

	issueCreateNoteCmd.MarkZshCompPositionalArgumentCustom(1, "__lab_completion_remote")
	issueCreateNoteCmd.MarkZshCompPositionalArgumentCustom(2, "__lab_completion_issue $words[2]")
	issueCmd.AddCommand(issueCreateNoteCmd)
}
