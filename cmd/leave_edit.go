package cmd

import (
	"github.com/spf13/cobra"
	"log"
	"strconv"
	"time"

	cal "github.com/zaquestion/lab/internal/calendar"
)

var editLeaveCfg struct {
	start string
	end   string
	name  string
}
var editLeaveCmd = &cobra.Command{
	Use:   "edit index",
	Short: "Add a new leave period",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 1 {
			log.Fatal(cmd.Help())
		}
		index, err := strconv.Atoi(args[0])
		mustNot(err)
		period, err := cal.GetLeave(index)
		mustNot(err)

		if editLeaveCfg.name != "" {
			period.Name = editLeaveCfg.name
		}
		if editLeaveCfg.start != "" {
			d, err := time.Parse("2006-01-02", editLeaveCfg.start)
			mustNot(err)
			period.Start = cal.Date{Time: d}
		}
		if editLeaveCfg.end != "" {
			d, err := time.Parse("2006-01-02", editLeaveCfg.end)
			mustNot(err)
			period.End = cal.Date{Time: d}
		}
		if period.Start.After(period.End.Time) {
			log.Fatalf("Illegal period: start must be before or equal to end. Got start=%s, end=%s", period.Start, period.End)
		}

		err = cal.EditLeave(index, period)
		mustNot(err)
	},
}

func init() {
	LeaveCmd.AddCommand(editLeaveCmd)
	editLeaveCmd.Flags().StringVarP(&editLeaveCfg.start, "start", "s", "", "New start date")
	editLeaveCmd.Flags().StringVarP(&editLeaveCfg.end, "end", "e", "", "New end date")
	editLeaveCmd.Flags().StringVarP(&editLeaveCfg.name, "name", "n", "", "New name")
}
