package cmd

import (
	"github.com/spf13/cobra"
)

var LeaveCmd = &cobra.Command{
	Use:   "leave",
	Short: "Commands for managing leave",
}

func init() {
	RootCmd.AddCommand(LeaveCmd)
}
