package cmd

import (
	"github.com/spf13/cobra"
	"log"
	"time"

	cal "github.com/zaquestion/lab/internal/calendar"
)

var addLeaveCmd = &cobra.Command{
	Use:   "add [start] [end] [name]",
	Short: "Add a new leave period",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 3 {
			log.Fatal(cmd.Help())
		}
		var name = args[2]

		start, err := time.Parse("2006-01-02", args[0])
		if err != nil {
			log.Fatal(err)
		}
		end, err := time.Parse("2006-01-02", args[1])
		if err != nil {
			log.Fatal(err)
		}
		if start.After(end) {
			log.Fatalf("Illegal period: start must be before or equal to end. Got start=%s, end=%s", args[0], args[1])
		}

		period := cal.LeavePeriod{
			Name:  name,
			Start: cal.Date{Time: start},
			End:   cal.Date{Time: end},
		}

		err = cal.AddLeave(period)
		if err != nil {
			log.Fatal(err)
		}
	},
}

func init() {
	LeaveCmd.AddCommand(addLeaveCmd)
}
