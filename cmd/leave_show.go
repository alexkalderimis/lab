package cmd

import (
	"fmt"
	"github.com/spf13/cobra"

	cal "github.com/zaquestion/lab/internal/calendar"
	"gitlab.com/alexkalderimis/trapezi"
)

var showLeaveCmd = &cobra.Command{
	Use:   "show",
	Short: "Show the current leave list",
	Run: func(cmd *cobra.Command, args []string) {
		var table []trapezi.CompoundCell

		for i, period := range cal.LoadLeave() {
			row := []trapezi.Cell{
				trapezi.Join(": ", trapezi.Text(fmt.Sprintf("%d", i)), trapezi.Text(period.Name)),
				trapezi.Text(period.Start.Format("2006-01-02")),
				trapezi.Text(period.End.Format("2006-01-02")),
			}
			table = append(table, trapezi.CompoundCell{
				Cells:   row,
				Divider: " | ",
			})
		}

		if len(table) == 0 {
			fmt.Printf("No leave\n")
		} else {
			trapezi.AlignTable(table)
			printTable(table)
		}
	},
}

func init() {
	LeaveCmd.AddCommand(showLeaveCmd)
}
