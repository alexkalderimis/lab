package cmd

import (
	"log"
	"net/url"
	"path"

	"github.com/spf13/cobra"
	lab "github.com/zaquestion/lab/internal/gitlab"
)

var mrBrowseCfg struct {
	mrState string
}

var mrBrowseCmd = &cobra.Command{
	Use:     "browse [remote] <id>",
	Aliases: []string{"b"},
	Short:   "View merge request in a browser",
	Long:    ``,
	Run: func(cmd *cobra.Command, args []string) {
		rn, mr, err := parseProjectMR(args, MRState(mrBrowseCfg.mrState))
		if err != nil {
			log.Fatal(err)
		}

		hostURL, err := url.Parse(lab.Host())

		if err != nil {
			log.Fatal(err)
		}

		hostURL.Path = path.Join(hostURL.Path, rn, "merge_requests")
		hostURL.Path = path.Join(hostURL.Path, mr.IID)

		err = browse(hostURL.String())
		if err != nil {
			log.Fatal(err)
		}
	},
}

func init() {
	mrBrowseCmd.MarkZshCompPositionalArgumentCustom(1, "__lab_completion_remote")
	mrBrowseCmd.MarkZshCompPositionalArgumentCustom(2, "__lab_completion_merge_request $words[2]")
	mrBrowseCmd.Flags().StringVarP(
		&mrBrowseCfg.mrState, "state", "s", string(StateOpen),
		"filter merge requests by state (opened/closed/merged)")
	mrCmd.AddCommand(mrBrowseCmd)
}
