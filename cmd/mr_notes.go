package cmd

import (
	"bufio"
	"fmt"
	"io"
	"log"
	exec "os/exec"
	strings "strings"
	"time"
	"unicode"

	"github.com/spf13/cobra"
	lab "github.com/zaquestion/lab/internal/gitlab"
	"github.com/zaquestion/lab/internal/interactive"
	"github.com/zaquestion/lab/internal/notes"
	"golang.org/x/crypto/ssh/terminal"
)

type printNoteCfg struct {
	lineLen       int
	contextWindow int
	nowrap        bool
	showURL       bool
}

var mrNotesCfg struct {
	printNoteCfg
	includeSystemNotes bool
	ignoreResolved     bool
	ignoreUsers        []string
	onlyUsers          []string
	checkOnly          bool
	threaded           bool
	ignore             map[string]bool
	only               map[string]bool
	interactive        bool
}

var mrNotesCmd = &cobra.Command{
	Use:        "notes [remote] [id]",
	Aliases:    []string{"get"},
	ArgAliases: []string{"s"},
	Short:      "Show notes of a Merge Request",
	Long:       ``,
	Run: func(cmd *cobra.Command, args []string) {
		mrNotesCfg.checkOnly = len(mrNotesCfg.onlyUsers) > 0

		_, mr, err := parseProjectMR(args, StateOpen)
		if err != nil {
			log.Fatal(err)
		}
		for _, username := range mrNotesCfg.ignoreUsers {
			mrNotesCfg.ignore[username] = true
		}
		for _, username := range mrNotesCfg.onlyUsers {
			mrNotesCfg.only[username] = true
		}
		if mrNotesCfg.interactive {
			discussions := notes.MRDiscussions(mr.MergeRequestID())
			interactive.MRNotesApp(&discussions)
		} else if mrNotesCfg.threaded {
			getDiscussions(mr)
		} else {
			getNotes(mr)
		}
	},
}

func getNotes(mr *MergeRequestIdentifiers) {
	getNotesSince(mr, nil)
}

func getNotesSince(mr *MergeRequestIdentifiers, cutoff *time.Time) {
	notes := notes.MRNotes(mr.MergeRequestID())

	for {
		note, err := notes.NextNote()

		if err != nil {
			log.Fatal(err)
		}

		if note == nil {
			break
		}

		if cutoff != nil && !note.CreatedAt.After(*cutoff) {
			continue
		}
		if note.System && !mrNotesCfg.includeSystemNotes {
			continue
		}
		if mrNotesCfg.ignore[note.Author.Username] {
			continue
		}
		if mrNotesCfg.checkOnly && !mrNotesCfg.only[note.Author.Username] {
			continue
		}
		if note.Resolved && mrNotesCfg.ignoreResolved {
			continue
		}
		fmt.Println(printNote(*note, false, mrNotesCfg.printNoteCfg))
	}
}

func getDiscussions(mr *MergeRequestIdentifiers) {
	discussions := notes.MRDiscussions(mr.MergeRequestID())

	for {
		d, err := discussions.NextDiscussion()
		if err != nil {
			log.Fatal(err)
		}
		if d == nil {
			break
		}

		showDiscussion(*d)
	}
}

func showDiscussion(discussion notes.Discussion) {
	var w strings.Builder

	written := 0
	// only include discussions with a relevant author
	if mrNotesCfg.checkOnly {
		hasAuthor := false
	HAS_AUTHOR:
		for _, note := range discussion.Notes.Nodes {
			if mrNotesCfg.only[note.Author.Username] {
				hasAuthor = true
				break HAS_AUTHOR
			}
		}
		if !hasAuthor {
			return
		}
	}

	// print out each note, dealing with per-note exclusions
	for i, note := range discussion.Notes.Nodes {
		if note.System && !mrNotesCfg.includeSystemNotes {
			continue
		}
		if mrNotesCfg.ignore[note.Author.Username] {
			continue
		}
		if mrNotesCfg.ignoreResolved && note.Resolved {
			continue
		}
		n, err := w.WriteString(printNote(note, i > 0, mrNotesCfg.printNoteCfg))
		if err != nil {
			log.Fatal(err)
		}
		written += n
	}
	if written > 0 {
		fmt.Println(strings.Repeat("=", 80))
		fmt.Printf("Discussion: %s\n", discussion.ID.ID)
		fmt.Println(w.String())
	}
}

// TODO: fix printing notes when the line-width is too small
func printNote(note notes.Note, isReply bool, cfg printNoteCfg) string {
	lab.CmdLogger().Debugf("printNote id=%d Author=%s isReply=%v", note.ID.ID, note.Author.Username, isReply)
	var b strings.Builder
	resolved := ""
	if note.Resolved {
		resolved = "RESOLVED"
	}
	position := ""
	if note.Position != nil && !isReply {
		if note.Position.NewLine == nil { // deletion
			position = fmt.Sprintf("\n%s [DELETED]", note.Position.NewPath)
		} else {
			line := note.Position.NewLine
			context := getContext(note.Position.DiffRefs.HeadSHA, note.Position.NewPath, *line, cfg)
			position = fmt.Sprintf("\n%s:%d %s", note.Position.NewPath, *line, context)
		}
	}
	indent := " "
	if isReply {
		indent = strings.Repeat(" ", 4) + "| "
	}

	if cfg.showURL {
		webURL := note.WebURL
		fmt.Fprintf(&b, "%s%s\n", indent, webURL)
	}

	fmt.Fprintf(&b, "%s@%*s %s %s%s\n%s%s",
		indent, -40, note.Author.Username, note.CreatedAt, resolved, position,
		indent, strings.Repeat("-", 80))

	formatNoteBody(&b, indent, note.Body, cfg)
	fmt.Fprintf(&b, "\n\n")

	return b.String()
}

func formatNoteBody(b io.Writer, indent string, body string, cfg printNoteCfg) {
	if cfg.nowrap {
		fmt.Fprint(b, body)
	} else {
		justifyNoteBody(b, indent, body, cfg)
	}
}

func justifyNoteBody(b io.Writer, indent string, body string, cfg printNoteCfg) {
	bodyWidth := cfg.lineLen - len(indent)
	lab.CmdLogger().Debugf("printNote.justify %d", len(body))
	for i, line := range strings.Split(body, "\n") {
		lab.CmdLogger().Debugf("printNote.justify.line %d", i)
		if len(line) == 0 {
			fmt.Fprintf(b, "\n%s", indent)
			continue
		}

		pos := 0
		rs := []rune(line)
		for {
			lab.CmdLogger().Debugf("printNote.justify.line.loop %d", pos)
			lastPos := pos
			for pos > 0 && pos < len(rs) && unicode.IsSpace(rs[pos]) {
				pos++
			}
			if pos >= len(rs) {
				break
			}

			endPos := pos + bodyWidth
			if endPos >= len(rs) {
				endPos = len(rs) - 1
			}
			for pos <= endPos && endPos+1 < len(rs) && !unicode.IsSpace(rs[endPos+1]) {
				endPos--
			}

			fmt.Fprintf(b, "\n%s%s", indent, string(rs[pos:endPos+1]))
			pos = endPos + 1
			if pos == lastPos {
				fmt.Fprintf(b, "\n%s%s", indent, string(rs[endPos+1:len(rs)-1]))
				break
			}
		}
	}
}

func getContext(sha string, path string, line int, cfg printNoteCfg) string {
	var context strings.Builder

	lab.CmdLogger().Debugf("getContext sha=%s, path=%s:%d", sha, path, line)
	if cfg.contextWindow < 1 {
		return ""
	}
	if line > 2000 {
		return "[line offset too large]"
	}
	halfWindow := cfg.contextWindow / 2

	// TODO: detect if the SHA is not available
	lab.CmdLogger().Debugf("git show %s:%s", sha, path)
	proc := exec.Command("git", "show", fmt.Sprintf("%s:%s", sha, path))
	output, err := proc.StdoutPipe()
	if err != nil {
		lab.CmdLogger().Warningf("Could not read context sha=%s, path=%s:%d", sha, path, line)
		return ""
	}
	if err := proc.Start(); err != nil {
		lab.CmdLogger().Warningf("Could not read context sha=%s, path=%s:%d", sha, path, line)
		return ""
	}
	lab.CmdLogger().Debugf("Reading output")

	scanner := bufio.NewScanner(output)
	currentLine := 1
	fmtWidth := 1
	if line+halfWindow >= 10 {
		fmtWidth = 2
	} else if line+halfWindow >= 100 {
		fmtWidth = 3
	} else if line+halfWindow >= 1000 {
		fmtWidth = 4
	} else if line+halfWindow >= 10000 {
		fmtWidth = 5
	}

	for scanner.Scan() {
		offset := currentLine - line
		if -halfWindow <= offset && offset <= halfWindow {
			lab.CmdLogger().Debugf("Reading line %d", currentLine)

			context.WriteString("\n")
			context.WriteString(fmt.Sprintf("%-*d", fmtWidth, currentLine))
			if currentLine == line {
				context.WriteString(" > ")
			} else {
				context.WriteString(" | ")
			}
			context.WriteString(scanner.Text())
		} else {
			lab.CmdLogger().Debugf("Skipped line %d", currentLine)
		}
		if offset >= halfWindow {
			break
		}
		currentLine++
	}
	/* drain the buffer */
	for scanner.Scan() {
		// nothing
	}
	if err := scanner.Err(); err != nil {
		lab.CmdLogger().Warningf("Could not read context sha=%s, path=%s:%d, %v", sha, path, line, err)
	} else {
		lab.CmdLogger().Debugf("Finished reading output after %d lines", currentLine)
	}

	if err := proc.Wait(); err != nil {
		lab.CmdLogger().Warningf("Could not read context sha=%s, path=%s:%d", sha, path, line)
	}
	return context.String()
}

func init() {
	mrNotesCfg.lineLen = 100
	mrNotesCfg.only = make(map[string]bool)
	mrNotesCfg.ignore = make(map[string]bool)

	mrNotesCmd.MarkZshCompPositionalArgumentCustom(1, "__lab_completion_remote")
	mrNotesCmd.MarkZshCompPositionalArgumentCustom(2, "__lab_completion_merge_request $words[2]")
	mrNotesCmd.Flags().BoolVarP(&mrNotesCfg.includeSystemNotes, "system-notes", "s", false, "Include system notes, in addition to user comments")
	mrNotesCmd.Flags().BoolVarP(&mrNotesCfg.threaded, "threaded", "t", true, "Show notes in their discussion threads, with replies below the head comment")
	mrNotesCmd.Flags().BoolVarP(&mrNotesCfg.showURL, "url", "u", false, "Show the web-url of the the note")
	mrNotesCmd.Flags().BoolVarP(&mrNotesCfg.ignoreResolved, "ignore-resolved", "", false, "Do not show resolved discussions or notes")
	mrNotesCmd.Flags().BoolVarP(&mrNotesCfg.nowrap, "no-wrap", "", false, "Do not try to re-wrap lines")
	mrNotesCmd.Flags().IntVarP(&mrNotesCfg.contextWindow, "context-window", "c", 5, "How large the context window should be (0 == no context, 1 == just the line)")
	width, _, err := terminal.GetSize(0)
	if err == nil && mrNotesCfg.lineLen >= width {
		mrNotesCfg.lineLen = width - 5 // some gutter space on the RHS
	}
	mrNotesCmd.Flags().IntVarP(&mrNotesCfg.lineLen, "line-length", "l", mrNotesCfg.lineLen, "How wide should comments be? Lines longer than this value will be wrapped")
	mrNotesCmd.Flags().StringSliceVar(&mrNotesCfg.ignoreUsers, "ignore", mrNotesCfg.ignoreUsers, "Set of users to ignore")
	mrNotesCmd.Flags().StringSliceVar(&mrNotesCfg.onlyUsers, "author", mrNotesCfg.onlyUsers, "Only show notes by these authors")
	mrNotesCmd.Flags().BoolVarP(&mrNotesCfg.interactive, "interactive", "i", false, "Present the list interactively")
	mrCmd.AddCommand(mrNotesCmd)
}
