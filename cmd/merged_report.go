package cmd

import (
	"context"
	"fmt"
	"log"
	"math"
	"sort"
	"strings"
	"time"

	"github.com/jinzhu/now"
	graphql "github.com/machinebox/graphql"
	"github.com/spf13/cobra"
	gitlab "github.com/xanzy/go-gitlab"
	cal "github.com/zaquestion/lab/internal/calendar"
	lab "github.com/zaquestion/lab/internal/gitlab"
	"gitlab.com/alexkalderimis/trapezi"
)

var mergedReportCfg struct {
	mergedAfter  string
	mergedBefore string
	timezone     string
	markdown     bool
	labels       []string
	projectPath  string
	ignoreNS     string
	requireNS    string
	username     string
}

const (
	ymd_layout = "2006-01-02"
)

var mergedReportCmd = &cobra.Command{
	Use:   "report [remote] [id]",
	Short: "Get a report for merge-requests merged in a given time period",
	Run: func(cmd *cobra.Command, args []string) {
		if mergedReportCfg.timezone != "" {
			loc, err := time.LoadLocation(mergedReportCfg.timezone)
			if err != nil {
				log.Fatal(err)
			}
			time.Local = loc
		}
		var err error
		var before, after time.Time
		layout := ymd_layout
		if mergedReportCfg.mergedAfter == "" {
			after = now.BeginningOfMonth()
		} else {
			after, err = time.Parse(layout, mergedReportCfg.mergedAfter)
			if err != nil {
				log.Fatalf("Could not parse after: '%s'", mergedReportCfg.mergedAfter)
			}
		}
		if mergedReportCfg.mergedBefore == "" {
			before = time.Now()
		} else {
			before, err = time.Parse(layout, mergedReportCfg.mergedBefore)
			if err != nil {
				log.Fatalf("Could not parse before: '%s'", mergedReportCfg.mergedBefore)
			}
		}

		mrs, err := getMergedMrs(after, before)
		if err != nil {
			log.Fatal(err)
		}
		count := 0
		totalDays := 0
		totalWorkDays := 0
		totalBusDays := 0
		totalChanges := 0
		totalAdded := 0
		totalDeleted := 0

		sort.Slice(mrs, func(i, j int) bool {
			return mrs[i].MergedAt == nil || (mrs[j].MergedAt != nil && mrs[i].MergedAt.Before(*mrs[j].MergedAt))
		})
		var workDaysList []int

		var table []trapezi.CompoundCell

		if mergedReportCfg.markdown {
			fmt.Printf("|Merge Request|Work Days|Changes|Merged At|Labels|\n")
			fmt.Printf("|-------------|---------|-------|---------|------|\n")
		}

		wantedLabels := make(map[string]bool)
		for _, wanted := range mergedReportCfg.labels {
			wantedLabels[strings.ToLower(wanted)] = true
		}
		anyWanted := len(mergedReportCfg.labels) > 0

		for _, mr := range mrs {
			if mergedReportCfg.ignoreNS != "" && strings.HasPrefix(mr.Reference, mergedReportCfg.ignoreNS) {
				continue
			}
			if mergedReportCfg.requireNS != "" && !strings.HasPrefix(mr.Reference, mergedReportCfg.requireNS) {
				continue
			}
			lab.CmdLogger().Infof("MR: %s", mr.Reference)
			calDays := int(math.Floor(mr.MergedAt.Sub(*mr.CreatedAt).Hours() / 24.0))
			busDays := cal.BusinessDaysInRange(mr.CreatedAt, mr.MergedAt)
			workDays := busDays - cal.LeaveDaysInRange(mr.CreatedAt, mr.MergedAt)

			count++
			totalDays += calDays
			totalBusDays += busDays
			totalWorkDays += workDays
			workDaysList = append(workDaysList, workDays)
			totalChanges += mr.DiffStatsSummary.Changed
			totalAdded += mr.DiffStatsSummary.Added
			totalDeleted += mr.DiffStatsSummary.Deleted

			if mergedReportCfg.markdown {
				var labels strings.Builder
				if anyWanted {
					for _, label := range mr.Labels.Nodes {
						var prefix string
						parts := strings.Split(label.Title, ":")
						if len(parts) > 0 {
							prefix = parts[0]
						}
						if !(wantedLabels[strings.ToLower(label.Title)] || wantedLabels[strings.ToLower(prefix)]) {
							continue
						}

						if labels.Len() > 0 {
							labels.WriteString(", ")
						}
						labels.WriteString("~")
						escape := strings.Contains(label.Title, " ")
						if escape {
							labels.WriteString("\"")
						}
						labels.WriteString(label.Title)
						if escape {
							labels.WriteString("\"")
						}
					}
				}
				var workDaysEmoji string
				if workDays > 14 {
					workDaysEmoji = ":red_circle:"
				} else if workDays > 7 {
					workDaysEmoji = ":warning:"
				} else {
					workDaysEmoji = ":large_blue_circle:"
				}

				fmt.Printf("|[%s:%s](%s)|%s %d|%d|%s|%s|\n", mr.Reference, mr.Title, mr.WebURL, workDaysEmoji, workDays, mr.DiffStatsSummary.Changed, mr.MergedAt.Format(ymd_layout), labels.String())
			} else {

				lineCount := statusCell(changesStatus(mr.DiffStatsSummary.Changed), fmt.Sprintf("%d", mr.DiffStatsSummary.Changed))
				lineCount.Align = trapezi.ALIGN_RIGHT

				table = append(table, trapezi.Join(" ",
					trapezi.Text(mr.Reference+":"),
					trapezi.Text(mr.MergedAt.Format("2006-01-02")),
					trapezi.Join(" ", lineCount, trapezi.Text("lines")),
					statusCell(timeStatus(workDays), fmt.Sprintf("(%d work days)", workDays)),
					trapezi.Text(mr.Title),
				))
			}
		}
		trapezi.AlignTable(table)
		printTable(table)
		mttm := float32(totalDays) / float32(count)
		mbdtm := float32(totalBusDays) / float32(count)
		mwdtm := float32(totalWorkDays) / float32(count)
		medwd := median(workDaysList)

		fmt.Printf("\nTotal merged: %2d, Total changes: %4d (+%d,-%d), MTTM: %2.1f days, MBDTM: %2.1f days, MWDTM: %2.1f, Median WD: %2d\n", count, totalChanges, totalAdded, totalDeleted, mttm, mbdtm, mwdtm, medwd)
	},
}

func changesStatus(changes int) gitlab.BuildStateValue {
	if changes > 1000 {
		return gitlab.Failed
	} else if changes > 300 {
		return gitlab.Running
	} else {
		return gitlab.Success
	}
}

func timeStatus(workDays int) gitlab.BuildStateValue {
	if workDays > 14 {
		return gitlab.Failed
	} else if workDays > 7 {
		return gitlab.Running
	} else {
		return gitlab.Success
	}
}

func median(input []int) int {
	sort.Slice(input, func(i, j int) bool {
		return input[i] < input[j]
	})

	// No math is needed if there are no numbers
	// For even numbers we add the two middle numbers
	// and divide by two using the mean function above
	// For odd numbers we just use the middle number
	l := len(input)
	m := l / 2
	if l == 0 {
		return 0
	} else if l%2 == 0 {
		return (input[m-1] + input[m]) / 2
	} else {
		return input[m]
	}
}

// MergedMR - The type of a merged MR
type MergedMR struct {
	Title            string
	Reference        string
	MergedAt         *time.Time
	CreatedAt        *time.Time
	WebURL           string
	DiffStatsSummary struct {
		Changed int
		Added   int
		Deleted int
	}
	TargetProject struct {
		FullPath string
	}
	Labels struct {
		Nodes []*struct {
			Title string
		}
	}
}

func getMergedMrs(after, before time.Time) ([]MergedMR, error) {
	client := lab.GQLClient()
	var signature, userQuery string

	if mergedReportCfg.username != "" {
		signature = "($afterT: Time, $beforeT: Time, $after: String!, $projectPath: String, $user: String!)"
		userQuery = "user(username: $user)"
	} else {
		signature = "($afterT: Time, $beforeT: Time, $after: String!, $projectPath: String)"
		userQuery = "currentUser"
	}

	query := fmt.Sprintf(`
	query %s {
		user: %s {
			authoredMergeRequests(mergedAfter: $afterT, mergedBefore: $beforeT, projectPath: $projectPath, state: merged, after: $after) {
			  pageInfo {
				  hasNextPage
					endCursor
				}
			  nodes {
				  title
					reference(full: true)
					createdAt
					mergedAt
					webUrl
					diffStatsSummary {
						changed: changes
						added: additions
						deleted: deletions
					}
					targetProject {
					  fullPath
					}
					labels {
					  nodes {
						  title
						}
					}
				}
			}
		}
	}
	`, signature, userQuery)

	lab.CmdLogger().Infof("Query: %s", query)
	type resp struct {
		User struct {
			AuthoredMergeRequests struct {
				PageInfo struct {
					HasNextPage bool
					EndCursor   string
				}
				Nodes []MergedMR
			}
		}
	}
	var retval []MergedMR
	var cursor = ""
	var err error

	for {
		req := graphql.NewRequest(query)
		req.Var("after", cursor)
		req.Var("afterT", after)
		req.Var("beforeT", before)
		lab.CmdLogger().Infof("Requesting report for date range: %s-%s", after, before)
		if mergedReportCfg.projectPath != "" {
			req.Var("projectPath", mergedReportCfg.projectPath)
		}
		if mergedReportCfg.username != "" {
			lab.CmdLogger().Infof("Requesting report for: '%s'", mergedReportCfg.username)
			req.Var("user", mergedReportCfg.username)
		}

		ctx := context.Background()
		var data resp
		lab.CmdLogger().Infof("Requesting after: %s", cursor)
		err = client.Run(ctx, req, &data)
		found := data.User.AuthoredMergeRequests.Nodes
		lab.CmdLogger().Infof("Found %d merge requests", len(found))
		retval = append(retval, data.User.AuthoredMergeRequests.Nodes...)
		if err != nil || !data.User.AuthoredMergeRequests.PageInfo.HasNextPage {
			break
		}
		cursor = data.User.AuthoredMergeRequests.PageInfo.EndCursor
	}

	return retval, err
}

func init() {
	mrCmd.AddCommand(mergedReportCmd)
	mergedReportCmd.MarkZshCompPositionalArgumentCustom(1, "__lab_completion_remote")
	mergedReportCmd.Flags().StringVarP(&mergedReportCfg.mergedAfter, "after", "a", "", "Include all merge requests merged after this date")
	mergedReportCmd.Flags().StringVarP(&mergedReportCfg.mergedBefore, "before", "b", "", "Include all merge requests merged before this date")
	mergedReportCmd.Flags().StringVar(&mergedReportCfg.timezone, "timezone", "UTC", "Timezone e.g. `Europe/London` formatted time-zone")
	mergedReportCmd.Flags().BoolVarP(&mergedReportCfg.markdown, "markdown", "m", false, "Format as Markdown")
	mergedReportCmd.Flags().StringVarP(&mergedReportCfg.projectPath, "project", "p", "", "Project all MRs must be merged to")
	mergedReportCmd.Flags().StringVarP(&mergedReportCfg.ignoreNS, "ignore", "i", "", "Ignore references starting with this string")
	mergedReportCmd.Flags().StringVarP(&mergedReportCfg.requireNS, "require", "r", "", "Require references to start with this string")
	mergedReportCmd.Flags().StringVarP(&mergedReportCfg.username, "user", "u", "", "Load a report for this user (default: the current user)")
	mergedReportCmd.Flags().StringArrayVar(&mergedReportCfg.labels, "label", mergedReportCfg.labels, "Labels to include in report")
}
