package cmd

import (
	"context"
	"fmt"
	"log"
	"os"
	"sort"
	"strings"
	"time"

	humanize "github.com/dustin/go-humanize"
	color "github.com/fatih/color"
	graphql "github.com/machinebox/graphql"
	"github.com/spf13/cobra"
	gitlab "github.com/xanzy/go-gitlab"
	lab "github.com/zaquestion/lab/internal/gitlab"
	interactive "github.com/zaquestion/lab/internal/interactive"
	types "github.com/zaquestion/lab/internal/types"
	"gitlab.com/alexkalderimis/trapezi"
)

var mrListConfig struct {
	cmdArguments           []string
	mrLabels               []string
	mrShowLabels           []string
	mrState                string
	mrTargetBranch         string
	mrNumRet               int
	mrAll                  bool
	mrMine                 bool
	mrAssignee             string
	mrReviewer             string
	mrAuthor               string
	ciStatus               bool
	ciStatusSymbol         bool
	ciTime                 bool
	pipelineStatus         bool
	mergeStatus            bool
	showAssignees          bool
	showReviewers          bool
	showAuthor             bool
	mrSourceBranch         bool
	showTarget             bool
	hideTitle              bool
	shortTitle             bool
	hideRef                bool
	maxRefLength           int
	maxBranchLen           int
	currentProjectOnly     bool
	targetProject          string
	showDiffStatSummary    bool
	indicateInputNeeded    bool
	urgentOnly             bool
	showWip                bool
	extendedLayout         bool
	markdownLayout         bool
	alignSourceBranchRight bool
	showAge                bool
	showMilestone          bool
	showURL                bool
	interactive            bool
	fetchMROps             *gitlab.GetMergeRequestsOptions
	currentMrIID           string
}

var currentUserName *string
var titlePrinter trapezi.CellPrinter

// listCmd represents the list command
var listCmd = &cobra.Command{
	Use:     "list [remote]",
	Aliases: []string{"ls"},
	Short:   "List merge requests",
	Long:    ``,
	Args:    cobra.MaximumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		var err error
		_, idents, err := parseProjectMR([]string{}, StateOpen)
		if err == nil {
			mrListConfig.currentMrIID = idents.IID
		}

		mrListConfig.alignSourceBranchRight = mrListConfig.showTarget
		titlePrinter = &trapezi.FmtPrinter
		if mrListConfig.ciStatusSymbol {
			mrListConfig.ciStatus = true
		}
		if mrListConfig.pipelineStatus {
			mrListConfig.ciStatus = true
			mrListConfig.ciTime = true
		}
		if mrListConfig.ciTime {
			mrListConfig.ciStatus = true
		}
		if mrListConfig.shortTitle {
			mrListConfig.hideTitle = false
		}
		if mrListConfig.markdownLayout {
			mrListConfig.hideTitle = false
			mrListConfig.ciStatus = true
		}
		if mrListConfig.extendedLayout {
			mrListConfig.hideTitle = false
			mrListConfig.shortTitle = false
			mrListConfig.alignSourceBranchRight = false
			mrListConfig.showAssignees = true
			mrListConfig.showReviewers = true
			mrListConfig.showMilestone = true
			bold := color.New(color.Bold)
			boldPrinter := *bold
			titlePrinter = &boldPrinter
		}
		if mrListConfig.mrMine {
			mrListConfig.mrAssignee = "@"
		}
		if mrListConfig.mrReviewer != "" {
			mrListConfig.mrAssignee = mrListConfig.mrReviewer
		}
		mrListConfig.cmdArguments = args

		if mrListConfig.interactive {
			interactive.InteractWithMRList(getMergeRequestList)
			return
		}

		mergeRequests, err := getMergeRequestList()
		if err != nil {
			log.Fatal(err)
		}

		m := make(map[string]([]types.MRListEntry))
		for _, mr := range mergeRequests {
			trackMRByBranchName(mr.SourceBranch, mr.ShortRef)
			if mrListConfig.urgentOnly && !needsAttention(mr) {
				continue
			}
			if authoredBy(mr, mrListConfig.mrReviewer) {
				continue
			}
			m[mr.TargetBranch] = append(m[mr.TargetBranch], mr)
		}

		if mrListConfig.extendedLayout {
			PrintExtended(m)
		} else if mrListConfig.markdownLayout {
			PrintBullets(m)
		} else {
			PrintTable(m)
		}
	},
}

func getMergeRequestList() ([]types.MRListEntry, error) {
	var mergeRequests []types.MRListEntry
	var err error

	filters := MRListFilters{
		State:        mrListConfig.mrState,
		TargetBranch: mrListConfig.mrTargetBranch,
		Labels:       mrListConfig.mrLabels,
	}
	if mrListConfig.targetProject != "" {
		filters.ProjectPath = mrListConfig.targetProject
	} else if (mrListConfig.mrAuthor != "" || mrListConfig.mrAssignee != "") && mrListConfig.currentProjectOnly {
		projectPath, _, err := parseArgs(mrListConfig.cmdArguments)
		if err != nil {
			log.Fatal(err)
		}
		filters.ProjectPath = projectPath
	}

	if mrListConfig.mrAuthor != "" {
		return listMRsByUser(mrListConfig.mrAuthor, "authored", filters)
	} else if mrListConfig.mrAssignee != "" {
		mergeRequests, err = listMRsByUser(mrListConfig.mrAssignee, "assigned", filters)
		if err != nil {
			return mergeRequests, err
		}
		lab.CmdLogger().Debugf("Assigned: %d", len(mergeRequests))
		if !mrListConfig.mrMine || mrListConfig.mrReviewer != "" {
			reviewed, err := listMRsByUser(mrListConfig.mrReviewer, "reviewRequested", filters)
			if err == nil {
				lab.CmdLogger().Debugf("Reviewed: %d", len(reviewed))

				seen := make(map[string]bool)
				for _, mr := range mergeRequests {
					seen[mr.Reference] = true
				}

				for _, mr := range reviewed {
					if !seen[mr.Reference] {
						mergeRequests = append(mergeRequests, mr)
					}
				}
			}
		}
		return mergeRequests, err
	} else {
		projectPath, _, err := parseArgs(mrListConfig.cmdArguments)
		if err != nil {
			return []types.MRListEntry{}, err
		}
		return listMRsByProject(projectPath, filters)
	}
}

func PrintBullets(m map[string][]types.MRListEntry) {
	targets := make([]string, 0, len(m))
	targetted := make(map[string]int)
	for k := range m {
		targetted[k] = len(m[k])
		targets = append(targets, k)
	}

	sort.Slice(targets, func(i, j int) bool {
		ti := targets[i]
		tj := targets[j]
		tni := targetted[ti]
		tnj := targetted[tj]

		if tni > tnj {
			return true
		} else if tni < tnj {
			return false
		} else if ti == "main" || ti == "master" {
			return true
		}

		return ti < tj
	})

	for _, target := range targets {
		mrs := m[target]
		handleBullet(mrs, m, 0)
	}
}

func PrintTable(m map[string][]types.MRListEntry) {
	targets := make([]string, 0, len(m))
	targetted := make(map[string]int)
	for k := range m {
		targetted[k] = len(m[k])
		targets = append(targets, k)
	}

	sort.Slice(targets, func(i, j int) bool {
		ti := targets[i]
		tj := targets[j]
		tni := targetted[ti]
		tnj := targetted[tj]

		if tni > tnj {
			return true
		} else if tni < tnj {
			return false
		} else if ti == "main" || ti == "master" {
			return true
		}

		return ti < tj
	})

	var table []trapezi.CompoundCell
	for _, target := range targets {
		mrs := m[target]
		handleList(&table, mrs, m, false)
	}
	trapezi.AlignTable(table)
	printTable(table)
}

func PrintExtended(m map[string][]types.MRListEntry) {
	for target, mrs := range m {
		fmt.Printf("# MRS into %s\n========\n", target)
		for _, mr := range mrs {
			printExtendedLayout(mr)
		}
	}
}

func printExtendedLayout(mr types.MRListEntry) {
	headLine := buildTitleCell(mr)
	if mrListConfig.indicateInputNeeded {
		trapezi.Join(" ", buildIndicatorCell(mr), headLine).Render(os.Stdout)
	} else {
		headLine.Render(os.Stdout)
	}
	fmt.Println()
	var statusRow []trapezi.Cell
	if mrListConfig.ciStatus {
		statusRow = append(statusRow, buildCiStatusCell(mr))
	}
	if mrListConfig.showDiffStatSummary {
		statusRow = append(statusRow, buildDiffStatCell(mr))
	}
	if len(statusRow) != 0 {
		trapezi.Join(" ", statusRow...).Render(os.Stdout)
	}
	fmt.Println()

	var kvs []trapezi.CompoundCell
	if mr.UpdatedAt != nil {
		kvs = printKV(kvs, "Last updated", trapezi.Text(humanize.RelTime(*mr.UpdatedAt, time.Now(), "ago", "")))
	}
	if mrListConfig.mrSourceBranch {
		kvs = printKV(kvs, "Source", buildSourceBranchCell(false, mr))
	}
	if mrListConfig.showAuthor {
		kvs = printKV(kvs, "Author", trapezi.Text(mr.Author.Username))
	}
	if mrListConfig.showAssignees && len(mr.Assignees.Nodes) > 0 {
		kvs = printKV(kvs, "Assignees", buildAssigneesCell(mr))
	}
	if mrListConfig.showReviewers && len(mr.Reviewers.Nodes) > 0 {
		kvs = printKV(kvs, "Reviewers", buildReviewersCell(mr))
	}
	if len(mrListConfig.mrShowLabels) > 0 {
		kvs = printKV(kvs, "Labels", buildLabelCell(mr))
	}
	if mrListConfig.showAge {
		kvs = printKV(kvs, "Age", buildAgeCell(mr))
	}
	kvs = printKV(kvs, "Milestone", buildMilestoneCell(mr))

	kvs = printKV(kvs, "URL", trapezi.Text(mr.WebUrl))

	trapezi.AlignTable(kvs)
	printTable(kvs)
	fmt.Println()
}

func handleBullet(mrs []types.MRListEntry, mrsByTarget map[string][]types.MRListEntry, indent int) {
	for _, mr := range mrs {
		lab.CmdLogger().Infof("Handling %s", mr.Reference)
		var line strings.Builder
		for i := 0; i < indent; i++ {
			line.WriteString("  ")
		}
		line.WriteString("- ")
		line.WriteString(fmt.Sprintf("[%s](%s)", mr.Reference, mr.WebUrl))
		line.WriteString(" ")
		pipeline := mr.HeadPipeline
		var status gitlab.BuildStateValue
		if pipeline != nil {
			status = gitlab.BuildStateValue(strings.ToLower(pipeline.Status))
		} else {
			status = lab.None
		}

		var statusEmoji string

		switch status {
		case gitlab.Failed:
			statusEmoji = "🔴"
		case lab.Success:
			statusEmoji = "🟢"
		case gitlab.Success:
			statusEmoji = "🟢"
		case gitlab.Running:
			statusEmoji = "🔵"
		case gitlab.Created:
			statusEmoji = "⚪"
		case gitlab.Pending:
			statusEmoji = "⚪"
		case gitlab.Skipped:
			statusEmoji = "⚫"
		default:
			statusEmoji = "?"
		}

		line.WriteString(statusEmoji)
		for _, label := range mr.Labels.Nodes {
			if strings.HasPrefix(label.Title, "workflow::") {
				line.WriteString(fmt.Sprintf(" (%s)", label.Title))
			}
		}
		fmt.Println(line.String())

		var titleLine strings.Builder
		for i := 0; i <= indent; i++ {
			titleLine.WriteString("  ")
		}
		titleLine.WriteString(mr.Title)
		fmt.Println(titleLine.String())

		if submrs, present := mrsByTarget[mr.SourceBranch]; present {
			delete(mrsByTarget, mr.SourceBranch)
			sort.Slice(submrs, func(i, j int) bool {
				return submrs[i].SourceBranch > submrs[j].SourceBranch
			})
			handleBullet(submrs, mrsByTarget, indent+1)
		}
	}
}

func handleList(rows *[]trapezi.CompoundCell, mrs []types.MRListEntry, mrsByTarget map[string][]types.MRListEntry, isSubList bool) {
	for _, mr := range mrs {
		lab.CmdLogger().Infof("Handling %s", mr.Reference)
		var currentRow []trapezi.Cell
		if !noTitleCell() {
			currentRow = append(currentRow, buildTitleCell(mr))
		}
		if mrListConfig.mrSourceBranch || mrListConfig.showTarget {
			var divider string
			if isSubList {
				divider = "    "
			} else {
				divider = " => "
			}
			branchCells := trapezi.Join(divider)
			if mrListConfig.mrSourceBranch {
				branchCells.Cells = append(branchCells.Cells,
					buildSourceBranchCell(isSubList, mr))
			}
			if mrListConfig.showTarget {
				branchCells.Cells = append(branchCells.Cells,
					buildTargetBranchCell(isSubList, mr))
			}
			currentRow = append(currentRow, branchCells)
		}
		if mrListConfig.showDiffStatSummary {
			currentRow = append(currentRow, buildDiffStatCell(mr))
		}
		if mrListConfig.ciStatus {
			currentRow = append(currentRow, buildCiStatusCell(mr))
		}
		if mrListConfig.showAge {
			currentRow = append(currentRow, buildAgeCell(mr))
		}
		if mrListConfig.showMilestone {
			currentRow = append(currentRow, buildMilestoneCell(mr))
		}
		if len(mrListConfig.mrShowLabels) > 0 {
			currentRow = append(currentRow, buildLabelCell(mr))
		}
		if mrListConfig.showAuthor {
			currentRow = append(currentRow, trapezi.Text(mr.Author.Username))
		}
		if mrListConfig.showAssignees {
			currentRow = append(currentRow, buildAssigneesCell(mr))
		}
		if mrListConfig.showReviewers {
			currentRow = append(currentRow, buildReviewersCell(mr))
		}
		if mrListConfig.indicateInputNeeded {
			currentRow = append(currentRow, buildIndicatorCell(mr))
		}
		if mrListConfig.showURL {
			currentRow = append(currentRow, buildURLCell(mr))
		}
		lab.CmdLogger().Infof("Row length: %d", len(currentRow))

		row := trapezi.CompoundCell{
			Cells:   currentRow,
			Divider: " | ",
		}
		if mrListConfig.currentMrIID != "" {
			var indicator string
			if mr.IID == mrListConfig.currentMrIID {
				indicator = "> "
			} else {
				indicator = "| "
			}
			row.Prefix = indicator
		}

		*rows = append(*rows, row)

		lab.CmdLogger().Infof("Table size: %d", len(*rows))

		if submrs, present := mrsByTarget[mr.SourceBranch]; present {
			delete(mrsByTarget, mr.SourceBranch)
			sort.Slice(submrs, func(i, j int) bool {
				return submrs[i].SourceBranch > submrs[j].SourceBranch
			})
			handleList(rows, submrs, mrsByTarget, true)
		}
	}
}

func buildSourceBranchCell(isSubList bool, basicMR types.MRListEntry) trapezi.Cell {
	var content string
	branchName := basicMR.SourceBranch
	maxLen := mrListConfig.maxBranchLen
	if isSubList {
		maxLen--
	}

	// Require showing at least 15 characters of the branchName
	if maxLen > 15 && len(branchName) > maxLen {
		endLeft := maxLen - 8
		startRight := len(branchName) - 5

		// show less for sub-entries
		if isSubList {
			endLeft = 9
			startRight = len(branchName) - 3
		}

		branchName = branchName[:endLeft] + "..." + branchName[startRight:]
	}

	alignRight := mrListConfig.alignSourceBranchRight
	if isSubList {
		if alignRight {
			content = fmt.Sprintf("%s╯", branchName)
		} else {
			content = fmt.Sprintf("╰%s", branchName)
		}
	} else {
		content = branchName
	}
	cell := trapezi.Text(content)

	if alignRight {
		cell.Align = trapezi.ALIGN_RIGHT
	}
	return cell
}

func buildTargetBranchCell(isSubList bool, entry types.MRListEntry) trapezi.Cell {
	if isSubList {
		return trapezi.Text("")
	}
	return trapezi.Text(entry.TargetBranch)
}

func buildDiffStatCell(entry types.MRListEntry) trapezi.Cell {
	stats := entry.DiffStatsSummary

	additions := trapezi.Text(fmt.Sprintf("+%d", stats.Additions))
	additions.Align = trapezi.ALIGN_RIGHT
	additions.Printer = lab.BuildStateColor(gitlab.Success)

	deletions := trapezi.Text(fmt.Sprintf("-%d", stats.Deletions))
	deletions.Align = trapezi.ALIGN_LEFT
	deletions.Printer = lab.BuildStateColor(gitlab.Failed)

	files := trapezi.Text(fmt.Sprintf("(%d files)", stats.FileCount))
	files.Align = trapezi.ALIGN_RIGHT

	plusMinus := trapezi.Join("/", additions, deletions)
	container := trapezi.Join(" ", plusMinus, files)

	return container
}

func buildAgeCell(entry types.MRListEntry) trapezi.Cell {
	mrAge := entry.Age()
	text := fmt.Sprintf("%d", mrAge)
	var status gitlab.BuildStateValue

	if mrAge > 14 {
		status = gitlab.Failed
	} else if mrAge > 10 {
		status = gitlab.Running
	} else if mrAge > 7 {
		status = gitlab.Pending
	} else {
		status = gitlab.Success
	}

	return statusCell(status, text)
}

func buildMilestoneCell(entry types.MRListEntry) trapezi.Cell {
	milestone := entry.Milestone.Title

	return trapezi.Text(milestone)
}

func buildCiStatusCell(entry types.MRListEntry) trapezi.Cell {
	container := trapezi.Join(" ")
	pipeline := entry.HeadPipeline
	status := pipeline.BuildState()

	message := pipeline.StatusMessage(mrListConfig.ciStatusSymbol)
	container.Cells = append(container.Cells, statusCell(status, message))

	if entry.SourceBranch != "" {
		trackMRBuildStatusByBranchName(entry.SourceBranch, status)
	}

	if mrListConfig.pipelineStatus {
		var pipelineCell trapezi.Cell
		if pipeline != nil {
			pipelineCell = statusCell(status, fmt.Sprintf("(%3d/%3d)", pipeline.Success.Count, pipeline.Total.Count))
		} else {
			pipelineCell = trapezi.Text("[no pipeline yet]")
		}
		container.Cells = append(container.Cells, pipelineCell)
	}

	complete := "⌛"
	incomplete := "⏳"

	if mrListConfig.ciTime {
		if pipeline != nil && pipeline.FinishedAt != nil {
			container.Cells = append(container.Cells, trapezi.Text(complete+humanize.Time(*pipeline.FinishedAt)))
		} else if pipeline != nil && pipeline.CreatedAt != nil {
			// Duration is only provided once the pipeline is complete
			container.Cells = append(container.Cells, trapezi.Text(incomplete+humanize.RelTime(*pipeline.CreatedAt, time.Now(), "", "")))
		} else {
			container.Cells = append(container.Cells, trapezi.Text(""))
		}
	}

	return container
}

func needsAttention(mr types.MRListEntry) bool {
	for _, user := range mr.Assignees.Nodes {
		fetchCurrentUsername()
		if user.Username == *currentUserName && user.Username != mr.Author.Username {
			return true
		}
	}
	return false
}

func buildIndicatorCell(mr types.MRListEntry) trapezi.Cell {
	var sb strings.Builder

	if needsAttention(mr) {
		sb.WriteString("⚠️")
	}

	return trapezi.Text(sb.String())
}

func buildURLCell(mr types.MRListEntry) trapezi.Cell {
	return trapezi.Text(mr.WebUrl)
}

func buildLabelCell(mr types.MRListEntry) trapezi.Cell {
	var sb strings.Builder
	for _, label := range mr.Labels.Nodes {
		for _, desired := range mrListConfig.mrShowLabels {
			if strings.HasPrefix(label.Title, desired) {
				if sb.Len() > 0 {
					sb.WriteString(", ")
				}
				sb.WriteString(label.Title)
				break
			}
		}
	}
	assignees := sb.String()

	return trapezi.Text(assignees)
}

func buildReviewersCell(mr types.MRListEntry) trapezi.Cell {
	return buildUsersCell(mr, mr.Reviewers.Nodes)
}

func buildAssigneesCell(mr types.MRListEntry) trapezi.Cell {
	return buildUsersCell(mr, mr.Assignees.Nodes)
}

func buildUsersCell(mr types.MRListEntry, users []types.NamedUser) trapezi.Cell {
	var sb strings.Builder
	for _, user := range users {
		if sb.Len() > 0 {
			sb.WriteString(", ")
		}
		sb.WriteString(user.Username)
		if user.Username == mr.Author.Username {
			sb.WriteString("*")
		}
		approved := false
		for _, approver := range mr.ApprovedBy.Nodes {
			if approver.Username == user.Username {
				approved = true
				break
			}
		}
		if approved {
			sb.WriteString("(✓)")
		}
	}

	return trapezi.Text(sb.String())
}

func buildTitleCell(listEntry types.MRListEntry) trapezi.Cell {
	var mrTitle string
	if mrListConfig.hideTitle {
		mrTitle = ""
	} else {
		mrTitle = strings.Trim(listEntry.Title, " ")
	}

	if mrListConfig.shortTitle && len(mrTitle) > 20 {
		mrTitle = fmt.Sprintf("%.20s", mrTitle)
	}

	refCell := trapezi.Text(listEntry.Reference)
	if mrListConfig.maxRefLength > 0 && len(listEntry.Reference) > mrListConfig.maxRefLength {
		i := len(listEntry.Reference) - (mrListConfig.maxRefLength + 3)
		refCell = trapezi.Text("..." + listEntry.Reference[i:])
	}

	refCell.Align = trapezi.ALIGN_RIGHT
	refCell.Printer = titlePrinter
	cells := []trapezi.Cell{refCell}

	if mrListConfig.mergeStatus {
		var status gitlab.BuildStateValue
		var msg string
		lab.CmdLogger().Infof("merge status: %s", listEntry.MergeStatus)
		if listEntry.MergeStatus == types.CanBeMerged {
			msg = "🌱"
			status = gitlab.Success
		} else if listEntry.MergeStatus == types.Unchecked {
			msg = "⌛"
			status = gitlab.Pending
		} else if listEntry.MergeStatus == types.Checking {
			msg = "⏳"
			status = gitlab.Pending
		} else {
			msg = "🔴"
			status = gitlab.Failed
		}
		mergeStatusCell := statusCell(status, msg)
		cells = append(cells, mergeStatusCell)
	}

	if mrListConfig.showWip {
		if listEntry.WorkInProgress {
			cells = append(cells, trapezi.Text("🚧"))
		} else {
			cells = append(cells, trapezi.Text("👷"))
		}
	}

	if !mrListConfig.hideTitle {
		cell := trapezi.Text(mrTitle)
		cell.Printer = titlePrinter
		cells = append(cells, cell)
	}

	return trapezi.Join(" ", cells...)
}

func noTitleCell() bool {
	return mrListConfig.hideTitle && mrListConfig.hideRef && !mrListConfig.mergeStatus && !mrListConfig.showWip
}

func getUserId(username string, userId **int) error {
	if username == "@" {
		_userId, err := lab.UserID()
		if err != nil {
			return err
		}
		*userId = &_userId
	} else if username != "" {
		_userId, err := lab.UserIDByUserName(username)
		if err != nil {
			return err
		}
		*userId = &_userId
	}
	return nil
}

func authoredBy(mr types.MRListEntry, reviewer string) bool {
	if reviewer == "@" {
		fetchCurrentUsername()
		return mr.Author.Username == *currentUserName
	} else if reviewer == "" {
		return false
	} else {
		return mr.Author.Username == reviewer
	}
}

func fetchCurrentUsername() {
	if currentUserName != nil {
		return
	}
	req := graphql.NewRequest(`
   query {
	   currentUser { username }
	 }
	`)
	ctx := context.Background()
	var resp struct {
		CurrentUser struct {
			Username string
		}
	}
	client := lab.GQLClient()
	err := client.Run(ctx, req, &resp)
	if err != nil {
		log.Fatal(err)
	}
	currentUserName = &resp.CurrentUser.Username
}

type MRListFilters struct {
	State        string
	TargetBranch string
	Labels       []string
	ProjectPath  string
}

func listMRsByProject(projectPath string, filters MRListFilters) ([]types.MRListEntry, error) {
	client := lab.GQLClient()
	// make a request
	// TODO: put author back: needs upstream work
	query := fmt.Sprintf(`
		query($projectPath: ID! %s) {
		  project(fullPath: $projectPath) {
				mergeRequests(%s) {
					nodes { %s }
				}
			}
		}
	`, mrFilterBindings(filters, false), mrRestrict(filters), mrSelectedFields())

	lab.CmdLogger().Infof("Query: %s", query)

	req := graphql.NewRequest(query)

	ctx := context.Background()
	var resp struct {
		Project struct {
			MergeRequests struct {
				Nodes []types.MRListEntry
			}
		}
	}
	req.Var("projectPath", projectPath)
	applyFilters(req, filters)
	err := client.Run(ctx, req, &resp)

	return resp.Project.MergeRequests.Nodes, err
}

func listMRsByUser(username string, edge string, filters MRListFilters) ([]types.MRListEntry, error) {
	client := lab.GQLClient()
	var userQuery, declaration string
	if username == "@" {
		userQuery = "currentUser"
		declaration = ""
	} else {
		userQuery = "user(username: $username)"
		declaration = "$username: String!"
	}
	query := fmt.Sprintf(`
		query mrsByUser(%s %s) {
			user: %s {
				mrs: %sMergeRequests(%s) {
					nodes { %s }
				}
			}
		}
	`, declaration, mrFilterBindings(filters, declaration != ""), userQuery, edge, mrRestrict(filters), mrSelectedFields())
	req := graphql.NewRequest(query)
	lab.CmdLogger().Infof("Query: %s", query)

	ctx := context.Background()
	var resp struct {
		User struct {
			MRs struct {
				Nodes []types.MRListEntry
			}
		}
	}
	setVariable(req, "username", username)
	applyFilters(req, filters)
	err := client.Run(ctx, req, &resp)

	return resp.User.MRs.Nodes, err
}

func setVariable(req *graphql.Request, key string, value interface{}) {
	lab.CmdLogger().Infof("%s: %v", key, value)
	req.Var(key, value)
}

func mrSelectedFields() string {
	return "id iid sourceBranch targetBranch shortRef: reference project{name id fullPath} " + refField() + mrOptionalFields()
}

func refField() string {
	var fullness string
	if mrListConfig.mrAuthor != "" || mrListConfig.mrAssignee != "" {
		fullness = fmt.Sprintf("%v", !mrListConfig.currentProjectOnly)
	} else {
		fullness = "false"
	}
	return " reference(full: " + fullness + ") "
}

func mrFilterBindings(filters MRListFilters, needsComma bool) string {
	var sb strings.Builder

	if filters.State != "" {
		if needsComma {
			sb.WriteString(", ")
		}
		needsComma = true
		sb.WriteString("$mrstate: MergeRequestState")
	}
	if filters.TargetBranch != "" {
		if needsComma {
			sb.WriteString(", ")
		}
		needsComma = true
		sb.WriteString("$targetbranch: String!")
	}
	if len(filters.Labels) > 0 {
		if needsComma {
			sb.WriteString(", ")
		}
		needsComma = true
		sb.WriteString("$labels: [String!]")
	}
	if filters.ProjectPath != "" {
		if needsComma {
			sb.WriteString(", ")
		}
		needsComma = true
		// ProjectPath has different types depending on the field!!
		sb.WriteString("$projectPath: String!")
	}
	return sb.String()
}

func mrRestrict(filters MRListFilters) string {
	var sb strings.Builder
	var commaNeeded = false
	if filters.State != "" {
		sb.WriteString("state: $mrstate")
		commaNeeded = true
	}
	if filters.TargetBranch != "" {
		if commaNeeded {
			sb.WriteString(", ")
		}
		commaNeeded = true
		sb.WriteString("targetBranches: [$targetbranch]")
	}
	if len(filters.Labels) > 0 {
		if commaNeeded {
			sb.WriteString(", ")
		}
		commaNeeded = true
		sb.WriteString("labels: $labels")
	}
	if filters.ProjectPath != "" {
		if commaNeeded {
			sb.WriteString(", ")
		}
		commaNeeded = true
		sb.WriteString("projectPath: $projectPath")
	}
	return sb.String()
}

func mrOptionalFields() string {
	var sb strings.Builder
	if mrListConfig.interactive {
		sb.WriteString("description ")
	}
	if mrListConfig.interactive || mrListConfig.showAssignees || mrListConfig.indicateInputNeeded {
		sb.WriteString("assignees { nodes { username } }\n")
	}
	if mrListConfig.interactive || mrListConfig.showReviewers {
		sb.WriteString("reviewers { nodes { username } }\n")
	}
	if mrListConfig.interactive {
		sb.WriteString("project { fullPath } ")
	}
	if mrListConfig.interactive || mrListConfig.showReviewers || mrListConfig.showAssignees || mrListConfig.indicateInputNeeded {
		sb.WriteString("approvedBy { nodes { username } }\n")
	}

	if mrListConfig.showAuthor || mrListConfig.indicateInputNeeded || mrListConfig.mrReviewer != "" {
		sb.WriteString("author { username }\n")
	}
	if mrListConfig.interactive || !mrListConfig.hideTitle {
		sb.WriteString("title\n")
	}
	if mrListConfig.mergeStatus {
		sb.WriteString("mergeStatus: mergeStatusEnum\n")
	}
	if mrListConfig.showWip {
		sb.WriteString("workInProgress: draft\n")
	}
	if mrListConfig.showAge || mrListConfig.interactive {
		sb.WriteString("createdAt")
	}
	if mrListConfig.interactive || mrListConfig.ciStatus {
		sb.WriteString(" headPipeline { status createdAt finishedAt ")
		if mrListConfig.pipelineStatus {
			sb.WriteString(`
				success: jobs(statuses: [SKIPPED, SUCCESS]) { count }
				total: jobs { count }
			`)
		}
		sb.WriteString("}")
	}
	if mrListConfig.showDiffStatSummary {
		sb.WriteString("diffStatsSummary { additions deletions fileCount }\n")
	}
	if mrListConfig.interactive || mrListConfig.markdownLayout || len(mrListConfig.mrShowLabels) > 0 {
		sb.WriteString("labels { nodes { title } }\n")
	}
	if mrListConfig.interactive || mrListConfig.markdownLayout || mrListConfig.extendedLayout || mrListConfig.showURL {
		sb.WriteString("webUrl\n")
	}
	if mrListConfig.extendedLayout {
		sb.WriteString("updatedAt\n")
	}
	if mrListConfig.interactive || mrListConfig.extendedLayout || mrListConfig.showMilestone {
		sb.WriteString("milestone { title }\n")
	}

	return sb.String()
}

func applyFilters(req *graphql.Request, filters MRListFilters) {
	if filters.State != "" {
		setVariable(req, "mrstate", filters.State)
	}
	if filters.TargetBranch != "" {
		setVariable(req, "targetbranch", filters.TargetBranch)
	}
	if len(filters.Labels) > 0 {
		setVariable(req, "mrLabels", filters.Labels)
	}
	if filters.ProjectPath != "" {
		setVariable(req, "projectPath", filters.ProjectPath)
	}
}

func init() {
	listCmd.Flags().IntVarP(
		&mrListConfig.mrNumRet, "number", "n", 10,
		"number of merge requests to return")
	// Filters
	listCmd.Flags().StringSliceVarP(
		&mrListConfig.mrLabels, "label", "l", []string{}, "filter merge requests by label")
	listCmd.Flags().StringSliceVarP(
		&mrListConfig.mrShowLabels, "show-scoped-label", "", []string{}, "Show labels with given scopes")
	listCmd.Flags().StringVarP(
		&mrListConfig.mrState, "state", "s", string(StateOpen),
		"filter merge requests by state (opened/closed/merged)")
	listCmd.Flags().StringVarP(
		&mrListConfig.mrTargetBranch, "target-branch", "t", "",
		"filter merge requests by target branch")
	listCmd.Flags().BoolVarP(&mrListConfig.mrAll, "all", "a", false, "List all MRs on the project")
	listCmd.Flags().BoolVarP(&mrListConfig.mrMine, "mine", "m", false, "List only MRs assigned to me")
	listCmd.Flags().BoolVarP(&mrListConfig.currentProjectOnly, "project", "p", false, "List only MRs in the current project. Overridden by --target-project")
	listCmd.Flags().StringVarP(&mrListConfig.targetProject, "target-project", "T", "", "List merge-requests only for the given project. Takes precedence over --project")
	listCmd.Flags().StringVar(
		&mrListConfig.mrAssignee, "assignee", "", "List only MRs assigned to $username (or @ for assigned to me)")
	listCmd.Flags().StringVar(
		&mrListConfig.mrAuthor, "author", "", "List only MRs authored by $username (or @ for by me)")
	listCmd.Flags().StringVar(
		&mrListConfig.mrReviewer, "reviewer", "", "List only MRs assigned to, but not authored by $username (or @ for assigned to me)")
	// Selections
	listCmd.Flags().BoolVarP(&mrListConfig.ciStatus, "ci-status", "c", false, "Include CI Status in the results")
	listCmd.Flags().BoolVarP(&mrListConfig.ciStatusSymbol, "ci-status-symbol", "", false, "Show CI status as a symbol (implies ci-status)")
	listCmd.Flags().BoolVarP(&mrListConfig.ciTime, "ci-time", "", false, "Include time of last CI run in the results")
	listCmd.Flags().BoolVarP(&mrListConfig.pipelineStatus, "pipeline-summary", "", false, "Include Pipeline Summary in the results (implies ci-status, ci-time when on)")
	listCmd.Flags().BoolVarP(&mrListConfig.mergeStatus, "merge-status", "", false, "Include Merge Status in the results")
	listCmd.Flags().BoolVarP(&mrListConfig.hideTitle, "notitle", "", false, "Hide the MR title")
	listCmd.Flags().BoolVarP(&mrListConfig.shortTitle, "short-title", "", false, "Show the MR title, shortened")
	listCmd.Flags().BoolVarP(&mrListConfig.hideRef, "noref", "", false, "Hide the MR reference")
	listCmd.Flags().IntVarP(&mrListConfig.maxRefLength, "truncate-ref", "", 0, "Truncate references to N characters")
	listCmd.Flags().BoolVarP(&mrListConfig.showAssignees, "assignees", "", false, "Include assignees in the results")
	listCmd.Flags().BoolVarP(&mrListConfig.showReviewers, "reviewers", "r", false, "Include reviewers in the results")
	listCmd.Flags().BoolVarP(&mrListConfig.showAuthor, "show-author", "", false, "Include author in the results")
	listCmd.Flags().BoolVarP(&mrListConfig.indicateInputNeeded, "mark-needing-attention", "", false, "Show an indicator if the MR needs attention")
	listCmd.Flags().BoolVarP(&mrListConfig.urgentOnly, "hide-non-urgent", "", false, "Show only MRs needing attention")
	listCmd.Flags().BoolVarP(&mrListConfig.mrSourceBranch, "show-branch", "b", false, "Include source branch in the results")
	listCmd.Flags().BoolVarP(&mrListConfig.showTarget, "show-target", "", false, "Include target branch in the results")
	listCmd.Flags().IntVarP(&mrListConfig.maxBranchLen, "truncate-branch", "", 0, "Truncate branch names")
	listCmd.Flags().BoolVarP(&mrListConfig.showWip, "show-wip", "", false, "Show WIP/Draft status")
	listCmd.Flags().BoolVarP(&mrListConfig.showDiffStatSummary, "diff-stats", "d", false, "Include diff stat summary")
	listCmd.Flags().BoolVarP(&mrListConfig.showURL, "web-url", "u", false, "Include web URL")
	listCmd.Flags().BoolVarP(&lab.UseColor, "color", "", false, "Use color for CI job status")
	listCmd.Flags().BoolVarP(&mrListConfig.extendedLayout, "extended-layout", "x", false, "Use extended multi-line layout, rather than table layout")
	listCmd.Flags().BoolVarP(&mrListConfig.markdownLayout, "markdown-layout", "", false, "Use markdown nested bullet-list, rather than table layout")
	listCmd.Flags().BoolVarP(&mrListConfig.interactive, "interactive", "i", false, "Present the list interactively")
	listCmd.Flags().BoolVarP(&mrListConfig.showAge, "age", "", false, "Show age of the merge request")
	listCmd.Flags().BoolVarP(&mrListConfig.showMilestone, "milestone", "", false, "Show milestone of the merge request")

	listCmd.MarkZshCompPositionalArgumentCustom(1, "__lab_completion_remote")
	listCmd.MarkFlagCustom("state", "(opened closed merged)")
	mrCmd.AddCommand(listCmd)
}
