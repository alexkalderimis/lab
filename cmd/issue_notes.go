package cmd

import (
	"fmt"
	"log"
	strings "strings"
	"time"

	"github.com/spf13/cobra"
	gitlab "github.com/xanzy/go-gitlab"
	lab "github.com/zaquestion/lab/internal/gitlab"
	"golang.org/x/crypto/ssh/terminal"
)

var issueNotesCfg struct {
	printNoteCfg
	includeSystemNotes bool
	ignoreResolved     bool
	ignoreUsers        []string
	threaded           bool
	ignore             map[string]bool
}

var issueNotesCmd = &cobra.Command{
	Use:        "notes [remote] id",
	Aliases:    []string{"get"},
	ArgAliases: []string{"s"},
	Short:      "Show notes of an issue",
	Long:       ``,
	Run: func(cmd *cobra.Command, args []string) {
		fullPath, issueNum, err := parseArgs(args)
		if err != nil {
			log.Fatal(err)
		}
		for _, username := range issueNotesCfg.ignoreUsers {
			issueNotesCfg.ignore[username] = true
		}
		if issueNotesCfg.threaded {
			getIssueDiscussions(fullPath, int(issueNum))
		} else {
			getIssueNotes(fullPath, int(issueNum))
		}
	},
}

func getIssueNotes(fullPath string, iid int) {
	getIssueNotesSince(fullPath, iid, nil)
}

func getIssueNotesSince(fullPath string, iid int, cutoff *time.Time) {
	client := lab.Client()
	opts := gitlab.ListIssueNotesOptions{ListOptions: gitlab.ListOptions{PerPage: 500}}
	for {
		list, resp, err := client.Notes.ListIssueNotes(fullPath, iid, &opts)
		if err != nil {
			log.Fatal(err)
		}
		for _, note := range list {
			if cutoff != nil && !note.CreatedAt.After(*cutoff) {
				continue
			}
			if note.System && !issueNotesCfg.includeSystemNotes {
				continue
			}
			if issueNotesCfg.ignore[note.Author.Username] {
				continue
			}
			if note.Resolved && issueNotesCfg.ignoreResolved {
				continue
			}
			fmt.Println(printIssueNote(note, false))
		}
		opts.ListOptions.Page = resp.NextPage
		if resp.CurrentPage >= resp.NextPage || resp.CurrentPage == resp.TotalPages {
			break
		}
	}
}

func getIssueDiscussions(fullPath string, iid int) {
	client := lab.Client()
	opts := gitlab.ListIssueDiscussionsOptions{PerPage: 500}
	var w strings.Builder
	for {
		lab.CmdLogger().Debugf("ListIssueDiscussions fetching opts=%s", opts)
		list, resp, err := client.Discussions.ListIssueDiscussions(fullPath, iid, &opts)
		if err != nil {
			log.Fatal(err)
		}
		lab.CmdLogger().Debugf("ListMergeRequestDiscussions resp=%s", resp)
		lab.CmdLogger().Debugf("ListMergeRequestDiscussions found %d discussions", len(list))
		for _, discussion := range list {
			w.Reset()
			written := 0
			for i, note := range discussion.Notes {
				if note.System && !issueNotesCfg.includeSystemNotes {
					continue
				}
				if issueNotesCfg.ignore[note.Author.Username] {
					continue
				}
				if issueNotesCfg.ignoreResolved && note.Resolved {
					continue
				}
				n, err := w.WriteString(printIssueNote(note, i > 0))
				if err != nil {
					log.Fatal(err)
				}
				written += n
			}
			if written > 0 {
				fmt.Println(strings.Repeat("=", 80))
				fmt.Printf("Discussion: %s\n", discussion.ID)
				fmt.Println(w.String())
			}
		}
		lab.CmdLogger().Debugf("ListMergeRequestDiscussions opts.Page=%s resp.NextPage=%s", opts.Page, resp.NextPage)
		opts.Page = resp.NextPage
		if resp.CurrentPage >= resp.NextPage || resp.CurrentPage == resp.TotalPages {
			lab.CmdLogger().Debugf("ListMergeRequestDiscussions complete resp=%s", resp)
			break
		}
	}
}

func printIssueNote(note *gitlab.Note, isReply bool) string {
	var b strings.Builder
	resolved := ""
	if note.Resolved {
		resolved = "RESOLVED"
	}
	indent := " "

	fmt.Fprintf(&b, "%s@%*s %s %s\n%s%s",
		indent, -40, note.Author.Username, note.CreatedAt, resolved,
		indent, strings.Repeat("-", 80))

	formatNoteBody(&b, indent, note.Body, issueNotesCfg.printNoteCfg)
	fmt.Fprintf(&b, "\n\n")

	return b.String()
}

func init() {
	issueNotesCfg.lineLen = 100
	issueNotesCmd.MarkZshCompPositionalArgumentCustom(1, "__lab_completion_remote")
	issueNotesCmd.MarkZshCompPositionalArgumentCustom(2, "__lab_completion_merge_request $words[2]")
	issueNotesCmd.Flags().BoolVarP(&issueNotesCfg.includeSystemNotes, "system-notes", "s", false, "Include system notes, in addition to user comments")
	issueNotesCmd.Flags().BoolVarP(&issueNotesCfg.threaded, "threaded", "t", true, "Show notes in their discussion threads, with replies below the head comment")
	issueNotesCmd.Flags().BoolVarP(&issueNotesCfg.ignoreResolved, "ignore-resolved", "", false, "Do not show resolved discussions or notes")
	issueNotesCmd.Flags().IntVarP(&issueNotesCfg.contextWindow, "context-window", "c", 5, "How large the context window should be (0 == no context, 1 == just the line)")
	width, _, err := terminal.GetSize(0)
	if err == nil && issueNotesCfg.lineLen > width {
		issueNotesCfg.lineLen = width
	}
	issueNotesCmd.Flags().IntVarP(&issueNotesCfg.lineLen, "line-length", "l", issueNotesCfg.lineLen, "How wide should comments be? Lines longer than this value will be wrapped")
	issueNotesCmd.Flags().StringSliceVar(&issueNotesCfg.ignoreUsers, "ignore", issueNotesCfg.ignoreUsers, "Set of users to ignore")
	issueCmd.AddCommand(issueNotesCmd)
}
