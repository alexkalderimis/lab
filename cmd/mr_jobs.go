package cmd

import (
	"context"
	"fmt"
	"log"
	"os"
	"text/tabwriter"
	"time"

	color "github.com/fatih/color"
	graphql "github.com/machinebox/graphql"
	"github.com/spf13/cobra"
	gitlab "github.com/xanzy/go-gitlab"
	gid "github.com/zaquestion/lab/internal/gid"
	lab "github.com/zaquestion/lab/internal/gitlab"
)

var mergeRequestIds = gid.Factory("gitlab", "MergeRequest")

// MRJobsCmdCfg The configuration for this command
type MRJobsCmdCfg = struct {
	onlyFailures bool
	noSkipped    bool
	wait         bool
	noCreated    bool
	summaryOnly  bool
}

var mrJobsCmdCfg MRJobsCmdCfg

// ciStatusCmd represents the run command
var mrJobsCmd = &cobra.Command{
	Use:     "jobs [remote] [id]",
	Aliases: []string{"run"},
	Short:   "List status and jobs for the MR head pipeline",
	Long:    ``,
	Example: "lab mr jobs",
	RunE:    nil,
	Run:     runMrJobs,
}

// CiStage The representation of a stage
type CiStage = struct {
	Name   string
	Status lab.BuildStateValue
}

// CiJob The representation of a job, sufficient for printing
type CiJob = struct {
	ID     gid.GID
	Name   string
	Status lab.BuildStateValue
	Stage  struct {
		Name string
	}
}

// MRWithJobs A merge request, along with details of the jobs in the current pipeline
type MRWithJobs = struct {
	IID          string
	Title        string
	Reference    string
	SourceBranch string
	HeadPipeline *struct {
		ID         gid.GID
		Status     lab.BuildStateValue
		FinishedAt *time.Time
		StartedAt  *time.Time
		CreatedAt  *time.Time
		Duration   *int
		Stages     struct {
			Nodes []CiStage
		}
		// jobs:
		Total struct {
			Count int
		}
		Created struct {
			Count int
		}
		Failed struct {
			Count int
			Nodes []CiJob
		}
		Running struct {
			Count int
			Nodes []CiJob
		}
		Passed struct {
			Count int
			Nodes []CiJob
		}
	}
}

func getMRWithJobs(mrIdents MergeRequestIdentifiers) (MRWithJobs, error) {
	additionalJobs := `
		running: jobs(statuses: RUNNING) { ...jobs }
		passed: jobs(statuses: SUCCESS) { ...jobs }
		`
	if mrJobsCmdCfg.onlyFailures {
		// still need the counts for the summary
		additionalJobs = `
			running: jobs(statuses: RUNNING) { count }
			passed: jobs(statuses: SUCCESS) { count }
		`
	}

	fragmentDef := fmt.Sprintf(`
	  fragment jobs on CiJobConnection {
			count
			nodes {
				id name status stage { name }
			}
		}

	  fragment jobDetails on Pipeline {
			stages {
				nodes {
					name status
				}
			}

			total: jobs { count }
			created: jobs(statuses: CREATED) { count }
			failed: jobs(statuses: FAILED) { ...jobs }
			%s
		}
  `, additionalJobs)

	fragmentRef := "...jobDetails"

	if mrJobsCmdCfg.summaryOnly {
		fragmentDef = ""
		fragmentRef = ""
	}

	query := fmt.Sprintf(`
	  %s

		query($id: MergeRequestID!) {
			mergeRequest(id: $id) {
				id
				title
				reference
				sourceBranch

				headPipeline {
				  id
					status
					finishedAt startedAt createdAt duration
					%s
				}
			}
		}
	`, fragmentDef, fragmentRef)

	lab.CmdLogger().Infof("Query: %s", query)

	req := graphql.NewRequest(query)
	mrid := mergeRequestIds.Create(mrIdents.ID)
	req.Var("id", &mrid)

	ctx := context.Background()
	var resp struct {
		MergeRequest MRWithJobs
	}
	client := lab.GQLClient()
	err := client.Run(ctx, req, &resp)

	if err != nil {
		return MRWithJobs{}, err
	}

	return resp.MergeRequest, nil
}

func runMrJobs(cmd *cobra.Command, args []string) {
	w := tabwriter.NewWriter(os.Stdout, 2, 4, 1, byte(' '), 0)
	lab.CmdLogger().Debugf("Parsing arguments: %v", args)
	_, idents, err := parseProjectMR(args, StateOpen)

	if err != nil {
		log.Fatal(err)
	}
	lab.CmdLogger().Debugf("Getting MR %v", idents)
	mr, err := getMRWithJobs(*idents)
	if err != nil {
		log.Fatal(err)
	}
	if mr.Reference == "" {
		lab.Uncache(mrCacheKey(idents.ID))
		uncacheMRIdents(args, StateOpen)
		log.Fatal("MR is blank.")
	}

	trackMRByBranchName(mr.SourceBranch, mr.Reference)

	// TODO: select pipeline
	if mr.HeadPipeline == nil {
		log.Fatalf("No pipeline for %s\n", mr.Reference)
	}
	trackMRBuildStatusByBranchName(mr.SourceBranch, gitlab.BuildStateValue(mr.HeadPipeline.Status))

	color.NoColor = !lab.UseColor
	var (
		printer *color.Color
	)

	pipelineID, err := mr.HeadPipeline.ID.IntegerID()
	if err != nil {
		log.Fatalf("Illegal pipeline ID")
	}

	fmt.Fprintf(w, "Pipeline #%d for %s: %s\n", pipelineID, mr.Reference, mr.Title)

	if !summaryOnly {

		if !mrJobsCmdCfg.summaryOnly {
			fmt.Fprintln(w, "Stage:\tName\t-\tStatus")
		}
		var printed = make(map[gid.GID]bool)

		for _, stage := range mr.HeadPipeline.Stages.Nodes {
			if mrJobsCmdCfg.onlyFailures && (stage.Status == "success" || stage.Status == "skipped" || stage.Status == "pending") {
				continue
			}

			var jobs []CiJob

			jobs = append(jobs, mr.HeadPipeline.Failed.Nodes...)

			if !mrJobsCmdCfg.onlyFailures {
				jobs = append(jobs, mr.HeadPipeline.Passed.Nodes...)
				jobs = append(jobs, mr.HeadPipeline.Running.Nodes...)
			}

			printer = lab.StatusColor(string(stage.Status))

			printer.Fprintf(w, "\nStage: %s (%s):\n", stage.Name, stage.Status)
			fmt.Fprintln(w, "===============")

			maxNameLength := 0

			for _, job := range jobs {
				if len(job.Name) > maxNameLength {
					maxNameLength = len(job.Name)
				}
			}

			for _, job := range jobs {
				if printed[job.ID] {
					continue
				}
				if job.Stage.Name != stage.Name {
					continue
				}

				printed[job.ID] = true

				if mrJobsCmdCfg.onlyFailures && job.Status != "failed" {
					continue
				}
				if mrJobsCmdCfg.noSkipped && job.Status == "skipped" {
					continue
				}
				if mrJobsCmdCfg.noCreated && job.Status == "created" {
					continue
				}

				printer = lab.StatusColor(string(job.Status))
				printer.Fprintf(w, jobFormatNoStage,
					-maxNameLength, job.Name,
					job.Status, job.ID.ID)
			}
		}
	}

	fmt.Fprintf(w, mrPipelineSummary(mr))

	w.Flush()
}

func mrPipelineSummary(mr MRWithJobs) string {
	return fmt.Sprintf("\nPipeline Status:\t%s\n%s\n\n%s\n",
		mr.HeadPipeline.Status.ColorString(), mrTimeMessage(mr), mrJobSummary(mr))
}

func mrJobSummary(mr MRWithJobs) string {
	total := mr.HeadPipeline.Total.Count
	numQueued := mr.HeadPipeline.Created.Count
	numFailed := mr.HeadPipeline.Failed.Count
	numPassed := mr.HeadPipeline.Passed.Count

	return fmt.Sprintf("total\tpassed\tfailed\tqueued\n%d\t%d\t%d\t%d",
		total, numPassed, numFailed, numQueued)
}

func mrTimeMessage(mr MRWithJobs) string {
	pipeline := mr.HeadPipeline
	if pipeline.FinishedAt != nil {
		finished := fmt.Sprintf("finished at %s\n", layoutTime(*pipeline.FinishedAt))
		if pipeline.Duration != nil && *pipeline.Duration >= 60 {
			return finished + fmt.Sprintf("duration:\t%d minutes", *pipeline.Duration/60)
		}
		return finished + fmt.Sprintf("duration:\t%d secs", pipeline.Duration)
	}

	if pipeline.Status == "running" {
		return fmt.Sprintf("started at %s", layoutTime(*pipeline.StartedAt))
	}

	return fmt.Sprintf("created at %s", layoutTime(*pipeline.CreatedAt))
}

func headPipelineTime(mr MRWithJobs) string {
	if mr.HeadPipeline.FinishedAt != nil {
		finished := fmt.Sprintf("finished at %s\n", layoutTime(*mr.HeadPipeline.FinishedAt))
		if mr.HeadPipeline.Duration != nil && *mr.HeadPipeline.Duration >= 60 {
			return finished + fmt.Sprintf("duration:\t%d minutes", *mr.HeadPipeline.Duration/60)
		}
		return finished + fmt.Sprintf("duration:\t%d secs", *mr.HeadPipeline.Duration)
	}

	if gitlab.BuildStateValue(mr.HeadPipeline.Status) == gitlab.Running {
		return fmt.Sprintf("started at %s", layoutTime(*mr.HeadPipeline.StartedAt))
	}

	return fmt.Sprintf("created at %s", layoutTime(*mr.HeadPipeline.CreatedAt))
}

func init() {
	mrJobsCmd.MarkZshCompPositionalArgumentCustom(1, "__lab_completion_remote_branches")
	mrJobsCmd.MarkZshCompPositionalArgumentCustom(2, "__lab_completion_merge_request $words[2]")
	mrJobsCmd.Flags().BoolVarP(&lab.UseColor, "color", "c", false, "Use color for success and failure")

	mrJobsCmd.Flags().BoolVarP(&mrJobsCmdCfg.wait, "wait", "w", false, "Continuously print the status and wait to exit until the pipeline finishes. Exit code indicates pipeline status")
	mrJobsCmd.Flags().BoolVarP(&mrJobsCmdCfg.noSkipped, "no-skipped", "", false, "Ignore skipped tests - do not print them")
	mrJobsCmd.Flags().BoolVarP(&mrJobsCmdCfg.onlyFailures, "failures", "f", false, "Only print failures")
	mrJobsCmd.Flags().BoolVarP(&mrJobsCmdCfg.noCreated, "results-only", "r", false, "Only show completed and running tests. Does not report queued jobs")
	mrJobsCmd.Flags().BoolVarP(&mrJobsCmdCfg.summaryOnly, "summary", "s", false, "Do not show individual jobs, just the pipeline summary")

	mrCmd.AddCommand(mrJobsCmd)
}
