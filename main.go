package main

import (
	"crypto/sha1"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/user"
	"path"
	"runtime"
	"strings"

	"github.com/spf13/viper"
	gitlab "github.com/xanzy/go-gitlab"
	"github.com/zaquestion/lab/cmd"
	"github.com/zaquestion/lab/internal/config"
	lab "github.com/zaquestion/lab/internal/gitlab"
)

// version gets set on releases during build by goreleaser.
var version = "master"

func loadConfig() (string, string, string) {
	var home string
	switch runtime.GOOS {
	case "windows":
		// userprofile works for roaming AD profiles
		home = os.Getenv("USERPROFILE")
	default:
		// Assume linux or osx
		home = os.Getenv("HOME")
		if home == "" {
			u, err := user.Current()
			if err != nil {
				log.Fatalf("cannot retrieve current user: %v \n", err)
			}
			home = u.HomeDir
		}
	}
	// Try XDG_CONFIG_HOME which is declared in XDG base directory specification
	confpath := os.Getenv("XDG_CONFIG_HOME")
	if confpath == "" {
		confpath = path.Join(home, ".config")
	}
	if _, err := os.Stat(confpath); os.IsNotExist(err) {
		os.Mkdir(confpath, 0700)
	}

	viper.SetConfigName("lab")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.AddConfigPath(confpath)

	viper.SetEnvPrefix("LAB")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()

	host, user, token := viper.GetString("core.host"), viper.GetString("core.user"), viper.GetString("core.token")
	if host != "" && user != "" && token != "" {
		return host, user, token
	} else if host != "" && token != "" {
		return host, getUser(), token
	}

	// Attempt to auto-configure for GitLab CI
	host, user, token = config.CI()
	if host != "" && user != "" && token != "" {
		return host, user, token
	}

	err := viper.ReadInConfig()
	if err != nil {
		switch err.(type) {
		case viper.ConfigFileNotFoundError:
			if err := config.New(path.Join(confpath, "lab.yaml"), os.Stdin); err != nil {
				log.Fatal(err)
			}

			if err := viper.ReadInConfig(); err != nil {
				log.Fatal(err)
			}
		default:
			log.Fatal(err)
		}
	}

	for _, path := range []string{"core.host", "core.token"} {
		if viper.GetString(path) == "" {
			log.Fatalf("missing required config value %s in %s", path, viper.ConfigFileUsed())
		}
	}

	user = getUser()
	viper.Set("core.user", user)
	return viper.GetString("core.host"), user, viper.GetString("core.token")
}

func getUser() string {
	lab.InitCache()
	host := viper.GetString("core.host")
	token := viper.GetString("core.token")
	lab.CmdLogger().Debugf("Getting username for %s", host)
	client := gitlab.NewClient(nil, token)
	client.SetBaseURL(host + "/api/v4")
	hash := sha1.Sum([]byte(fmt.Sprintf("%s:%s", host, token)))
	filename := fmt.Sprintf("user-%x.json", hash)
	inCache, bytes, err := lab.ReadCacheTouch(filename, true)
	if inCache && err == nil {
		user := gitlab.User{}
		err := json.Unmarshal(bytes, &user)
		if err == nil {
			lab.CmdLogger().Debugf("Found user in cache for %s", filename)
			return user.Username
		}
	}
	lab.CmdLogger().Debugf("Fetching user")
	u, _, err := client.Users.CurrentUser()
	if err != nil {
		log.Fatal(err)
	}

	bytes, err = json.Marshal(u)
	if err == nil {
		lab.WriteCache(filename, bytes)
	}

	return u.Username
}

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	cmd.Version = version
	if !skipInit() {
		lab.Init(loadConfig())
	}
	cmd.Execute()
}

func skipInit() bool {
	if len(os.Args) <= 1 {
		return false
	}
	return os.Args[1] == "completion"
}
